import { AppRegistry } from 'react-native';

import App from './src/App.android';

AppRegistry.registerComponent('hughdemo', () => App);