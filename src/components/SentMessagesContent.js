import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    Image
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import Hr from 'react-native-hr';

const styles = StyleSheet.create({
    app: {
        flex: 1,
        height: Dimensions.get('window').height,
        ...Platform.select({
            android: {
                paddingBottom: 50
            }
        })
    },
    scrollView: {
        backgroundColor: '#fff',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        padding: 15,
        marginBottom: 70,
        ...Platform.select({
            android: {
                position: 'absolute',
                marginBottom: 80
            }
        })
    },
    messageContainer: {
        paddingTop: 20,
        paddingBottom: 20,
        marginLeft: 7,
        marginRight: 7
    },
    messageHeading: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    messageSubheading: {
        color: '#808080',
        fontSize: 10
    },
    resultsContainer: {
        marginTop: 20  
    },
    result: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginBottom: 10
    },
    resultStatus: {
        marginRight: 10  
    },
    reactionImage: {
        height: 15,
        width: 15
    },
    bottomButtonWrapper: {         
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
    },
    newMessageButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    arrowWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        right: 0
    },
    arrowText: {
        height: 25,
        width: 25
    }
});

@autobind
export default class SentMessagesContent extends Component {

    static propTypes = {
        onNewMessage: React.PropTypes.func,
        onMessagePress: React.PropTypes.func
    }
    
    state = {}

    render() {
        
        return (
            <View style={styles.app}>   
                <ScrollView keyboardShouldPersistTaps={"handled"} style={styles.scrollView}>
                    <SentMessageContent onMessagePress={this.props.onMessagePress} />
                    <Hr lineColor='#b3b3b3' />
                    <SentMessageContent onMessagePress={this.props.onMessagePress} />
                </ScrollView>

                <View style={styles.bottomButtonWrapper}>
                    <View style={styles.newMessageButton}>
                        <Button
                            onPress={this.props.onNewMessage}
                            title='Create New Message'
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

@autobind
export class SentMessageContent extends Component {

    static propTypes = {
        onMessagePress: React.PropTypes.func
    }
    
    state = {}
    
    openMessage() {
        this.props.onMessagePress();
    }

    render() {
        
        return (
            <TouchableHighlight onPress={this.openMessage} style={styles.messageContainer} underlayColor={'#fff'}>
                <View>
                    <View>
                        <Text style={styles.messageHeading}>AI Software to track clients</Text>
                        <Text style={styles.messageSubheading}>Sent on 25th June 2017</Text>
                        <View style={styles.arrowWrapper}>
                            <Image source={require('../../images/ic_chevron_right_black_24dp/android/drawable-hdpi/ic_chevron_right_black_24dp.png')} style={styles.arrowText} />
                        </View>
                    </View>
                    <View style={styles.resultsContainer}>
                        <View style={styles.result}>
                            <View style={styles.resultStatus}><Image style={styles.reactionImage} source={require('../../images/dislike.png')} /></View>
                            <Text style={styles.resultRecipient}>Tim Cook, Apple Inc.</Text>
                        </View>
                        <View style={styles.result}>
                            <View style={styles.resultStatus}><Image style={styles.reactionImage} source={require('../../images/like.png')} /></View>
                            <Text style={styles.resultRecipient}>Larry Ellison, Oracle</Text>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }
}





                    