import React, { Component } from 'react';
import {
    Platform,
    Button,
    View,
    Text,
    StyleSheet,
    Dimensions,
    ScrollView,
    Image,
    Switch,
    NetInfo,
	TouchableHighlight,
	TouchableWithoutFeedback,
    TextInput,
    Linking,
    ActivityIndicator
 } from 'react-native';
import autobind from 'autobind-decorator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { ping } from '../services/api';

const styles = StyleSheet.create({
    app: {
		position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    label: {
        color: '#808080',
        fontSize: 13,
        marginRight: 10
    },
    registerButton: {
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 40,
        marginTop: 20,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    switchWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: 'center'
    },
    termsText: {
        width: Dimensions.get('window').width - 30,
        textDecorationLine: 'underline',
        textAlign: 'center',
        fontSize: 15,
        marginTop: 20
    },
    switchText: {
        width: Dimensions.get('window').width - 150,
        textDecorationLine: 'underline',
        marginLeft: 10,
        fontSize: 15,
    },
});

@autobind
export default class RegisterForm extends Component {

    static propTypes = {
        loading: React.PropTypes.bool,
        onSubmit: React.PropTypes.func,
        onChange: React.PropTypes.func,
        error: React.PropTypes.string,
        value: React.PropTypes.object
    }

    state = {
        user: {
			email: '',
			name: '',
			password: '',
			tAndCAgreed: true
		},
        showError: false,
        isConnected: true,
		showPassword: false
    }

	componentDidMount() {
		this.isConnected();
    }

    openUrl(url) {
        let urlLink = url;

        // has to start with http:// or https://
        if(url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
    }

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            paddingHorizontal: 15,
            paddingTop: 15,
            ...Platform.select({
                android: {
                    marginTop: 60
                },
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

    inputValidation(value, extraStyle) {

        let style = {
            height: 40,
            marginTop: 7,
            marginBottom: 15,
            borderColor: '#d0d0d0',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 7
        };

        if (this.state.showErrors && (!value || value.length === 0)) {
            style.borderColor = '#fa4f4f';
        }

		for (var attrname in extraStyle) { style[attrname] = extraStyle[attrname]; }

        return style;
    }

	checkboxStyle(checked) {
        let style = {
            position: 'relative',
            width: 30,
            height: 30,
            backgroundColor: '#fff',
            borderRadius: 5,
            borderWidth: 1,
            borderColor: '#cbcbcb',
            marginRight: 0
        }

        if (checked) {
            style.backgroundColor = '#61b746';
        }

        return style;
    }

	isConnected() {

        let that = this;

        this.props.testConnection(connectionStatus => {
            that.setState({
                isConnected: connectionStatus.connection,
                isConnectedError: connectionStatus.message
            });
        });
    }

	onChange(user) {
		this.setState({
			user: user
		});
	}

    submit() {

        this.setState({
            showError: true,
            isConnected: true,
            isConnectedError: '',
            showStatus: true
        })

        if (!/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(this.state.user.email.toLowerCase())) {
            this.setState({
                error: "Please enter a vaild email address",
                showError: true
            });
            return false;
        }

		this.props.testConnection(connectionStatus => {
            this.setState({
                isConnected: connectionStatus.connection,
                isConnectedError: connectionStatus.message
			});

            if (connectionStatus.connection) {

                if(!this.state.user.email || !this.state.user.name || !this.state.user.password) {
                    this.setState({
                        showError: true,
                        error: 'Please complete all fields'
                    });

					return false;
                } else if (!this.isPasswordValid(this.state.user.password)) {
                    this.setState({
                        showErrors: true,
                        error: 'Please complete all fields'
                    });

                    return false;
                }

                let names = this.state.user.name.split(" ");
                let firstName = names[0];
                names.splice(0, 1);
                let lastName = names.join(" ");

                const payload = {
                    email: this.state.user.email,
                    firstName: firstName,
                    lastName: lastName,
                    username: this.state.user.email,
                    password: this.state.user.password,
                    clientId: 'rayzedWebApi',
                    tAndCVersion: 1,
                    tAndCAgreed: this.state.user.tAndCAgreed,
                };
                this.props.onSubmit(payload);
                this.setState({
                    showError: true
                });
            }
        });
    }

	isPasswordValid(password) {
		let regularExpression = /^((?=.*\d).{10,30})/;

		return regularExpression.test(password);
	}

    render() {

		let passwordValid = this.isPasswordValid(this.state.user.password);

		let togglePasswordImage = this.state.showPassword? require('../../images/ic_visibility_off_black_24dp/web/ic_visibility_off_black_24dp_2x.png') : require('../../images/ic_visibility_black_24dp/web/ic_visibility_black_24dp_2x.png');

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}>

					<View style={styles.elementContainer}>
                        <Text style={styles.label}>Full name</Text>
                        <TextInput
                            ref={'input-1'}
                            autoCorrect={false}
                            style={this.inputValidation(this.state.user.name)}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(name) => this.onChange({ ...this.state.user, name })}
                            value={this.state.user.name}
                            placeholder={'Full name'}
                            returnKeyType={'next'}
                            blurOnSubmit={false}
                            onSubmitEditing={()=> this.refs['input-2'].focus()}
                        />
                    </View>

					<View style={styles.elementContainer}>
                        <Text style={styles.label}>Email (We will <Text style={{ textDecorationLine: 'underline' }}>never</Text> share your email address)</Text>
                        <TextInput
                            ref={'input-2'}
                            autoCorrect={false}
                            style={this.inputValidation(this.state.user.email)}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(email) => this.onChange({ ...this.state.user, email })}
                            value={this.state.user.email}
                            placeholder={'Email'}
                            keyboardType='email-address'
                            returnKeyType={'next'}
                            blurOnSubmit={false}
                            onSubmitEditing={()=> this.refs['input-3'].focus()}
                        />
                    </View>

					<View style={styles.elementContainer}>
                        <Text style={styles.label}>Password (10 characters including a number)</Text>
                        <TextInput
                            ref={'input-3'}
                            autoCorrect={false}
							secureTextEntry={!this.state.showPassword}
                            style={this.inputValidation(this.state.user.password, { paddingRight: 50 })}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(password) => this.onChange({ ...this.state.user, password })}
                            value={this.state.user.password}
                            placeholder={'Password'}
                            returnKeyType={'done'}
                            onSubmitEditing={()=> this.submit()}
                        />
						<TouchableWithoutFeedback onPress={()=> this.setState({ showPassword: !this.state.showPassword })}>
							<View style={{ position: 'absolute', top: 30, right: 10}}>
								<Image style={{ width: 30, height: 30 }} source={togglePasswordImage} />
							</View>
						</TouchableWithoutFeedback>
						{this.state.showErrors && !passwordValid && <Text style={{ textAlign: 'center', color: 'red' }}>Please enter a password which is at least 10 characters, and contains at least one number.</Text>}
                    </View>

					<View style={{ marginBottom: 20 }}>
                        {this.state.isConnected && this.state.showError && !this.props.error && !this.state.error && <Text style={{ textAlign: 'center', color: 'black' }}>Attempting to register...</Text>}
                        {this.state.isConnected && this.state.showError && !this.props.error && !this.state.error && <ActivityIndicator
                            animating={true}
                            style={[styles.centering, {height: 40}]}
                            size="small"
                            color="green"
                        />}
                        {this.state.isConnected && this.state.error && this.state.showError && !this.props.error && <Text style={{ textAlign: 'center', color: 'red' }}>{this.state.error}</Text>}
                        {this.state.isConnected && this.state.showError && this.props.error.length > 0 && <Text style={{ textAlign: 'center', color: 'red' }}>{this.props.error}</Text>}
                        {!this.state.isConnected && !this.state.isConnectedError && <Text style={{ textAlign: 'center', color: 'red' }}>Not connected to the internet</Text>}
                        {!this.state.isConnected && this.state.isConnectedError && <Text style={{ textAlign: 'center', color: 'red' }}>{this.state.isConnectedError}</Text>}
                    </View>
                    {false && <Text>{this.props.loading ? 'LOADING' : 'NOT LOADING ' + JSON.stringify(this.props.loading)}</Text>}

                    <View style={styles.switchWrapper}>
                        <TouchableWithoutFeedback onPress={()=> this.openUrl(this.props.links.termsAndConditions)}>
                            <View>
                                <Text style={styles.termsText}>
                                    <Text style={{ color: '#808080' }}>By registering you are agreeing to our terms.{"\n"}Click to read.</Text>
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

					{false && <View style={styles.switchWrapper}>
                        <Switch
                          onValueChange={(value) => this.setState({emailOptIn: value})}
                          style={{}}
                          value={this.state.emailOptIn} />
                        <Text style={styles.switchText}>I would like to receive emails from Raised</Text>
                    </View>}

					{this.state.showErrors && !passwordValid && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 15 }}>An error has occurred above</Text>}

                    <View style={styles.registerButton}>
                        <Button
                            disabled={this.props.loading || (!this.state.isConnected && !this.state.isConnectedError)}
                            onPress={this.submit}
                            title='Agree To Terms And Register'
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}
