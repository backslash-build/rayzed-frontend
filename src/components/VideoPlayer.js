import React, { Component } from 'react';
import {
    AppRegistry,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    Slider,
    StyleSheet,
    Dimensions,
    Text,
    Image,
    Platform,
    ActivityIndicator
} from 'react-native';
import Video from 'react-native-video';
import autobind from 'autobind-decorator';

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        width: windowWidth,
        height: (windowWidth * 9) / 16,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        margin: 0
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    controls: {
        backgroundColor: 'transparent',
        position: 'absolute',
        width: Dimensions.get('window').width,
        ...Platform.select({
            ios: {
                bottom: 0,
            },
            android: {
                bottom: 5,
            }
        }),
        left: 0,
        right: 0,
        zIndex: 100
    },
    seekbar: {
        flexDirection: 'row',
        paddingLeft: 5,
        paddingRight: 5,
        width: Dimensions.get('window').width
    },
    rewind_button: {
        width: 30,
        ...Platform.select({
            ios: {
                position: 'absolute',
                bottom: 8,
                left: 3
            },
            android: {
                position: 'relative',
                bottom: 3,
                left: 7
            }
        })
    },
    fullscreen_button: {
        width: 30,
        ...Platform.select({
            ios: {
                position: 'absolute',
                bottom: 8,
                right: 0
            },
            android: {
                position: 'absolute',
                bottom: 1,
                right: 0
            }
        })
    },
    current_time: {
        minWidth: 30,
        paddingTop: 6,
        ...Platform.select({
            ios: {
                position: 'absolute',
                bottom: 13,
                left: 30
            },
            android: {
                position: 'absolute',
                bottom: 6,
                left: 40
            }
        })
    },
    length_time: {
        minWidth: 30,
        paddingTop: 6,
        ...Platform.select({
            ios: {
                position: 'absolute',
                bottom: 13,
                right: 30
            },
            android: {
                position: 'absolute',
                bottom: 6,
                right: 35
            }
        }),
    },
    seekbar_slider: {
        padding: 0,
        margin: 0,
        ...Platform.select({
            ios: {
                position: 'absolute',
                bottom: 0,
                left: 65,
            },
            android: {
                position: 'absolute',
                bottom: 4,
                left: 65,
            }
        }),
        width: Dimensions.get('window').width - 135
    }
});

function formatTime(t) {
    return Math.floor(t/60) + ':' + ('0' + Math.floor(t%60)).slice(-2);
}

@autobind
export default class VideoPlayer extends Component {

    static propTypes = {
        videoUrl: React.PropTypes.string,
        showSlider: React.PropTypes.bool,
        onVideoEnd: React.PropTypes.func
    }

    state = {
        showControls: true,
        rate: 1,
        volume: 1,
        muted: false,
        resizeMode: 'contain',
        duration: 0.0,
        currentTime: 0.0,
        paused: true,
        videoLoading: true
    };

    video

    onLoadStart = () => {
        this.setState({
            currentTime: 0.0,
            videoLoading: true
        });
    }

    onLoad = (data) => {
        if (data.duration > 61 && this.props.lengthLimit) {
            this.props.onError();
        }

        this.setState({ duration: data.duration, paused: true, videoLoading: false });
    };

    onProgress = (data) => {
        if(!this.state.paused) {
            this.setState({
                currentTime: data.currentTime,
                videoEnded: false
            });
        }
    };

    onEnd = () => {
        this.setState({
            paused: true,
            showControls: true,
            videoEnded: true
        });
        this.props.onVideoEnd();
    };

    onAudioBecomingNoisy = () => {
        this.setState({ paused: true });
    };

    onAudioFocusChanged = (event) => {
        this.setState({ paused: !event.hasAudioFocus });
    };

    onBuffering = (event) => {
        if (!event.isBuffering) {
            this.setState({
                videoLoading: false
            })
        }
        console.log("Buffering", event);
    };

    onError = (event) => {
        console.log("Error", event);
    };

    togglePlay() {

        let showControls = true;

        if (this.state.paused) {
            if (this.state.videoEnded) {
                this.video.seek(0);
                this.setState({ paused: false, currentTime: 0 });
            } else {
                showControls = false;
                this.setState({
                    paused: !this.state.paused,
                    showControls: showControls
                });
            }
        } else {
            this.setState({
                paused: !this.state.paused,
                showControls: showControls
            });
        }
    }

    renderVideoElement() {

        if (this.props.videoUrl) {

			let sourceVideo = this.props.videoUrl;

			if (typeof this.props.videoUrl == "string") {
				sourceVideo = { uri: this.props.videoUrl };
			}

            return (<Video
                ref={(ref) => {
                    this.video = ref;
                }}
                source={sourceVideo}
                style={styles.fullScreen}
                rate={this.state.rate}
                paused={this.state.paused}
                volume={this.state.volume}
                muted={this.state.muted}
                ignoreSilentSwitch={"ignore"}
                resizeMode={this.state.resizeMode}
                onLoadStart={this.onLoadStart}
                onLoad={this.onLoad}
                onProgress={this.onProgress}
                onEnd={this.onEnd}
                onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                onAudioFocusChanged={this.onAudioFocusChanged}
                onBuffer={this.onBuffering}
                onError={this.onError}
                repeat={false}
            />);
        } else {
            return (<View>
                <Text style={{ color: '#fff' }}>{this.props.defaultMessage}</Text>
            </View>);
        }
    }

    renderVideoLoading() {

        let playButtonSize = Platform.OS == "ios"? 50 : 75;

        const renderPausedButton = () => (
            <Image
                style={{width: playButtonSize, height: playButtonSize}}
                source={require('../../images/ic_pause_white_24dp/android/drawable-xxhdpi/ic_pause_white_24dp.png')}
            />
        );
        const renderPlayButton = () => (
            <Image
                style={{width: playButtonSize, height: playButtonSize}}
                source={require('../../images/ic_play_arrow_white_24dp/android/drawable-xxhdpi/ic_play_arrow_white_24dp.png')}
            />
        );

        if (!this.state.videoLoading && this.props.videoUrl && this.state.showControls) {
            return(<TouchableOpacity
                onPress={this.togglePlay} >
                {this.state.paused ? renderPlayButton() : renderPausedButton()}
            </TouchableOpacity>);
        } else if (this.state.videoLoading && this.props.initialLoading ) {
            return(<View>
                <ActivityIndicator
                    animating={true}
                    size="small"
                    color={"white"}
                />
            </View>);
        }
    }

    render() {

        let videoElement = this.renderVideoElement();

        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback
                    style={styles.fullScreen}
                    onPress={() => this.setState({ showControls: !this.state.showControls })}
                >
                    {videoElement}
                </TouchableWithoutFeedback>

                {this.props.videoUrl && this.state.showControls && <View style={styles.controls}>
                    <View style={{ width: windowWidth }}>
                        <Seekbar
                            showSlider={this.props.showSlider}
                            currentTime={this.state.currentTime}
                            duration={this.state.duration}
                            onSeekStart={() => this.setState({ paused: true })}
                            onSeekEnd={v => {
                                this.video.seek(v);
                                this.setState({ paused: false, currentTime: v });
                            }}
                            onFullscreen={() => this.video.presentFullscreenPlayer()}
                        />
                    </View>
                </View>}

                {this.renderVideoLoading()}
            </View>
        );
    }

}

@autobind
class Seekbar extends Component {
    static propTypes = {
        currentTime: React.PropTypes.number,
        duration: React.PropTypes.number,
        onSeek: React.PropTypes.func,
        onSeekEnd: React.PropTypes.func,
        onFullscreen: React.PropTypes.func,
        showSlider: React.PropTypes.bool
    }

    state = {
        isSeeking: false
    }

    shouldComponentUpdate() {
        if(this.state.isSeeking) {
            return false;
        }
        return true;
    }

    restartVideo() {
        this.onSeekEnd(0);
    }

    onSeekStart() {
        this.props.onSeekStart();
        this.setState({ isSeeking: true });
    }

    onSeekEnd(value) {
        this.props.onSeekEnd(value);
        this.setState({ isSeeking: false });
    }

    render() {
        return (
            <View style={styles.seekbar}>
                <TouchableOpacity
                    onPress={this.restartVideo}
                    style={styles.rewind_button}>
                    <Image
                        style={{width: 24, height: 24}}
                        source={require('../../images/ic_restore_white_24dp/android/drawable-xxhdpi/ic_restore_white_24dp.png')}
                    />
                </TouchableOpacity>
                <View style={styles.current_time}>
                    <Text style={{ color: 'white', textAlign: 'center', fontSize: 12 }}>{formatTime(this.props.currentTime)}</Text>
                </View>
                {this.props.showSlider && <Slider
                    style={styles.seekbar_slider}
                    onValueChange={this.onSeekStart}
                    onSlidingComplete={this.onSeekEnd}
                    maximumValue={this.props.duration}
                    value={this.props.currentTime}
                />}
                <View style={styles.length_time}>
                    <Text style={{ color: 'white', textAlign: 'center', fontSize: 12 }}>{formatTime(this.props.duration)}</Text>
                </View>
                {Platform.OS == "ios" && <TouchableOpacity
                    onPress={() => this.props.onFullscreen()}
                    style={styles.fullscreen_button}>
                    <Image
                        style={{width: 24, height: 24}}
                        source={require('../../images/ic_fullscreen_white_24dp/android/drawable-xxhdpi/ic_fullscreen_white_24dp.png')}
                    />
                </TouchableOpacity>}
            </View>
        );
    }
}

AppRegistry.registerComponent('VideoPlayer', () => VideoPlayer);