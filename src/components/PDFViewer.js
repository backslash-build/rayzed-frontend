import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
    Linking,
    WebView,
    TouchableHighlight
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import Pdf from 'react-native-pdf';

import LoadingScene from '../containers/LoadingScene';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff'
    },
    pdf: {
        flex:1,
        width: Dimensions.get('window').width,
    },
    app: {
        flex:1,
    }
});

@autobind
export default class PDFViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            pageCount: 1,
            loading: true
        };
        this.pdf = null;
    }

    componentDidMount() {}

    render() {
        //let source = {uri:'https://www.irs.gov/pub/irs-pdf/fw2.pdf',cache:true};
        let source = {uri:this.props.attachment};
        //let source = require('./test.pdf'); //ios only
        //let source = {uri:"data:application/pdf;base64, ..."}; // this is a dummy

        return (
            <View style={styles.container}>
                {this.state.loading && <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height}}><LoadingScene /></View>}
                <View>
                    {this.state.error && <Text style={{ textAlign: 'center', color: 'red', marginVertical: 20 }}>{this.state.error}</Text>}
                </View>
                {this.props.attachment && !this.state.error && <Pdf ref={(pdf)=>{this.pdf = pdf;}}
                    spacing={1}
                    fitWidth={true}
                    source={source}
                    page={1}
                    horizontal={false}
                    onLoadComplete={(pageCount)=>{
                        this.setState({ loading: false });
                    }}
                    onError={(error)=>{
                        console.log("Pdf failed to load");
                        this.setState({
                            error: 'Pdf failed to load, please try again',
                            loading: false
                        });
                    }}
                    style={styles.pdf}/>}
            </View>
        )
    }
}