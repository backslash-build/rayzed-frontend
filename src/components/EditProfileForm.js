import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    Image,
	TouchableWithoutFeedback
 } from 'react-native';
import autobind from 'autobind-decorator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Hr from 'react-native-hr';

const styles = StyleSheet.create({
    app: {
		position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    label: {
        color: '#808080',
        fontSize: 15,
        marginRight: 10
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1
    },
    addContentButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
});

@autobind
export default class EditProfileForm extends Component {

    static propTypes = {
        onSubmit: React.PropTypes.func,
        me: React.PropTypes.object,
    }

    state = {
        showErrors: false,
		user: this.props.me.data,
		showCurrentPassword: false,
		showNewPassword: false,
		password: {},
        showError: false,
        isConnected: true
    }

	componentDidReload() {
		this.setState({
			password: null
		});
	}

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            paddingHorizontal: 15,
            paddingTop: 15,
            ...Platform.select({
                android: {
                    marginTop: 60,
                    marginBottom: 96
                },
                ios: {
                    marginBottom: 80
                }
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

    inputValidation(value, extraStyle) {

        let style = {
            height: 40,
            marginTop: 7,
            marginBottom: 15,
            borderColor: '#d0d0d0',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 7
        };

//        if (this.state.showErrors && (!value || value.length === 0)) {
//            style.borderColor = '#fa4f4f';
//        }

//		for (var attrname in extraStyle) { style[attrname] = extraStyle[attrname]; }

        return style;
    }

	onChange(user) {
		this.setState({
			user: user
		});
	}

	onPasswordChange(passwords) {
		this.setState({
			password: passwords
		});
	}

	isPasswordValid(password) {
		let regularExpression = /^((?=.*\d).{10,30})/;

		return regularExpression.test(password);
	}

    submit() {

		if((this.state.password.CurrentPassword && !this.isPasswordValid(this.state.password.NewPassword)) || (this.state.password.NewPassword && !this.state.password.CurrentPassword)){
			this.setState({
				showErrors: true
			});

			return false;
		}

		this.props.onSubmit(this.state.user, this.state.password);
    }

    render() {

		let passwordValid = this.isPasswordValid(this.state.password.NewPassword);

		let toggleCurrentPasswordImage = this.state.showCurrentPassword? require('../../images/ic_visibility_black_24dp/web/ic_visibility_black_24dp_2x.png') : require('../../images/ic_visibility_off_black_24dp/web/ic_visibility_off_black_24dp_2x.png');
		let toggleNewPasswordImage = this.state.showNewPassword? require('../../images/ic_visibility_black_24dp/web/ic_visibility_black_24dp_2x.png') : require('../../images/ic_visibility_off_black_24dp/web/ic_visibility_off_black_24dp_2x.png');

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}>

                    {this.props.userError && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 20 }}>{this.props.userError}</Text>}

					{this.state.user && <View style={{ marginBottom: 20 }}>
						<View style={styles.elementContainer}>
							<Text style={styles.label}>Email</Text>
							<TextInput
                                ref='input-1'
								autoCorrect={false}
								style={this.inputValidation()}
								underlineColorAndroid='rgba(0,0,0,0)'
								onChangeText={(email) => this.onChange({ ...this.state.user, email })}
								value={this.state.user.email}
								placeholder={'Email'}
                                returnKeyType={'next'}
                                blurOnSubmit={false}
                                onSubmitEditing={()=> this.refs['input-2'].focus()}
							/>
						</View>

						<View style={styles.elementContainer}>
							<Text style={styles.label}>First name</Text>
							<TextInput
                                ref='input-2'
								autoCorrect={false}
								style={this.inputValidation()}
								underlineColorAndroid='rgba(0,0,0,0)'
								onChangeText={(firstName) => this.onChange({ ...this.state.user, firstName })}
								value={this.state.user.firstName}
								placeholder={'First name'}
                                returnKeyType={'next'}
                                blurOnSubmit={false}
                                onSubmitEditing={()=> this.refs['input-3'].focus()}
							/>
						</View>

						<View style={styles.elementContainer}>
							<Text style={styles.label}>Last name</Text>
							<TextInput
                                ref='input-3'
								autoCorrect={false}
								style={this.inputValidation()}
								underlineColorAndroid='rgba(0,0,0,0)'
								onChangeText={(lastName) => this.onChange({ ...this.state.user, lastName })}
								value={this.state.user.lastName}
								placeholder={'Last name'}
                                returnKeyType={'done'}
							/>
						</View>
					</View>}

                    {this.state.user && <Hr lineColor='#b3b3b3' />}

					{this.state.user && <View style={{ marginTop: 20 }}>
						{passwordValid && this.props.passwordError && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 20 }}>{this.props.passwordError}</Text>}
						{this.state.showErrors && !passwordValid && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 20 }}>Please enter a password which is at least 10 characters, and contains at least one number.</Text>}
						<View style={styles.elementContainer}>
							<Text style={styles.label}>Current password</Text>
							<TextInput
                                ref='input-4'
								autoCorrect={false}
								secureTextEntry={!this.state.showCurrentPassword}
								style={this.inputValidation(this.state.user.password, { paddingRight: 50 })}
								underlineColorAndroid='rgba(0,0,0,0)'
								onChangeText={(CurrentPassword) => this.onPasswordChange({ ...this.state.password, CurrentPassword })}
								value={this.state.password.CurrentPassword}
								placeholder={'Current password'}
                                returnKeyType={'next'}
                                blurOnSubmit={false}
                                onSubmitEditing={()=> this.refs['input-5'].focus()}
							/>
							<TouchableWithoutFeedback onPress={()=> this.setState({ showCurrentPassword: !this.state.showCurrentPassword })}>
								<View style={{ position: 'absolute', top: 30, right: 10}}>
									<Image style={{ width: 30, height: 30 }} source={toggleCurrentPasswordImage} />
								</View>
							</TouchableWithoutFeedback>
						</View>

						<View style={styles.elementContainer}>
							<Text style={styles.label}>New password</Text>
							<TextInput
                                ref='input-5'
								autoCorrect={false}
								secureTextEntry={!this.state.showNewPassword}
								style={this.inputValidation(this.state.user.password, { paddingRight: 50 })}
								underlineColorAndroid='rgba(0,0,0,0)'
								onChangeText={(NewPassword) => this.onPasswordChange({ ...this.state.password, NewPassword })}
								value={this.state.password.NewPassword}
								placeholder={'New password'}
                                returnKeyType={'done'}
							/>
							<TouchableWithoutFeedback onPress={()=> this.setState({ showNewPassword: !this.state.showNewPassword })}>
								<View style={{ position: 'absolute', top: 30, right: 10}}>
									<Image style={{ width: 30, height: 30 }} source={toggleNewPasswordImage} />
								</View>
							</TouchableWithoutFeedback>
						</View>
					</View>}
                </KeyboardAwareScrollView>

                {this.state.user && <View style={styles.bottomButtonWrapper}>
                    <View style={styles.addContentButton}>
						<Button
							diabled
							onPress={this.submit}
							title='Save Details'
							color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
						/>
                    </View>
                </View>}
            </View>
        );
    }
}
