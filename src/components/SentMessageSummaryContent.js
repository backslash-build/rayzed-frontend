import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
    Image
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';

const styles = StyleSheet.create({
    app: {
        flex: 1,
        height: Dimensions.get('window').height,
        ...Platform.select({
            android: {
                paddingBottom: 50
            }
        })
    },
    scrollView: {
        backgroundColor: '#fff',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        padding: 15,
        marginBottom: 70,
        ...Platform.select({
            android: {
                marginTop: 60,
                marginBottom: 80
            }
        })
    },
    pageHeading: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16
    },
    detailWrapper: {
        marginTop: 20,
        marginBottom: 20
    },
    messageDetail: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 10
    },
    detailHeading: {
        width: 80,
        textAlign: 'right',
        marginRight: 20,
        fontWeight: 'bold',
        fontSize: 13
    },
    detailContent: {},
    recipientHeader: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    resultsContainer: {
        marginTop: 20
    },
    result: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginBottom: 10
    },
    resultStatus: {
        marginRight: 10
    },
    reactionImage: {
        height: 15,
        width: 15
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
    },
    cancelMessageButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
});

@autobind
export default class SentMessageSummaryContent extends Component {

    static propTypes = {}

    state = {}

    render() {

        return (
            <View style={styles.app}>
                <ScrollView keyboardShouldPersistTaps={"handled"} style={styles.scrollView}>
                    <View>
                        <View>
                            <Text style={styles.pageHeading}>AI Software to track clients</Text>
                        </View>

                        <View style={styles.detailWrapper}>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Title:</Text>
                                <Text style={styles.detailContent}></Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Recipients:</Text>
                                <Text style={styles.detailContent}></Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Content:</Text>
                                <Text style={styles.detailContent}></Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Contact:</Text>
                                <Text style={styles.detailContent}></Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Message Cost:</Text>
                                <Text style={styles.detailContent}></Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Admin Fee:</Text>
                                <Text style={styles.detailContent}></Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Total Cost:</Text>
                                <Text style={styles.detailContent}></Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.recipientHeader}>Recipients</Text>
                        <View style={styles.resultsContainer}>
                            <View style={styles.result}>
                                <View style={styles.resultStatus}><Image style={styles.reactionImage} source={require('../../images/dislike.png')} /></View>
                                <Text style={styles.resultRecipient}>Tim Cook, Apple Inc.</Text>
                            </View>
                            <View style={styles.result}>
                                <View style={styles.resultStatus}><Image style={styles.reactionImage} source={require('../../images/like.png')} /></View>
                                <Text style={styles.resultRecipient}>Larry Ellison, Oracle</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={styles.bottomButtonWrapper}>
                    <View style={styles.cancelMessageButton}>
                        <Button
                            onPress={()=>this.props.navigator.push(getScene('newMessage'))}
                            title='Cancel unwatched part of message'
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                        />
                    </View>
                </View>
            </View>
        );
    }
}
