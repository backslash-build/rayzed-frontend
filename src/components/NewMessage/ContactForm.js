import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
    KeyboardAvoidingView
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    subheading: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    label: {
        marginTop: 15,
        color: '#808080',
        fontSize: 15
    },
    preFilledInput: {
        fontSize: 17
    },
    formInput: {
        height: 40,
        marginTop: 5,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7
    },
    paymentButton: {
        marginTop: 15,
        borderRadius: 5,
        paddingTop: 3,
        paddingBottom: 3,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1
    },
    submitButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    },
    errorMessage: {
        marginVertical: 25,
        fontSize: 13,
        color: '#fa4f4f',
        textAlign: 'center'
    },
});

@autobind
export default class NewMessageContactForm extends Component {

    static propTypes = {
        onSubmit: React.PropTypes.func,
        onChange: React.PropTypes.func,
        me: React.PropTypes.object,
        message: React.PropTypes.object
    }

    state = {
        company: '',
        website: '',
        phoneNumber: null
    }

    inputValidation(value) {

        let style = {
            height: 40,
            marginTop: 5,
            borderColor: '#d0d0d0',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 7
        };

        if (this.state.showErrors && (!value || value.length === 0)) {
            style.borderColor = '#fa4f4f';
        }

        return style;
    }

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            paddingHorizontal: 15,
            paddingTop: 15,
            ...Platform.select({
                android: {
                    marginTop: 60,
                    marginBottom: 96
                },
                ios: {
                    marginBottom: 66
                }
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

    submit() {

        if (this.props.disableSubmit) {
            return false;
        }

        const {id, organisation, telephone, website} = this.props.message;
        this.props.onSubmit({id, organisation, telephone, website}, () => {
            this.refs.scroll.scrollToPosition(0, 0, false);
        });
    }

    render() {

        let allFieldsComplete = !this.props.message || !this.props.message.organisation || !this.props.message.website || !this.props.message.telephone || (this.props.message.organisation && this.props.message.organisation.length === 0) || (this.props.message.website && this.props.message.website.length === 0) || (this.props.message.telephone && this.props.message.telephone.length === 0);

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    ref='scroll'
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    extraScrollHeight={20}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}>
                    <View>
                        <Text style={styles.subheading}>
                            Details of how your recipient/s should reach you:
                        </Text>
                    </View>
                    {this.state.showErrors && allFieldsComplete && <Text style={styles.errorMessage}>Please complete all fields</Text>}
                    {!this.state.showErrors && !!this.props.error && this.props.error.length > 0 && <Text style={styles.errorMessage}>{this.props.error}</Text>}
                    <View style={styles.titleContainer}>
                        <Text style={styles.label}>Name of sender</Text>
                        <Text style={styles.preFilledInput}>{this.props.me.firstName} {this.props.me.lastName}</Text>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text style={styles.label}>Email of sender</Text>
                        <Text style={styles.preFilledInput}>{this.props.me.email}</Text>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text style={styles.label}>Your company or organisation</Text>
                        <TextInput
                            ref={'input-1'}
                            autoCorrect={false}
                            style={this.inputValidation(this.props.message.organisation)}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(organisation) => this.props.onChange({ organisation })}
                            value={this.props.message && this.props.message.organisation? this.props.message.organisation : ''}
                            placeholder={'Company / Organisation'}
                            blurOnSubmit={ false }
                            returnKeyType={ "next" }
                            onSubmitEditing={()=> this.refs['input-2'].focus()}
                        />
                    </View>
                    <View style={styles.titleContainer}>
                        <Text style={styles.label}>Your website</Text>
                        <TextInput
                            ref={'input-2'}
                            autoCapitalize={false}
                            autoCorrect={false}
                            style={this.inputValidation(this.props.message.website)}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(website) => this.props.onChange({ website })}
                            value={this.props.message && this.props.message.website? this.props.message.website : ''}
                            placeholder={'Website'}
                            keyboardType={'url'}
                            blurOnSubmit={false}
                            returnKeyType={"next"}
                            onSubmitEditing={()=> this.refs['input-3'].focus()}
                        />
                    </View>
                    <View style={{ marginBottom: 40 }}>
                        <Text style={styles.label}>Phone number for recipient to reach you at</Text>
                        <TextInput
                            ref={'input-3'}
                            style={this.inputValidation(this.props.message.telephone)}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(telephone) => this.props.onChange({ telephone })}
                            value={this.props.message? this.props.message.telephone : ''}
                            placeholder={'Phone Number'}
                            keyboardType={'phone-pad'}
                            returnKeyType={"done"}
                            onSubmitEditing={()=> this.submit()}
                        />
                    </View>
                </KeyboardAwareScrollView>

                <View style={styles.bottomButtonWrapper}>
                    <View style={styles.submitButton}>
                        <Button
                            onPress={this.submit}
                            title='Complete message'
                            color={(Platform.OS === 'ios') ? ((this.props.disableSubmit) ? '#ddd' : '#fff') : '#61b746'}
                        />
                    </View>
                </View>
            </View>
        );
    }
}
