import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    Image,
    Linking,
	TouchableWithoutFeedback
 } from 'react-native';
import autobind from 'autobind-decorator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = StyleSheet.create({
    app: {
		position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    label: {
        color: '#808080',
        fontSize: 15,
        marginRight: 10
    },
    elementContainer: {
        marginTop: 15
    },
    roundGreenButton: {
        borderRadius: 20,
        padding: 0,
        width: 40,
        height: 40,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#61b746'
    },
    roundButtonText: {
        color: '#fff',
        fontSize: 25,
        ...Platform.select({
            ios: {
                lineHeight: 25
            },
            android: {
                lineHeight: 15
            }
        })
    },
    durationToggle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    durationInput: {
        height: 40,
        marginTop: 15,
        marginBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        padding: 7,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    addRecipient: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginTop: 20,
        marginBottom: 30
    },
    addRecipientsButton: {
        marginTop: 20,
        marginBottom: 20,
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1
    },
    addContentButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    },
    recipientError: {
        textAlign: 'center',
        color: '#fa4f4f',
        marginTop: 20
    },
    searchResults: {
        marginTop: 20,
        marginBottom: 20,
    },
    recipientResult: {
        paddingTop: 20,
        paddingBottom: 20,
        position: 'relative'
    },
    segmentHeading: {
        marginTop: 10,
    },
    searchResults: {
        marginBottom: 30
    },
    resultsHeading: {
        flex: 3,
        fontSize: 18,
        textAlign: 'left',
        margin: 4,
        padding: 10,
        textAlignVertical: 'center',
    },
    resultRow: {
        paddingTop: 10,
        paddingBottom: 10,
        position: 'relative',
    },
    resultWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    resultName: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 9
    },
    resultIcon: {
        position: 'absolute',
        top: 20,
        right: 0,
        height: 20,
        width: 20
    },
    resultHeading: {
        fontWeight: 'bold',
        fontSize: 13,
        marginLeft: 10,
        width: Dimensions.get('window').width - 115,
        marginRight: 7
    },
    resultSubheading: {
        fontSize: 10,
        color: '#808080',
        fontSize: 10,
        marginLeft: 10,
        width: Dimensions.get('window').width - 115,
    },
    arrowWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 10,
        right: 10
    },
    arrowText: {
        height: 20,
        width: 20
    },
    roundGreenButton: {
        borderRadius: 20,
        padding: 0,
        width: 40,
        height: 40,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#61b746'
    },
});

@autobind
export default class NewMessageForm extends Component {

    static propTypes = {
        onSubmit: React.PropTypes.func,
        onFindRecipient: React.PropTypes.func,
        recipients: React.PropTypes.array,
        error: React.PropTypes.any,
        value: React.PropTypes.object,
        onChange: React.PropTypes.func.isRequired,
        loading: React.PropTypes.bool
    }

    state = {
        showErrors: false
    }

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            paddingHorizontal: 15,
            paddingTop: 15,
            ...Platform.select({
                android: {
                    marginTop: 60,
                    marginBottom: 96
                },
                ios: {
                    marginBottom: 66
                }
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

	openUrl(url) {
        let urlLink = url;

        // has to start with http:// or https://
        if(url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL('http://www.linkedin.com').catch(err => console.error('An error occurred', err));
    }

    inputValidation() {

        let style = {
            height: 40,
            marginTop: 15,
            marginBottom: 15,
            borderColor: '#d0d0d0',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 7
        };

        if (this.state.showErrors && (!this.props.value || this.props.value.subject.length === 0)) {
            style.borderColor = '#fa4f4f';
        }

        return style;
    }

    increaseDuration() {
        if (this.props.value.expiryInDays >= 35) {
            return;
        }

        this.props.onChange({
            ...this.props.value,
            expiryInDays: this.props.value.expiryInDays + 7
        });
    }

    decreaseDuration() {
        if (this.props.value.expiryInDays === 14) {
            return;
        }

        this.props.onChange({
            ...this.props.value,
            expiryInDays: this.props.value.expiryInDays - 7
        });
    }

    findRecipient() {
        this.props.onFindRecipient();
    }

    submit() {

        if (this.props.value.subject.length === 0 || this.props.value.recipients.length === 0) {
            this.setState({ showErrors: true });
        } else {
            const value = {
                ...this.props.value,
                body: 'nothing',
                recipients: this.props.value.recipients.map(a => {
                    let userId = a.userId? a.userId : a.id;
                    return userId;
                })
            };
            this.props.onSubmit(value);
            this.setState({ showErrors: false });
        }
    }

    renderRecipient(result) {

        // let organisation = '';
        // let position = '';
        // if (result.organisations && result.organisations.length > 0) {
        //     organisation = result.organisations[0].organisation;
        //     position = result.organisations[0].position;
        // } else if (result.organisation) {
        //     let organisationArray = result.organisation.split(', ');
        //     organisation = organisationArray[0];
        //     position = organisationArray[1];
        // }

        let userId = result.userId? result.userId : result.id;

        return (
            <View
            key={'sent-'+result.id}
            style={styles.resultRow} >
                <View
                    key={'sent-'+result.id}
                    style={styles.resultRow}>
                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                        <TouchableHighlight
                        style={styles.roundGreenButton}
                        underlayColor={'rgba(119, 221, 68, 0.72)'}
                        onPress={() => {
                            this.props.removeRecipient(result);
                        }}
                        underlayColor={'#FFF'}>
                            <Text style={styles.roundButtonText}>-</Text>
                        </TouchableHighlight>
                        <View style={styles.resultWrapper}>
                            <View>
                                <Text style={styles.resultHeading}>{result.name} <Text>(£{result.donation})</Text></Text>
                                <Text style={styles.resultSubheading}>
                                    <Text>
                                        {result.organisations? result.organisations.join('; ') : result.organisation}
                                    </Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    {false && <TouchableWithoutFeedback onPress={()=> this.openUrl('www.linkedin.com')}>
                        <Image source={require('../../../images/linkedin.png')} style={styles.resultIcon} />
                    </TouchableWithoutFeedback>}
                </View>
            </View>
        );
    }

    render() {
        let recipients = this.props.recipients || [];

        let nearestWeek = 7.0*Math.round(this.props.value.expiryInDays/7.0);
        let expiryInDays = (nearestWeek / 7) + " Week";
        if ((nearestWeek / 7) > 1) expiryInDays += "s";
        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}>
                    <View style={styles.elementContainer}>
                        <Text style={styles.label}>Message Title (Displayed to recipient)</Text>
                        <TextInput
                            autoCorrect={false}
                            style={this.inputValidation()}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(subject) => this.props.onChange({ ...this.props.value, subject })}
                            value={this.props.value.subject}
                            placeholder={'Message title'}
                            maxLength = {100}
                            returnKeyType='done'
                        />
                    </View>

					{false && <View style={styles.elementContainer}>
                        <Text style={styles.label}>Must be viewed in how many weeks</Text>
                        <View style={styles.durationToggle}>
                            <TouchableHighlight onPress={this.decreaseDuration} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                                <Text style={styles.roundButtonText}>-</Text>
                            </TouchableHighlight>
                            <View style={styles.durationInput}>
                                   <Text>{expiryInDays}</Text>
                            </View>
                            <TouchableHighlight onPress={this.increaseDuration} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                                <Text style={styles.roundButtonText}>+</Text>
                            </TouchableHighlight>
                        </View>
                    </View>}

                    <View style={styles.elementContainer}>
                        <Text style={styles.label}>Who do you want to send this message to?</Text>

                        <TouchableHighlight onPress={this.findRecipient} style={styles.addRecipientsButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                            <View>
                                <Button
                                    onPress={this.findRecipient}
                                    title='Choose Recipients'
                                    color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                                />
                            </View>
                        </TouchableHighlight>

                        {recipients.length > 0 && <View style={styles.searchResults}>
                            {recipients.map(this.renderRecipient)}
                        </View>}

                        {this.props.value.recipients.length === 0 && this.state.showErrors && <Text style={styles.recipientError}>Please select recipient/s</Text>}
                        {this.props.value.recipients.length > 0 && this.props.value.subject.length == 0 && this.state.showErrors && <Text style={styles.recipientError}>Please complete all fields</Text>}

                        {false && <View style={styles.addRecipient}>
                            <Text style={styles.label}>Add additional recipient</Text>
                            <TouchableHighlight onPress={this.findRecipient} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                                <Text style={styles.roundButtonText}>+</Text>
                            </TouchableHighlight>
                        </View>}
                    </View>
                </KeyboardAwareScrollView>

                <View style={styles.bottomButtonWrapper}>
                    <View style={styles.addContentButton}>
                        {this.props.loading? (
                            <Button
                                diabled
                                onPress={()=>{}}
                                title='Saving...'
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                            />
                        ):(
                            <Button
                                onPress={this.submit}
                                title='Save and go to message content'
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                            />
                        )}
                    </View>
                </View>
            </View>
        );
    }
}
