import React, { Component } from 'react';
import {
    View,
    Text,
    Platform,
    StyleSheet,
    Button,
    Dimensions,
    ScrollView,
    Image
 } from 'react-native';
import autobind from 'autobind-decorator';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    scrollView: {
		backgroundColor: '#fff',
		height: Dimensions.get('window').height,
		paddingHorizontal: 15,
		paddingTop: 15,
		...Platform.select({
			android: {
                marginTop: 60,
				marginBottom: 96
			},
			ios: {
				marginBottom: 66
			}
		})
    },
    logo: {
        marginTop: 20,
        marginBottom: 20,
        width: 260,
		height: 60
    },
    subheading: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 17,
        marginTop: 15
    },
    hopeMessage: {
        fontWeight: 'bold',
        marginTop: 20
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
    },
    submitPaymentButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    },
});

@autobind
export default class NewMessageRaisedScene extends Component {

    static propTypes = {
        onSubmit: React.PropTypes.func,
    }

    state = {}

    submit() {
        this.props.onSubmit();
    }

    render() {

        return (
            <View style={styles.app}>
                <ScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    style={styles.scrollView}>
                    <View style={{flex:1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
						<View>
                        	<Text style={styles.subheading}>Your message has been</Text>
                        	<Image style={styles.logo} source={require('../../../images/Raised-with-green-stopwatch.png')} />
						</View>
                    </View>

                    <View>

                        <Text>The recipient/s have been sent a notification about your message. Your card will only be billed when they watch the message. 100% of the viewing fee will go to their chosen charity.
                        {"\n"}{"\n"}The recipient/s have up to 28 days to watch your message and you will be emailed when they do so, including any feedback they have given.
                        {"\n"}{"\n"}<Text style={styles.hopeMessage}>The Raised Team wishes you the best of luck. Thank you for using Raised.</Text></Text>
                    </View>
                </ScrollView>

                <View style={styles.bottomButtonWrapper}>
                    <View style={styles.submitPaymentButton}>
                        <Button
                            onPress={this.submit}
                            title='Ok'
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                        />
                    </View>
                </View>
            </View>
        );
    }
}
