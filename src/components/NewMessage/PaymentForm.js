import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
    Linking,
    WebView
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';

import LoadingScene from '../../containers/LoadingScene';

const styles = StyleSheet.create({
    app: {
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                paddingBottom: 50,
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64,
            }
        })
    },
});

@autobind
export default class NewMessagePaymentForm extends Component {

    static propTypes = {
        attachment: React.PropTypes.string
    }

    state = {
        pageLoading: true
    }

    onNavigationStateChange(navState) {
        console.log("URL", navState.url);
        if (navState.url.indexOf("https://inbox.raised.global/messages/paymentResponse") != -1) {
            this.props.onPageChange();
        }
    }

    render() {

        let paymentUrl = this.props? this.props.paymentUrl + "&header=none" : '';

        console.log(paymentUrl);

        return (
            <View style={styles.app}>
                {this.state.pageLoading && <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height}}><LoadingScene /></View>}
                <WebView
                    automaticallyAdjustContentInsets={false}
                    contentInset={{ top: 0 }}
                    scalesPageToFit={true}
                    onNavigationStateChange={this.onNavigationStateChange}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    source={{
                        uri: paymentUrl,
                    }}
                    onError={(error)=> console.log("Error", error)}
                    onLoadEnd={()=> { this.setState({ pageLoading: false }); console.log("Loading end"); } }
                    onLoad={()=> { this.setState({ pageLoading: false }); console.log("Loading end"); } }
                  />
            </View>
        );
    }
}
