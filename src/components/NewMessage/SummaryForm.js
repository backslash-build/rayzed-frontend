import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
	SegmentedControlIOS
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import stripe from 'tipsi-stripe';

import PaymentForm from './PaymentForm';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    scrollView: {
		backgroundColor: '#fff',
		height: Dimensions.get('window').height,
		paddingHorizontal: 15,
		paddingTop: 15,
		...Platform.select({
			android: {
                marginTop: 60,
				marginBottom: 80
			},
			ios: {
				marginBottom: 66
			}
		})
    },
    label: {
        color: '#808080',
        fontSize: 13,
        marginRight: 10,
		marginBottom: 5
    },
    subheading: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    detailWrapper: {
        marginTop: 20,
        marginBottom: 20
    },
    messageDetail: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 10
    },
    detailHeading: {
        width: 80,
        textAlign: 'right',
        marginRight: 20,
        fontSize: 13
    },
    detailContent: {
        fontWeight: 'bold',
        width: Dimensions.get('window').width - 130,
    },
	multiDetailContent: {
        fontWeight: 'bold',
	},
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
    },
    submitPaymentButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    },
});

@autobind
export default class NewMessageSummaryForm extends Component {

    static propTypes = {
        items: React.PropTypes.array,
        summary: React.PropTypes.object,
        onSubmit: React.PropTypes.func
    }

    state = {
		selectedIndex: 0
	}

    async applePay() {

        const applePaySupport = await stripe.deviceSupportsApplePay();

        console.log("APPLE PAY:", applePaySupport);

        if (applePaySupport) {
            const token = await stripe.paymentRequestWithApplePay(this.props.items, options);
        } else {
            const token = await stripe.paymentRequestWithCardForm(options);
        }
    }

    async androidPay() {
        const androidPaySupport = await stripe.deviceSupportsAndroidPay();

        console.log("ANDROID PAY:", androidPaySupport);

        if (androidPaySupport) {
            const token = await stripe.paymentRequestWithAndroidPay(this.props.items, options);
        } else {
            const token = await stripe.paymentRequestWithCardForm(options);
        }
    }

    completePayment() {
//        if (Platform.OS === 'ios') {
//            this.applePay();
//        } else if (Platform.OS === 'android') {
//            this.androidPay();
//        }
        this.props.onSubmit();
    }

	renderRecipients() {

		return this.props.summary.recipients.map(function (recipient, index) {

			let recipientText = recipient.name;

			if (recipient.organisation != null) {
				 recipientText += ', ' + recipient.organisation;
			}

			return (<Text key={'recipient-'+index} numberOfLines={1} style={styles.multiDetailContent}>{recipientText}</Text>)
		});
	}

	renderContact() {

		return this.props.summary.contact.map(function (detail, index) {

			return (<Text key={'contact-'+index} numberOfLines={1} style={styles.multiDetailContent}>{detail}</Text>)
		});
	}

	renderContent() {

		if (!this.props.summary) {
			return false;
		}

		return this.props.summary.attachments.map(function (attachment, index) {

			return (<Text key={'attachment-'+index} numberOfLines={1} style={styles.multiDetailContent}>{attachment.title}</Text>)
		});
	}

    render() {

        return (
            <View style={styles.app}>
				{<ScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    style={styles.scrollView}>
                    <View>
                        <Text style={styles.subheading}>Message Summary</Text>

                        <View style={styles.detailWrapper}>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Title:</Text>
                                <Text style={styles.detailContent}>{this.props.summary.title}</Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Recipients:</Text>
								<View style={styles.detailContent}>
									{this.renderRecipients()}
								</View>
                            </View>
							<View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Content:</Text>
								<View style={styles.detailContent}>
									{this.renderContent()}
								</View>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Contact:</Text>
								<View style={styles.detailContent}>
									{this.renderContact()}
								</View>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Cost:</Text>
                                <Text style={styles.detailContent}>{this.props.summary.messageCost}</Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Admin Fee:</Text>
                                <Text style={styles.detailContent}>{this.props.summary.adminFee}</Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Total Cost:</Text>
                                <Text style={styles.detailContent}>{this.props.summary.totalCost}</Text>
                            </View>
                        </View>

						{false && <Text style={styles.label}>Payment method</Text>}
						{this.props.error && <Text style={{ textAlign: 'center', color: 'red', marginVertical: 15 }}>{this.props.error}</Text>}
						{false && <SegmentedControlIOS
					  		values={['Apple Pay', 'Credit card via website']}
						  	selectedIndex={this.state.selectedIndex}
						  	onChange={(event) => {
								this.setState({selectedIndex: event.nativeEvent.selectedSegmentIndex});
						  	}}
						/>}
                    </View>
                </ScrollView>}

				{<View style={styles.bottomButtonWrapper}>
                    <View style={styles.submitPaymentButton}>
                        <Button
                            onPress={this.completePayment}
                            title={`Upload Pitch (total cost ${this.props.summary.totalCost})`}
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                            titleColor='#fff'
                        />
                    </View>
                </View>}

				{false && this.props.paymentUrl && <PaymentForm paymentUrl={this.props.paymentUrl} />}
            </View>
        );
    }
}
