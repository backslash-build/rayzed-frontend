import React, { Component } from 'react';
import {
    NativeModules,
    View,
    Text,
    Platform,
    StyleSheet,
    Button,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    TextInput,
    KeyboardAvoidingView
 } from 'react-native';
import autobind from 'autobind-decorator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import VideoPlayer from '../VideoPlayer';
import CameraPicker from '../CameraPicker';

const DocumentPicker = NativeModules.RNDocumentPicker;
const FilePickerManager = NativeModules.FilePickerManager;

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    explanationText: {
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        color: '#808080',
        fontSize: 15
    },
    errorMessage: {
        marginTop: 15,
        fontSize: 13,
        color: '#fa4f4f',
        textAlign: 'center'
    },
    videoWrapper: {},
    pickerButton: {
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#539fe0'
            }
        })
    },
    selectedFiles: {
        marginTop: 20,
		marginBottom: 30
    },
    additionalFileName: {
        height: 40,
        fontSize: 16,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7,
        width: Dimensions.get('window').width - 85
    },
    uploadedFileName: {
        fontSize: 16,
        marginLeft: 20,
        width: Dimensions.get('window').width - 30,
    },
    attachedFileName: {
        fontSize: 16,
        paddingLeft: 7,
        width: Dimensions.get('window').width - 85,
    },
    additionalButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    additionalButton: {
        borderRadius: 5,
        padding: 3,
        marginTop: 20,
        marginLeft: 5,
        marginRight: 5,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    fileWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        height: 40
    },
    roundGreenButton: {
        borderRadius: 20,
        padding: 0,
        marginLeft: 15,
        marginRight: 10,
        width: 40,
        height: 40,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#61b746'
    },
    roundButtonText: {
        color: '#fff',
        fontSize: 25,
        lineHeight: 25
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1
    },
    addContactButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        width: Dimensions.get('window').width - 30,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    }
});

@autobind
export default class NewMessageContentForm extends Component {

    static propTypes = {
        onSubmit: React.PropTypes.func,
        onUploadAllFiles: React.PropTypes.func,
        attachments: React.PropTypes.array,
        onDeleteAttachment: React.PropTypes.func
    }

    state = {
        showCameraPicker: false,
        chosenVideo: null,
        selectedVideo: {},
        selectedFiles: [],
        videoErrorMessage: '',
        additionalErrorMessage: ''
    }

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            ...Platform.select({
                android: {
                    marginTop: 55,
                    marginBottom: 96
                },
                ios: {
                    marginBottom: 80
                }
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

    chooseMainAsset() {
        if (Platform.OS === "ios") {
            this.setState({ showMainAssetPicker: true });
        } else if (Platform.OS === "android") {
            FilePickerManager.showFilePicker(null, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    console.log('User cancelled file picker');
                } else if (response.error) {
                    console.log('FilePickerManager Error: ', response.error);
                } else {

                    let fileExtension = response.path.split('.').pop();

                    if (fileExtension == "3gp" || fileExtension == "mp4" || fileExtension == "ts" || fileExtension == "webm" || fileExtension == "mkv" || fileExtension == "mov") {

                        this.setState({
                            selectedVideo: response,
                            videoErrorMessage: '',
                            loading: false
                        });
                    } else {
                        this.setState({ videoErrorMessage: 'Main asset has to be a video file.'});
                    }
                }
            });
        }
    }

    playSelectedVideo(videos, current) {
        this.setState({
            showMainAssetPicker: false,
            selectedVideo: current,
            videoErrorMessage: ''
        });
    }

    chooseAdditionalVideo() {

        let totalFiles = this.state.selectedFiles.length + this.props.attachments.length;

        if (totalFiles < 3) {
            if (Platform.OS == "ios") {
                this.setState({ showAdditionalVideoPicker: true });
            } else if (Platform.OS == "android") {
                FilePickerManager.showFilePicker(null, (response) => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled file picker');
                    } else if (response.error) {
                        console.log('FilePickerManager Error: ', response.error);
                    } else {

                        if (response.didCancel) {
                            console.log('User cancelled file picker');
                        } else if (response.error) {
                            console.log('FilePickerManager Error: ', response.error);
                        } else {

                            let fileExtension = response.path.split('.').pop();

                            if (fileExtension == "3gp" || fileExtension == "mp4" || fileExtension == "ts" || fileExtension == "webm" || fileExtension == "mkv" || fileExtension == "mov") {

                                let selectedFiles = this.state.selectedFiles;

                                response.filename = response.path.split("/").pop().replace(/\..+$/, '');
                                response.extension = fileExtension;

                                selectedFiles.push(response);

                                this.setState({
                                    showAdditionalVideoPicker: false,
                                    selectedFiles: selectedFiles
                                });

                            } else {
                                this.setState({ additionalErrorMessage: 'Selected file was not a video file.' });
                            }
                        }
                    }
                });
            }
        } else {
            this.setState({ additionalErrorMessage: 'No more than 3 additional assets can be added.' });
        }
    }

    addAdditionalVideo(videos, current) {

        current.extension = current.filename.split('.').pop();

        let selectedFiles = this.state.selectedFiles;

        selectedFiles.push(current);

        current.filename = current.filename.replace(/\..+$/, '');

        this.setState({
            showAdditionalVideoPicker: false,
            selectedFiles: selectedFiles
        });
    }

    chooseDocument() {

        let totalFiles = (this.state.selectedFiles? this.state.selectedFiles.length : 0) + (this.props.attachments? this.props.attachments.length : 0);

        if (totalFiles < 3) {
            if (Platform.OS === 'ios') {
                DocumentPicker.show({
                    filetype: ['com.adobe.pdf']
                }, (error, url) => {

                    console.log(url);

                    if (url.fileSize < 104857600) {
                        let selectedFiles = this.state.selectedFiles;

                        url.filename = url.fileName.replace(/\..+$/, '');
                        url.extension = url.fileName.split('.').pop();

                        selectedFiles.push(url);

                        this.setState({
                            showAdditionalVideoPicker: false,
                            selectedFiles: selectedFiles,
                            additionalErrorMessage: ''
                        });
                    } else {
                        this.setState({
                            additionalErrorMessage: 'File size is over the limit of 100MB'
                        });
                    }
                });
            } else if (Platform.OS === 'android') {
                FilePickerManager.showFilePicker(null, (response) => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled file picker');
                    } else if (response.error) {
                        console.log('FilePickerManager Error: ', response.error);
                    } else {
                        let selectedFiles = this.state.selectedFiles;

                        response.filename = response.path.split('/').pop().replace(/\..+$/, '');
                        response.extension = response.path.split('.').pop();

                        selectedFiles.push(response);

                        this.setState({
                            showAdditionalVideoPicker: false,
                            selectedFiles: selectedFiles,
                            additionalErrorMessage: ''
                        });
                    }
                });
            }
        } else {
            this.setState({ additionalErrorMessage: 'No more than 3 additional assets can be added.' });
        }
    }

    changeFilename(value, index) {
        let selectedFiles = this.state.selectedFiles;
        selectedFiles[index].filename = value;

        this.setState({
            selectedFiles: selectedFiles
        })
    }

    buttonStyle(colour, disabled, disabledColour) {

        let selectedColour = colour;
        if(disabled) {
            selectedColour = disabledColour;
        }

        let style = {
            paddingTop: 5,
            paddingBottom: 5,
            borderRadius: 5,
            overflow: 'hidden',
            alignSelf: 'stretch',
            ...Platform.select({
                ios: {
                    backgroundColor: selectedColour
                },
                android: {
                    marginBottom: 30
                }
            })
        };

        return style;
    }

    removeFile(fileIndex) {
        let selectedFiles = this.state.selectedFiles;

        selectedFiles.splice(fileIndex, 1);

        this.setState({
            selectedFiles: selectedFiles,
            additionalErrorMessage: ''
        });
    }

    renderAdditionalFiles() {

        let selectedFiles = [];

        this.state.selectedFiles.map((file, index) => {
            selectedFiles.push(<View key={'file-'+index} style={styles.fileWrapper}>
                <TouchableHighlight onPress={()=> this.removeFile(index)} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                    <Text style={styles.roundButtonText}>-</Text>
                </TouchableHighlight>
                <TextInput
                    autoCorrect={false}
                    onChangeText={(value) => this.changeFilename(value, index)}
                    style={styles.additionalFileName}
                    value={file.filename}/>
            </View>);
        });

        return selectedFiles;
    }

    renderAttachedFiles() {
        let attachedFiles = [];

        if (this.props.uploadedFiles.length > 0) {
            this.props.uploadedFiles.map((attachment, index) => {
                attachedFiles.push(<View key={'file-'+index} style={styles.fileWrapper}>
                    <Text numberOfLines={1} style={styles.uploadedFileName}>{attachment.filename}</Text>
                </View>);
            });
        }

        if (this.props.attachments && this.props.attachments !== true) {
            this.props.attachments.map((attachment, index) => {
                attachedFiles.push(<View key={'file-'+index} style={styles.fileWrapper}>
                    <TouchableHighlight onPress={()=> this.props.onDeleteAttachment(attachment.id)} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                        <Text style={styles.roundButtonText}>-</Text>
                    </TouchableHighlight>
                    <Text numberOfLines={1} style={styles.attachedFileName}>{attachment.title}</Text>
                </View>);
            });
        }

        return attachedFiles;
    }

    onVideoEnd() {}

    showErrorMessage() {
        this.setState({
            videoErrorMessage: 'The selected video is longer than 60 seconds.',
            selectedVideo: {}
        });
    }

    uploadAllFiles() {

        this.props.onUploadAllFiles(this.state.selectedVideo, this.state.selectedFiles);
        this.setState({
            uploading: true,
            uploadedVideo: this.state.selectedVideo,
            uploadedFiles: this.state.selectedFiles,
            selectedVideo: {},
            selectedFiles: []
        });
    }

    submit() {
        if ((this.state.selectedVideo && this.props.defaultVideoUrl === true) || (this.props.defaultVideoUrl && this.props.defaultVideoUrl.length > 0 && this.state.videoErrorMessage.length == 0)) {
            this.props.onSubmit();
        } else if (this.state.videoErrorMessage.length == 0) {
            this.setState({ videoErrorMessage: 'A main message asset is required.'});
        }
    }

    renderBottomButton() {

        if (this.props.uploadingText.length > 0) {
            return (<View style={this.buttonStyle('#61b746', true, '#fff')}>
                <Button
                    disabled={true}
                    onPress={this.submit}
                    title={this.props.uploadingText}
                    color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                />
            </View>);
        } else if ((this.props.defaultVideoUrl && this.state.selectedFiles.length > 0) || this.state.selectedVideo.uri) {
            return (<View style={this.buttonStyle('#61b746', ((!this.props.defaultVideoUrl && !this.state.selectedVideo) || this.state.loading), 'rgba(119, 221, 68, 0.5)')}>
                <Button
                    disabled={(!this.props.defaultVideoUrl && !this.state.selectedVideo) || this.state.loading}
                    onPress={this.uploadAllFiles}
                    title={'Upload files'}
                    color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                />
            </View>);
        } else {
            return (<View style={this.buttonStyle('#61b746', ((!this.props.defaultVideoUrl && !this.state.selectedVideo) || this.state.loading), 'rgba(119, 221, 68, 0.5)')}>
                <Button
                    disabled={(!this.props.defaultVideoUrl && !this.state.selectedVideo) || this.state.loading}
                    onPress={this.submit}
                    title={'Save and go to message contact'}
                    color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                />
            </View>);
        }
    }

    render() {

        let selectedVideoUrl = null;
        if (this.state.selectedVideo.uri) {
            selectedVideoUrl = this.state.selectedVideo.uri;
        } else if (this.props.uploadedVideo.uri) {
            selectedVideoUrl = this.props.uploadedVideo.uri;
        } else if (this.props.defaultVideoUrl && this.props.defaultVideoUrl.length > 0) {
            selectedVideoUrl = this.props.defaultVideoUrl;
        }

        let selectedFiles = this.renderAdditionalFiles();
        let attachedFiles = this.renderAttachedFiles();

        let totalFiles = 0;
        if (this.state.selectedFiles) {
            totalFiles += this.state.selectedFiles.length;
        }
        if (this.props.attachments) {
            totalFiles += this.props.attachments.length;
        }

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}>
                    <View style={styles.videoWrapper}>
                        <VideoPlayer showSlider={true} videoUrl={selectedVideoUrl} onVideoEnd={this.onVideoEnd} onError={this.showErrorMessage} lengthLimit defaultMessage="Please select a video" />
                        <Text style={styles.explanationText}>Main message needs to be an MP4 video clip no longer than 60 seconds.</Text>
                        {this.state.videoErrorMessage.length > 0 && <Text style={styles.errorMessage}>{this.state.videoErrorMessage}</Text>}
                        <View style={styles.pickerButton}>
                            <Button
                                onPress={this.chooseMainAsset}
                                title='Change Video for Main Message'
                                color={(Platform.OS === 'ios') ? '#fff' : '#539fe0'}
                            />
                        </View>
                    </View>

                    <View style={styles.selectedFiles}>
                        <Text style={styles.explanationText}>Add up to 3 pdf files for your recipient to read if they like your message (size limit 100MB).</Text>
                        {this.state.additionalErrorMessage.length > 0 && <Text style={styles.errorMessage}>{this.state.additionalErrorMessage}</Text>}
                        {attachedFiles}
                        {selectedFiles}
                        {totalFiles < 3 && <View style={styles.additionalButtons}>
                            <View style={styles.additionalButton}>
                                <Button
                                    onPress={this.chooseDocument}
                                    title='Add PDF'
                                    color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                                />
                            </View>
                        </View>}
                    </View>
                </KeyboardAwareScrollView>

                <View style={styles.bottomButtonWrapper}>
                    {this.renderBottomButton()}
                </View>

                {this.state.showMainAssetPicker && <CameraPicker hidePicker={()=> this.setState({ showMainAssetPicker: false })} handleSelectedVideo={this.playSelectedVideo} selectedVideo={this.state.selectedVideo} />}

                {this.state.showAdditionalVideoPicker && <CameraPicker hidePicker={()=> this.setState({ showAdditionalVideoPicker: false })} handleSelectedVideo={this.addAdditionalVideo} />}
            </View>
        );
    }
}
