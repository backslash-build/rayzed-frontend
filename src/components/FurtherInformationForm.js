import React, { Component } from 'react';
import {
    View,
    Text,
    Platform,
    StyleSheet,
    Button,
    Dimensions,
    ScrollView,
    Linking,
    Image
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const Entities = require('html-entities').XmlEntities;
const entities = new Entities();

import VideoPlayer from './VideoPlayer';

const styles = StyleSheet.create({
    app: {
		position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    scrollView: {
        backgroundColor: '#fff',
		height: Dimensions.get('window').height,
		...Platform.select({
			android: {
                marginTop: 55
			}
		})
    },
    pageHeader: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 30
    },
    viewDocumentButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 15,
        marginBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#539fe0'
            }
        })
    },
    detailWrapper: {
        marginTop: 20,
        marginBottom: 20
    },
    messageDetail: {
        marginBottom: 10,
        flexDirection: 'row',
    },
    detailHeading: {
        width: 110,
        textAlign: 'right',
        marginRight: 20,
        fontSize: 15
    },
    detailContent: {
        fontWeight: 'bold',
		maxWidth: Dimensions.get('window').width - 140,
    },
    detailLink: {
        width: Dimensions.get('window').width - 140,
        color: '#539fe0',
        textDecorationLine: 'underline',
		fontWeight: 'bold'
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
    },
    emailDocumentsButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    pageHeading: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16
    },
    recipientsWrapper: {
        marginTop: 30,
        marginLeft: 15,
        marginBottom: 30
    },
    recipientHeader: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    resultsContainer: {
        marginTop: 20
    },
    result: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: 10
    },
    resultStatus: {
        marginRight: 10
    },
    reactionImage: {
        height: 15,
        width: 15
    },
});

@autobind
export default class FurtherInformationForm extends Component {

    static propTypes = {
        message: React.PropTypes.object,
        videoUrl: React.PropTypes.string
    }

    state = {
        selectedVideo: '',
		showVideo: false
    }

    openUrl(url) {

        let urlLink = url;

        // has to start with http:// or https://
        if(url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
    }

    sendEmail(email) {

        Linking.openURL('mailto:'+this.props.message.sender.email+'?subject='+this.props.message.subject+'&body=Dear '+this.props.message.sender.firstName+' '+this.props.message.sender.lastName+', I\'ve just watched your message \''+this.props.message.subject+'\' on the Raised app.').catch(err => console.error('An error occurred', err));
    }

    renderAttachments(){

        if (!this.props.attachments || this.props.attachments === true || this.props.attachments == 0) {
            return (<View></View>);
        }

        let that = this;
        let attachments = [];

        this.props.attachments.map(function (attachment, index) {
            attachments.push(<View key={'attachment-'+index} style={styles.viewDocumentButton}>
                <Button
                    onPress={()=>that.props.viewFile(attachment)}
                    title={attachment.title}
                    color={(Platform.OS === 'ios') ? '#fff' : '#539fe0'}
                />
            </View>);
        });

        return attachments;
    }

    renderRecipients() {

        if (!this.props.message.recipients || this.props.message.recipients.length == 0) {
            return (<View></View>);
        }

        let that = this;
        let recipients = [];

        this.props.message.recipients.map(function (recipient, index) {

            let reactionImage = null;
            if (recipient.liked === false) {
                reactionImage = require('../../images/neutral.png');
            } else if (recipient.liked === true) {
                reactionImage = require('../../images/like.png');
            }

            recipients.push(<View key={"reaction-"+index}>
                <View style={styles.result}>
                    <View style={styles.resultStatus}>
                        {reactionImage !== null && <Image style={styles.reactionImage} source={reactionImage} />}
                        {reactionImage === null && <Text style={{ fontWeight: 'bold' }}>Not watched</Text>}
                    </View>
					<View>
						<Text style={styles.resultRecipient}>{recipient.name}</Text>
						<Text style={{ fontSize: 12 }}>{recipient.organisation}</Text>
					</View>
                </View>
				{!!recipient.comments && recipient.comments.length > 0 && <View>
                    <Text style={{ marginBottom: 10, fontStyle: 'italic' }}>"{entities.decode(recipient.comments)}"</Text>
                </View>}
            </View>);
        });

        return recipients;
    }

    render() {

        let selectedVideoUrl = null;
        if (this.props.videoUrl && this.props.videoUrl !== true) {
            selectedVideoUrl = this.props.videoUrl;
        }

        //selectedVideoUrl = 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8';
        selectedVideoUrl = 'https://raisedtestmedia.streaming.mediaservices.windows.net/f015a8b6-236a-4823-bc66-5fe45c05359f/Video.ism/Manifest(format=m3u8-aapl)';

        let messageCost = 0;
        let adminCost = 0;

        if (this.props.invoice) {
            messageCost = this.props.invoice.totalDonations;
            adminCost = this.props.invoice.totalLineItems;
        }

        let strippedWebsite = (this.props.message && this.props.message.website)? this.props.message.website.replace("https://", "").replace("http://", "") : "";

        return (
            <View style={styles.app}>
                {this.props.message && (
                    <KeyboardAwareScrollView
                        automaticallyAdjustContentInsets={false}
                        style={styles.scrollView}
                        keyboardShouldPersistTaps="handled">

						{(this.state.showVideo || this.props.message.recipients) && <VideoPlayer onVideoEnd={()=> this.setState({ showFeedbackModal: true })} showSlider={false} videoUrl={selectedVideoUrl} onError={this.showErrorMessage} initialLoading />}

                        {this.props.error && <Text style={{ textAlign: 'center', color: 'red', marginTop: 20 }}>{this.props.error}</Text>}

                        <Text style={styles.pageHeader}>{this.props.message.subject}</Text>

                        {this.props.message.sender && <View style={styles.detailWrapper}>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Contact:</Text>
                                <Text numberOfLines={1} style={styles.detailContent}>
                                    {this.props.message.sender.firstName} {this.props.message.sender.lastName}
                                </Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}></Text>
                                <Text numberOfLines={1} style={styles.detailContent}>{this.props.message.organisation}</Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Email:</Text>
                                <Text onPress={this.sendEmail} numberOfLines={1} style={styles.detailLink}>{this.props.message.sender.email}</Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Website:</Text>
                                <Text
                                    onPress={()=> this.openUrl(this.props.message.website)}
                                    numberOfLines={1}
                                    style={styles.detailLink}>
                                        {strippedWebsite}
                                </Text>
                            </View>
                            <View style={styles.messageDetail}>
                                <Text style={styles.detailHeading}>Phone:</Text>
                                <Text numberOfLines={1} style={styles.detailContent}>{this.props.message.telephone}</Text>
                            </View>
                        </View>}

						{!this.state.showVideo && !this.props.message.recipients && <View key={'video-button'} style={styles.viewDocumentButton}>
							<Button
								onPress={()=>this.setState({ showVideo: true })}
								title={'Re-watch message'}
								color={(Platform.OS === 'ios') ? '#fff' : '#539fe0'}
							/>
						</View>}

                        <View>{this.renderAttachments()}</View>

                        {this.props.message && this.props.message.recipients && this.props.invoice && <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
							<View>
								<View style={styles.detailWrapper}>
									<View style={styles.messageDetail}>
										<Text style={styles.detailHeading}>Message Cost:</Text>
										<Text style={styles.detailContent}>£{messageCost}</Text>
									</View>
									<View style={styles.messageDetail}>
										<Text style={styles.detailHeading}>Admin Fee:</Text>
										<Text style={styles.detailContent}>£{adminCost}</Text>
									</View>
									<View style={styles.messageDetail}>
										<Text style={styles.detailHeading}>Total Cost:</Text>
										<Text style={styles.detailContent}>{this.props.invoice && <Text style={styles.detailContent}>£{this.props.invoice.total}</Text>}</Text>
									</View>
								</View>
							</View>
                        </View>}

                        {this.props.message.recipients && <View style={styles.recipientsWrapper}>
                            <Text style={styles.recipientHeader}>Recipients</Text>
                            <View style={styles.resultsContainer}>
                                {this.renderRecipients()}
                            </View>
                        </View>}
                    </KeyboardAwareScrollView>
                )}

                {false && <View style={styles.bottomButtonWrapper}>
                    <View style={styles.emailDocumentsButton}>
                        <Button
                            onPress={this.doSomethingHere}
                            title='Email documents to a colleague'
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                        />
                    </View>
                </View>}
            </View>
        );
    }
}
