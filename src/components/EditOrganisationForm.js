import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    KeyboardAvoidingView,
    Image
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import Hr from 'react-native-hr';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            ios: {
                height: Dimensions.get('window').height - 64,
            },
            android: {
                height: Dimensions.get('window').height - 25
            }
        })
    },
    label: {
        color: '#808080',
        fontSize: 15,
        marginRight: 10
    },
    indentedLabel: {
        color: '#808080',
        fontSize: 15,
        marginLeft: 10
    },
    formInput: {
        height: 40,
        marginTop: 5,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7
    },
	indentedFormInput: {
        height: 40,
        marginTop: 5,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        marginRight: 50,
		paddingLeft: 7
	},
	positionField: {
		marginTop: 10
	},
    elementContainer: {
        marginTop: 15
    },
    organisationWrapper: {
        marginTop: 10,
        marginBottom: 25
    },
    roundGreenButton: {
        borderRadius: 20,
        padding: 0,
        width: 40,
        height: 40,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#61b746'
    },
    roundButtonText: {
        color: '#fff',
        fontSize: 25,
        ...Platform.select({
            ios: {
                lineHeight: 25
            },
            android: {
                lineHeight: 15
            }
        })
    },
    addOrganisation: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginTop: 10,
        marginBottom: 30
    },
    removeOrganisation: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginTop: 10,
        marginBottom: 30,
		position: 'absolute',
		right: 0,
		top: 10
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
        backgroundColor: '#fff'
    },
	firstSubmitButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
		marginBottom: 10,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    submitButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    recipientError: {
        textAlign: 'center',
        color: '#fa4f4f',
        marginTop: 20
    },
    linkedInLogin: {
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: 'center'
    },
    linkedInIcon: {
        width: 30,
        height: 30,
        marginRight: 10
    },
    linkedInLink: {
        color: '#808080',
        textDecorationLine: 'underline',
        width: Dimensions.get('window').width - 80,
    },
});

@autobind
export default class EditOrganisationForm extends Component {

    static propTypes = {
        organisations: React.PropTypes.object,
        onSubmit: React.PropTypes.func
    }

    state = {
        newOrganisations: [],
        showErrors: false
    }

	componentDidMount() {
		if (this.props.organisations.data && this.props.organisations.data.length == 0) {
			this.setState({
				newOrganisations: [{
					name: '',
					position: ''
				}]
			});
		}
	}

    submit() {

		let that = this;

		if (this.props.organisations.data.length == 0 && this.state.newOrganisations.length > 0 && (this.state.newOrganisations[0].name == '' || this.state.newOrganisations[0].position == '')) {
            this.setState({
                showErrors: true
            });
		} else {
            this.props.onSubmit(this.state.newOrganisations);

			setTimeout(function(){
				that.setState({
                    showErrors: false,
					newOrganisations: []
				});
			}, 2000);
        }
    }

	signupToReceiveMessages() {

		let that = this;

		if (this.props.organisations.data.length == 0 && this.state.newOrganisations.length > 0 && (this.state.newOrganisations[0].name == '' || this.state.newOrganisations[0].position == '')) {
            this.setState({
                showErrors: true
            });
		} else {
            this.props.signupToReceiveMessages(this.state.newOrganisations);

			setTimeout(function(){
				that.setState({
                    showErrors: false,
					newOrganisations: []
				});
			}, 2000);
        }
	}

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            paddingHorizontal: 15,
            paddingTop: 15,
            ...Platform.select({
                android: {
                    marginTop: 60
                },
                ios: {
                    marginBottom: 80
                }
            })
        };

		if (Platform.OS === 'ios' && this.props.me && this.props.me.data && this.props.me.data.recipient.status !== 'Approved' && this.props.organisations && this.props.organisations.data.length == 0) {
			style.marginBottom = 135;
		}

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

    addOrganisation() {

        let currentOrganisations = this.state.newOrganisations;

        currentOrganisations.push({
            name: '',
            position: '',
            website: ''
        });

        this.setState({
            newOrganisations: currentOrganisations
        });
    }

    updateNewOrganisation(payload) {

        let currentOrganisations = this.state.newOrganisations;

        currentOrganisations[payload.id] = payload.organisation;

        this.setState({
            newOrganisations: currentOrganisations
        });
    }

    deleteNewOrganisation(index) {

        let currentOrganisations = this.state.newOrganisations;

        currentOrganisations.splice(index, 1);

        this.setState({
            newOrganisations: currentOrganisations
        });
    }

    renderOrganisationFields() {

        if (!this.props.organisations || !this.props.organisations.data) {
            return false;
        }

        let that = this;

        let organisationFields = [];

        this.props.organisations.data.map(function (organisation, index){
            organisationFields.push(<OrganisationForm key={"organisation-"+organisation.id} id={organisation.id} organisation={organisation} organisationCount={that.props.organisations.data.length + that.state.newOrganisations.length} onUpdateOrganisation={that.props.onUpdateOrganisation} onDeleteOrganisation={that.props.onDeleteOrganisation} position={index + 1} />)
            if (index != that.props.organisations.data.length - 1 || that.state.newOrganisations.length > 0) {
                organisationFields.push(<Hr key={"hr-"+index} lineColor='#b3b3b3' lineStyle={{margin: 20}} />)
            }
        });

        this.state.newOrganisations.map(function (organisation, index){
            organisationFields.push(<OrganisationForm key={"new-organisation-"+index} id={index} organisation={organisation} organisationCount={that.props.organisations.data.length + that.state.newOrganisations.length} onUpdateOrganisation={that.updateNewOrganisation} onDeleteOrganisation={that.deleteNewOrganisation} position={index + 1 + that.props.organisations.data.length} />)
            if (index != that.state.newOrganisations.length - 1) {
                organisationFields.push(<Hr key={"hr-"+index} lineColor='#b3b3b3' lineStyle={{margin: 20}} />)
            }
        });


        return organisationFields;
    }

    render() {

        const organisationsLength = (this.props.organisations && this.props.organisations.data)? this.props.organisations.data.length : null;
        const newOrganisationsLength = this.state.newOrganisations.length;
        let isLatestOrganisationComplete = false;

        if (newOrganisationsLength > 0 && this.state.newOrganisations[newOrganisationsLength - 1].name.length > 0 && this.state.newOrganisations[newOrganisationsLength - 1].position.length > 0) {
            isLatestOrganisationComplete = true;
        } else if (newOrganisationsLength == 0 && organisationsLength > 0 && this.props.organisations.data[organisationsLength - 1].name.length > 0 && this.props.organisations.data[organisationsLength - 1].position.length > 0) {
            isLatestOrganisationComplete = true;
        }

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}>

                    {((this.props.organisations && this.props.organisations.data && this.props.organisations.data.length == 0 && this.state.newOrganisations.length > 0 && (this.state.newOrganisations[0].name == '' || this.state.newOrganisations[0].position == '')) && this.state.showErrors) && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 15 }}>Please add an organisation to be able to send or receive messages</Text>}

                    {false && <View style={styles.linkedInLogin}>
                        <Image source={require('../../images/linkedin.png')} style={styles.linkedInIcon} />
                        <Text style={styles.linkedInLink}>Adding your LinkedIn profile makes it easier for message senders to find the right person</Text>
                    </View>}

                    {this.renderOrganisationFields()}

                    <View style={styles.elementContainer}>
                       {isLatestOrganisationComplete && <View style={styles.addOrganisation}>
                            <TouchableHighlight onPress={this.addOrganisation} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                                <Text style={styles.roundButtonText}>+</Text>
                            </TouchableHighlight>
                            <Text style={styles.indentedLabel}>Add additional organisation</Text>
                        </View>}
                    </View>
                </KeyboardAwareScrollView>

				{((this.props.me && this.props.me.data && this.props.me.data.recipient.status === 'Approved') || (this.props.organisations.data && this.props.organisations.data.length > 0)) && <View style={styles.bottomButtonWrapper}>
                    <View style={styles.submitButton}>
                        <Button
                            onPress={this.submit}
                            title='Save Details'
                            color={(Platform.OS === 'ios') ? ((this.props.organisations.data.length == 0 && this.state.newOrganisations.length > 0 && (this.state.newOrganisations[0].name == '' || this.state.newOrganisations[0].position == '')) ? '#ddd' : '#fff') : '#61b746'}
                        />
                    </View>
                </View>}
				{((this.props.me && this.props.me.data && this.props.me.data.recipient.status !== 'Approved' && this.props.organisations.data.length == 0)) && <View style={styles.bottomButtonWrapper}>
                    <View style={styles.firstSubmitButton}>
                        <Button
                            onPress={this.signupToReceiveMessages}
                            title="I'm a decision maker"
                            color={(Platform.OS === 'ios') ? ((this.props.organisations.data.length == 0 && this.state.newOrganisations.length > 0 && (this.state.newOrganisations[0].name == '' || this.state.newOrganisations[0].position == '')) ? '#ddd' : '#fff') : '#61b746'}
                        />
                    </View>

					<View style={styles.submitButton}>
                        <Button
                            onPress={this.submit}
                            title='I want to talk to decision makers'
                            color={(Platform.OS === 'ios') ? ((this.props.organisations.data.length == 0 && this.state.newOrganisations.length > 0 && (this.state.newOrganisations[0].name == '' || this.state.newOrganisations[0].position == '')) ? '#ddd' : '#fff') : '#61b746'}
                        />
                    </View>
                </View>}
            </View>
        );
    }
}

export class OrganisationForm extends Component {

    static propTypes = {
        organisation: React.PropTypes.object,
        onUpdateOrganisation: React.PropTypes.func,
        onDeleteOrganisation: React.PropTypes.func
    }

    state = {
        name: this.props.organisation.name,
        position: this.props.organisation.position,
        website: this.props.organisation.website
    }

	ordinal_suffix_of(number) {
		let j = number % 10;
		let	k = number % 100;

		if (j == 1 && k != 11) {
			return number + "st";
		}
		if (j == 2 && k != 12) {
			return number + "nd";
		}
		if (j == 3 && k != 13) {
			return number + "rd";
		}
		return number + "th";
	}

    updateOrganisation() {

        let payload = {
            name: this.state.name,
            position: this.state.position,
            website: this.state.website
        }

        this.props.onUpdateOrganisation({ id: this.props.id, organisation: payload });
    }

    render() {

        return(
            <View style={styles.organisationWrapper}>
                <View style={styles.organisationField}>
					{this.props.position == 1 && <Text style={styles.label}>Organisation you are associated with:</Text>}
					{this.props.position != 1 && <Text style={styles.label}>{this.ordinal_suffix_of(this.props.position)} Organisation</Text>}
                    <TextInput
                        ref={'input-1'}
                        style={this.props.organisationCount > 1? styles.indentedFormInput: styles.formInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        onChangeText={(name) => this.setState({ name })}
                        value={this.state.name}
                        placeholder={'Company / Organisation'}
                        returnKeyType='done'
                        onEndEditing={()=>this.updateOrganisation()}
                        onSubmitEditing={()=>this.updateOrganisation()}
                    />
					{this.props.organisationCount > 1 && <View style={styles.removeOrganisation}>
						<TouchableHighlight onPress={()=> this.props.onDeleteOrganisation(this.props.id)} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
							<Text style={styles.roundButtonText}>-</Text>
						</TouchableHighlight>
					</View>}
                </View>
                <View style={styles.positionField}>
					{this.props.position == 1 && <Text style={styles.label}>Position (e.g. Director, Chairman): </Text>}
					{this.props.position != 1 && <Text style={styles.label}>Position: </Text>}
                    <TextInput
                        ref={'input-2'}
                        style={styles.formInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        onChangeText={(position) => this.setState({ position })}
                        value={this.state.position}
                        placeholder={'Position'}
                        returnKeyType='done'
                        onEndEditing={()=>this.updateOrganisation()}
                        onSubmitEditing={()=>this.updateOrganisation()}
                    />
                </View>
                <View style={styles.positionField}>
					<Text style={styles.label}>Website: </Text>
                    <TextInput
                        ref={'input-2'}
                        style={styles.formInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        onChangeText={(website) => this.setState({ website })}
                        value={this.state.website}
                        placeholder={'Website'}
                        returnKeyType='done'
                        onEndEditing={()=>this.updateOrganisation()}
                        onSubmitEditing={()=>this.updateOrganisation()}
                    />
                </View>
            </View>
        );
    }
}