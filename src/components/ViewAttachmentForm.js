import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView,
    Linking
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';

import PDFViewer from './PDFViewer';
import VideoPlayer from './VideoPlayer';

const styles = StyleSheet.create({
    app: {
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                paddingBottom: 50,
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64,
            }
        })
    },
});

@autobind
export default class ViewAttachmentForm extends Component {

    static propTypes = {
        message: React.PropTypes.object,
        attachment: React.PropTypes.string
    }

    state = {
        selectedVideo: ''
    }

    renderAttachment() {
        return(<PDFViewer attachment={this.props.attachment} />);
    }

    render() {

        return (
            <View style={styles.app}>
                {this.renderAttachment()}
            </View>
        );
    }
}
