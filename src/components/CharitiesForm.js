import React, { Component } from 'react';
import {
    View,
    Button,
    Text,
    Platform,
    StyleSheet,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    TextInput,
    KeyboardAvoidingView,
    Linking,
    ActivityIndicator
} from 'react-native';
import autobind from 'autobind-decorator';
import Slider from "react-native-slider";
import Hr from 'react-native-hr';
import Triangle from 'react-native-triangle';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    },
    scrollView: {
        backgroundColor: '#fff',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        marginBottom: 80,
        padding: 15,
        ...Platform.select({
            android: {
                marginTop: 60,
                marginBottom: 96
            },
            ios: {
                marginBottom: 80,
            }
        })
    },
    charityIntroText: {
        color: '#808080'
    },
    selectFeeWrapper: {
        position: 'relative',
        marginBottom: 15,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    minimumFee: {
        position: 'absolute',
        left: 15,
        bottom: 35,
        color: '#808080'
    },
    maximumFee: {
        position: 'absolute',
        right: 15,
        bottom: 35,
        color: '#808080'
    },
    selectFee: {
        marginTop: 10,
        width: Dimensions.get('window').width - 75
    },
    track: {
        height: 4,
        borderRadius: 2,
    },
    thumb: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        backgroundColor: 'white',
        borderColor: '#30a935',
        borderWidth: 2,
    },
    messageFeeWrapper: {
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        paddingHorizontal: 5,
        backgroundColor: 'rgb(110, 149, 255)',
        minHeight: 50,
        width: Dimensions.get('window').width - 50,
        borderRadius: 5,
        overflow: 'hidden',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    messageFee: {
        color: '#fff',
        textAlign: 'center'
    },
    triangle: {
        ...Platform.select({
            ios: {
                marginBottom: 7
            },
            android: {
                marginBottom: 30
            },
        })
    },
    formInput: {
        height: 40,
        width: 50,
        marginTop: 5,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7
    },
    maximumMessages: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginTop: 20,
        marginBottom: 30
    },
    charityDescription: {
        marginBottom: 30,
        color: '#808080'
    },
    charityWrapper: {
        paddingTop: 10,
        paddingBottom: 10,
        position: 'relative'
    },
    charityName: {
        marginLeft: 50,
        flex: 3,
        textAlignVertical: 'center',
        width: Dimensions.get('window').width - 100,
    },
    charityUrl: {
        marginLeft: 50,
        width: Dimensions.get('window').width - 100,
        color: '#539fe0',
        textDecorationLine: 'underline',
        fontWeight: 'bold'
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1
    },
    chooseCharityButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    },
    arrowWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 10,
        right: 5
    },
    arrowText: {
        height: 20,
        width: 20
    },
    heading: {
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 5,
        marginBottom: 5
    },
    aboutButton: {
        marginBottom: 20,
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#4477dd'
            }
        })
    },
    label: {
        color: '#808080',
        fontSize: 15,
        marginRight: 10
    },
    elementContainer: {
        marginTop: 15,
        marginBottom: 20,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    roundGreenButton: {
        borderRadius: 20,
        padding: 0,
        width: 40,
        height: 40,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#61b746'
    },
    roundButtonText: {
        color: '#fff',
        fontSize: 25,
        ...Platform.select({
            ios: {
                lineHeight: 25
            },
            android: {
                lineHeight: 15
            }
        })
    },
    durationToggle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    durationInput: {
        height: 40,
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 7,
        marginRight: 7,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        padding: 7,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    showNoCharitiesError: {
        textAlign: 'center',
        color: 'red',
        marginTop: 10
    },
    addCharity: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginTop: 20,
        marginBottom: 30
    },
    addCharitiesButton: {
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    resultRow: {
        paddingTop: 10,
        paddingBottom: 10,
        position: 'relative',
    },
    resultWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 5
    },
    resultName: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 9
    },
    resultIcon: {
        height: 20,
        width: 20
    },
    resultHeading: {
        fontWeight: 'bold',
        fontSize: 15,
        marginLeft: 7,
        width: Dimensions.get('window').width - 110,
    }
});

@autobind
export default class CharitiesForm extends Component {

    static propTypes = {
        allCharities: React.PropTypes.object,
        myCharities: React.PropTypes.object,
        onAttachCharity: React.PropTypes.func,
        onDetachCharity: React.PropTypes.func,
        onSubmit: React.PropTypes.func,
        me: React.PropTypes.object,
        error: React.PropTypes.string
    }

    state = {
        donation: this.props.me.data.donation > 30 ? this.props.me.data.donation : 30, //donation,
        maximumMessages: this.props.me.data.maxMessages ? this.props.me.data.maxMessages : 0,
        charitiesError: ''
    }

    openUrl(url) {
        let urlLink = url;

        // has to start with http:// or https://
        if (url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
    }

    detachCharity(charityId, index) {

        let myCharities = this.props.myCharities;
        myCharities.data.splice(index, 1);

        let that = this;

        this.props.onDetachCharity(charityId);
    }

    renderMyCharity(charity, index) {

        let charityUrl = charity.url.replace("http://", "").replace("https://", "");

        return (
            <View
                key={'sent-' + charity.id}
                style={styles.resultRow}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableHighlight
                        style={styles.roundGreenButton}
                        underlayColor={'rgba(119, 221, 68, 0.72)'}
                        onPress={() => this.detachCharity(charity.id, index)}
                        underlayColor={'#FFF'}>
                        <Text style={styles.roundButtonText}>-</Text>
                    </TouchableHighlight>
                    <View style={styles.resultWrapper}>
                        <View>
                            <Text style={styles.resultHeading}>{charity.name}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    generatePerAnnum(donation) {

        let proRata = 480 * donation;

        return this.numberWithCommas(proRata);
    }

    increaseMaximum() {
        if (this.state.maximumMessages == 0) {
            this.setState({
                maximumMessages: 10
            });
            return true;
        }

        this.setState({
            maximumMessages: this.state.maximumMessages == 20 ? 0 : this.state.maximumMessages + 1
        });
    }

    decreaseMaximum() {
        if (this.state.maximumMessages === 1) {
            return;
        }

        this.setState({
            maximumMessages: this.state.maximumMessages == 0 ? 10 : this.state.maximumMessages - 1
        });
    }

    submit() {

        if (this.props.myCharities.data.length > 0) {
            let payload = {
                donation: this.state.donation,
                maxMessages: this.state.maximumMessages
            }

            this.props.onSubmit(payload);
        } else {

            this.setState({
                charitiesError: 'At least one charity must be supported'
            });
        }
    }


    findCharity() {
        this.props.onFindCharity();
        this.setState({
            charitiesError: ''
        });
    }

    render() {

        let numberOfMessages = this.state.maximumMessages + ' message'
        if (this.state.maximumMessages != 1) {
            numberOfMessages += 's';
        }

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    automaticallyAdjustContentInsets={false}
                    style={styles.scrollView}
                    keyboardShouldPersistTaps="handled">
                    <View style={{ flex: 1 }}>

                        <Text style={styles.heading}>Set fee paid every time you watch a message</Text>

                        <View style={styles.selectFeeWrapper}>
                            <View style={styles.messageFeeWrapper}>
                                <Text style={styles.messageFee}>When you watch a message on Raised <Text style={{ fontWeight: "bold" }}>£{this.state.donation}</Text> will be split between your chosen charities.{'\n'}Valuing your time at £{this.generatePerAnnum(this.state.donation)} per day.</Text>
                            </View>
                            <View style={styles.triangle}>
                                <Triangle
                                    width={30}
                                    height={20}
                                    color={'rgb(110, 149, 255)'}
                                    direction={'down'}
                                />
                            </View>

                            <Text style={styles.minimumFee}>£30</Text>
                            <Slider
                                onValueChange={(donation) => this.setState({ donation: parseInt(donation) })}
                                value={this.state.donation}
                                maximumValue={500}
                                minimumValue={30}
                                step={10}
                                style={{ width: Dimensions.get('window').width - 100 }}
                                trackStyle={styles.track}
                                thumbStyle={styles.thumb}
                                minimumTrackTintColor='#30a935' />
                            <Text style={styles.maximumFee}>£500</Text>
                        </View>

                        <Text style={styles.heading}>Control how many messages you receive...</Text>

                        <View style={styles.elementContainer}>
                            {false && <Text style={styles.label}>Maximum messages per week:</Text>}
                            <View style={styles.durationToggle}>
                                <TouchableHighlight onPress={this.decreaseMaximum} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                                    <Text style={styles.roundButtonText}>-</Text>
                                </TouchableHighlight>
                                <View style={styles.durationInput}>
                                    {this.state.maximumMessages != 0 && <Text>Max {numberOfMessages} a week</Text>}
                                    {this.state.maximumMessages == 0 && <Text>Unlimited messages a week</Text>}
                                </View>
                                <TouchableHighlight onPress={this.increaseMaximum} style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                                    <Text style={styles.roundButtonText}>+</Text>
                                </TouchableHighlight>
                            </View>
                        </View>

                        <View>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={styles.heading}>Supported charities</Text>
                                {this.state.charitiesError.length > 0 && <Text style={styles.showNoCharitiesError}>{this.state.charitiesError}</Text>}

                                <TouchableHighlight onPress={this.findCharity} style={styles.addCharitiesButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                                    <View>
                                        <Button
                                            onPress={this.findCharity}
                                            title='Choose Charities'
                                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                                        />
                                    </View>
                                </TouchableHighlight>

                                {this.props.myCharities.data && this.props.myCharities.data.map(this.renderMyCharity)}
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>

                <View style={styles.bottomButtonWrapper}>
                    <View style={styles.chooseCharityButton}>
                        <Button
                            onPress={() => this.submit()}
                            title='Save'
                            color={(Platform.OS === 'ios') ? (this.props.myCharities && this.props.myCharities.data && this.props.myCharities.data.length == 0 ? '#eee' : '#fff') : '#61b746'}
                        />
                    </View>
                </View>
            </View>
        );
    }
}