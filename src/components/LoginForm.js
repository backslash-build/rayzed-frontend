import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import t from 'tcomb-form-native';
import {
    Platform,
    Button,
    View,
    StyleSheet,
    Text,
    NetInfo,
	TextInput,
	TouchableWithoutFeedback,
    Image,
    Dimensions,
    Linking,
    ActivityIndicator
} from 'react-native';

const styles = StyleSheet.create({
    nameInput: {
        height: 40,
        marginTop: 30,
        marginBottom: 15,
        marginLeft: 20,
        marginRight: 20
    },
    passwordInput: {
        height: 40,
        marginTop: 15,
        marginBottom: 30,
        marginLeft: 20,
        marginRight: 20
    },
    loginButton: {
        borderRadius: 5,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    label: {
        color: '#808080',
        fontSize: 15,
        marginRight: 10
    },
    link: {
        color: '#808080',
        textDecorationLine: 'underline',
        width: Dimensions.get('window').width - 30,
        textAlign: 'center',
    },
});

@autobind
export default class LoginForm extends Component {

    static propTypes = {
        loading: React.PropTypes.bool,
        onSubmit: React.PropTypes.func,
        error: React.PropTypes.string
    }

    state = {
        value: null,
        showStatus: false,
        isConnected: true,
		showPassword: false,
        user: {
			username: '',
			password: ''
		},
    }

    componentDidMount() {

        this.isConnected();
    }

    isConnected() {

		let that = this;

		this.props.testConnection(connectionStatus => {
            that.setState({
                isConnected: connectionStatus.connection,
                isConnectedError: connectionStatus.message
			});
        });
    }

	onChange(user) {
		this.setState({
			user: user
		});
	}

    inputValidation(value, extraStyle) {

        let style = {
            height: 40,
            marginTop: 7,
            marginBottom: 15,
            borderColor: '#d0d0d0',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 7
        };

        if (this.state.showErrors && (!value || value.length === 0)) {
            style.borderColor = '#fa4f4f';
        }

		for (var attrname in extraStyle) { style[attrname] = extraStyle[attrname]; }

        return style;
    }

    submit() {

        this.setState({
            showErrors: false,
            isConnected: true,
            isConnectedError: '',
            showStatus: true
        });

        this.props.testConnection(connectionStatus => {
            this.setState({
                isConnected: connectionStatus.connection,
                isConnectedError: connectionStatus.message
			});

            if (connectionStatus.connection) {
				if(!this.state.user.username || !this.state.user.password){
                    this.setState({
                        showErrors: true
                    });

					return false;
                }

                const payload = {
                    username: this.state.user.username,
                    password: this.state.user.password
                };
                this.props.onSubmit(payload);
            }
        });
    }

    render() {

		let togglePasswordImage = this.state.showPassword? require('../../images/ic_visibility_off_black_24dp/web/ic_visibility_off_black_24dp_2x.png') : require('../../images/ic_visibility_black_24dp/web/ic_visibility_black_24dp_2x.png');

        return (
            <View style={{ marginHorizontal: 15 }}>
				<View style={styles.elementContainer}>
					<Text style={styles.label}>Email</Text>
					<TextInput
                        ref={'input-1'}
						autoCorrect={false}
						style={this.inputValidation(this.state.user.username)}
						underlineColorAndroid='rgba(0,0,0,0)'
						onChangeText={(username) => this.onChange({ ...this.state.user, username })}
						value={this.state.user.username}
						placeholder={'Email'}
                        keyboardType='email-address'
                        returnKeyType={'next'}
                        blurOnSubmit={false}
                        onSubmitEditing={()=> this.refs['input-2'].focus()}
					/>
				</View>

				<View style={styles.elementContainer}>
					<Text style={styles.label}>Password</Text>
					<TextInput
                        ref={'input-2'}
						autoCorrect={false}
						secureTextEntry={!this.state.showPassword}
						style={this.inputValidation(this.state.user.password, { paddingRight: 50 })}
						underlineColorAndroid='rgba(0,0,0,0)'
						onChangeText={(password) => this.onChange({ ...this.state.user, password })}
						value={this.state.user.password}
						placeholder={'Password'}
						onSubmitEditing={this.submit}
                        returnKeyType={'done'}
					/>
					<TouchableWithoutFeedback onPress={()=> this.setState({ showPassword: !this.state.showPassword })}>
						<View style={{ position: 'absolute', top: 30, right: 10}}>
							<Image style={{ width: 30, height: 30 }} source={togglePasswordImage} />
						</View>
					</TouchableWithoutFeedback>
				</View>

				{this.props.links && !this.props.keyboardShowing && <TouchableWithoutFeedback onPress={()=> this.props.openUrl(this.props.links.resetPassword)}>
                    <View>
                        <Text style={styles.link}>Forgot password?</Text>
                    </View>
				</TouchableWithoutFeedback>}

                <View style={{ marginVertical: 20 }}>
                    {this.state.isConnected && this.state.showStatus && !this.props.error && <Text style={{ textAlign: 'center', color: 'black' }}>Attempting to login...</Text>}
                    {this.state.isConnected && this.state.showStatus && !this.props.error && <ActivityIndicator
                        animating={true}
                        style={[styles.centering, {height: 40}]}
                        size="small"
                        color="green"
                    />}
                    {this.state.isConnected && this.state.showStatus && this.props.error && <Text style={{ textAlign: 'center', color: 'red' }}>{this.props.error}</Text>}
                    {this.state.isConnected && this.state.showErrors && <Text style={{ textAlign: 'center', color: 'red' }}>Please complete all fields</Text>}
                    {!this.state.isConnected && !this.state.isConnectedError && <Text style={{ textAlign: 'center', color: 'red' }}>Not connected to the internet</Text>}
                    {!this.state.isConnected && this.state.isConnectedError &&  <Text style={{ textAlign: 'center', color: 'red' }}>{this.state.isConnectedError}</Text>}
                </View>
                <View style={styles.loginButton}>
                    <Button
                        disabled={this.props.loading || (!this.state.isConnected && !this.state.isConnectedError)}
                        onPress={this.submit}
                        title='Login'
                        color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                    />
                </View>
            </View>
        );
    }
}