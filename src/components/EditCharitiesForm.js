import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    ScrollView,
    Dimensions,
    Image,
    TouchableHighlight,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
	Keyboard,
    Linking,
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import Hr from 'react-native-hr';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    heading: {
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 5,
        marginBottom: 5
    },
    subheading: {
        fontSize: 14
    },
    label: {
        marginTop: 15,
        color: '#808080',
        fontSize: 15
    },
    elementContainer: {
        marginTop: 50
    },
    searchIcon: {
        height: 15,
        width: 15,
        position: 'absolute',
        top: 50,
        left: 5
    },
    searchButton: {
        marginTop: 15,
        borderRadius: 5,
        paddingTop: 3,
        paddingBottom: 3,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    inputContainer: {
        position: 'relative'
    },
    recipientResult: {
        paddingTop: 20,
        paddingBottom: 20,
        position: 'relative'
    },
    segmentHeading: {
        marginTop: 30,
    },
    searchResults: {
        marginTop: 20,
        marginBottom: 20
    },
    resultsHeading: {
        fontSize: 18,
        textAlign: 'left',
        textAlignVertical: 'center',
    },
    resultHeading: {
        fontWeight: 'bold',
        fontSize: 15,
        marginLeft: 10,
        width: Dimensions.get('window').width - 140,
        marginRight: 7,
    },
    resultSubheading: {
        fontSize: 10,
        color: '#808080',
        marginLeft: 10,
        width: Dimensions.get('window').width - 110,
    },
    resultRow: {
        position: 'relative',
    },
    resultWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    resultName: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 9
    },
    resultIcon: {
        height: 20,
        width: 20
    },
    charityWrapper: {
        paddingTop: 5,
        paddingBottom: 5,
        position: 'relative'
    },
    charityName: {
        marginLeft: 50,
        flex: 3,
        textAlignVertical: 'center',
		width: Dimensions.get('window').width - 100,
    },
    arrowWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 10,
        right: 5
    },
    arrowText: {
        height: 20,
        width: 20
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1
    },
    submitButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    },
    roundGreenButton: {
        borderRadius: 20,
        padding: 0,
        width: 40,
        height: 40,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#61b746'
    },
    roundButtonText: {
        color: '#fff',
        fontSize: 25,
        ...Platform.select({
            ios: {
                lineHeight: 25
            },
            android: {
                lineHeight: 15
            }
        })
    },
    requestCharity: {
        flexDirection: 'row',
        ...Platform.select({
            android: {
                marginBottom: 30
            },
            ios: {
                marginBottom: 20
            }
        })
    },
    requestCharityButton: {
        width: 100,
        height: 45,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 5,
        marginTop: 10,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android : {
                marginBottom: 30
            }
        })
    },
    requestCharityInput: {
        height: 45,
        marginTop: 10,
        marginBottom: 15,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7,
        width: Dimensions.get('window').width - 145,
        marginRight: 10
    },
    resultRow: {
        paddingTop: 4,
        paddingBottom: 4,
        position: 'relative',
    },
    resultWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    resultName: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 9
    },
    resultIcon: {
        height: 20,
        width: 20
    },
    linkText: {
        color: '#222',
        fontSize: 20
    },
    modalWrapper: {
        backgroundColor: 'rgba(70, 70, 70, 0.75)',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    feedbackModal: {
        width: 250,
        height: 180,
        borderRadius: 10,
        backgroundColor: '#fff',
        overflow: 'hidden',
        padding: 10
    },

    submitModalButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        marginTop: 20,
        overflow: 'hidden',
        width: 228,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    }
});

@autobind
export default class EditCharitiesForm extends Component {

    static propTypes = {
        onSearch: React.PropTypes.func,
        onSubmit: React.PropTypes.func,
    }

    state = {
        showCameraPicker: false,
        chosenVideo: null,
        showErrors: false,
        showRequestModal: false
    }

    inputValidation() {
        let style = {
            height: 40,
            marginTop: 5,
            borderColor: '#d0d0d0',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 25,
            paddingTop: 5,
            paddingBottom: 5,
            position: 'relative'
        };

        if (this.state.showErrors && this.props.searchTerms.nameSearch.length == 0 ) {
            style.borderColor = '#fa4f4f';
        }

        return style;
    }

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            paddingLeft: 15,
            paddingRight: 0,
            paddingTop: 15,
            ...Platform.select({
                android: {
                    marginTop: 60,
                    marginBottom: 96
                }
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

	attachCharity(charityId, index) {

        if (this.props.myCharities.length < 4) {

            this.setState({
                charitiesError: '',
            });

            this.props.onAttachCharity(charityId);
        } else {
            this.setState({
                charitiesError: 'Up to four supported charities can be added'
            });
        }
    }

    renderCharity(charity, index) {

        let charityUrl = charity.url.replace("http://", "").replace("https://", "");

        return (
            <View
            key={'sent-'+charity.id}
            style={styles.resultRow} >
                <View
                    key={'charity-'+charity.id}
                    style={styles.resultRow}>
                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                        <TouchableHighlight
                        style={styles.roundGreenButton}
                        underlayColor={'rgba(119, 221, 68, 0.72)'}
                        onPress={() => this.attachCharity(charity.id, index)}
                        underlayColor={'#FFF'}>
                            <Text style={styles.roundButtonText}>+</Text>
                        </TouchableHighlight>
                        <TouchableWithoutFeedback
                            onPress={() => this.attachCharity(charity.id, index)}
                            underlayColor={'#FFF'}>
                            <View style={styles.resultWrapper}>
                                <View>
                                    <Text numberOfLines={1} ellipsizeMode={'tail'} style={styles.resultHeading}>{charity.name}</Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => this.props.openUrl(charity.url, index)}
                            underlayColor={'rgba(119, 221, 68, 0.72)'}>
                            <View style={{ position: 'absolute', right: 0 }}>
                                <Image style={{ height: 30, width: 75 }} resizeMode={'contain'} source={require('../../images/charity-link.jpg')} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>
        );
    }

    requestCharity() {

        Keyboard.dismiss();

        this.props.requestCharity({
            "name": this.state.charityName,
            "url": ""
        });

        this.setState({
            showRequestModal: true
        })
    }

    search() {

        Keyboard.dismiss();

        if (this.props.searchTerms.nameSearch.length > 2 || this.props.searchTerms.positionSearch.length > 2 || this.props.searchTerms.companySearch.length > 2) {

            const query = {
                name: this.props.searchTerms.nameSearch,
                position: this.props.searchTerms.positionSearch,
                organisation: this.props.searchTerms.companySearch
            };

            this.props.onSearch(query);

            this.setState({
                showErrors: false
            })
        } else {
            this.setState({
                showErrors: true
            });
        }
    }

    render() {

        let searchEligible = this.props.searchTerms.nameSearch.length > 2;

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}
                    keyboardShouldPersistTaps="handled">

                    {false && !this.props.showingResults && <View>
                        <View>
                            <Text style={styles.subheading}>Use boxes below to filter and find individuals:</Text>
                        </View>
                        {this.state.showErrors && !searchEligible && <Text style={{ textAlign: 'center', color: 'red', marginTop: 15 }}>Search term has to be at least 3 characters</Text>}
                        <View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.label}>Name:</Text>
                                <Image
                                    source={require('../../images/search-grey.png')}
                                    style={styles.searchIcon}/>
                                <TextInput
                                    autoCorrect={false}
                                    style={this.inputValidation()}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    onChangeText={(nameSearch) => this.props.updateSearchTerms('nameSearch', nameSearch)}
                                    value={this.props.searchTerms.nameSearch}
                                    placeholder={'Search'}
                                    onSubmitEditing={()=> this.search()}
                                    returnKeyType='search' />
                            </View>
                        </View>

                        <View style={styles.searchButton}>
                            <Button
                                onPress={this.search}
                                title='Search'
                                color={(Platform.OS === 'ios') ? (searchEligible? '#fff' : '#ddd') : '#61b746'}
                            />
                        </View>
                    </View>}

                    {this.props.showingResults && <View style={styles.searchButton}>
                        <Button
                            onPress={()=> this.props.clearSearch()}
                            title='Edit search'
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                        />
                    </View>}

                    <View style={styles.searchResults}>
                        {this.props.allCharities && this.props.allCharities.length > 0 && this.props.allCharities.map(this.renderCharity)}
                        {this.props.allCharities && this.props.allCharities.length == 0 && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 20 }}>No charities found</Text>}
                    </View>

                    <Text style={styles.heading}>Please add my favorite charity...</Text>
                    {!this.state.requestedCharity && <View style={styles.requestCharity}>
                        <TextInput
                            autoCorrect={false}
                            style={styles.requestCharityInput}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(charityName) => this.setState({ charityName: charityName })}
                            value={this.state.charityName}
                            placeholder={'Please add this charity'}
                            maxLength = {100}
                            returnKeyType='done'
                        />
                        <View style={styles.requestCharityButton}>
                            <Button
                                onPress={this.requestCharity}
                                title="Request"
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                            />
                        </View>
                    </View>}
                    {this.state.requestedCharity && <Text style={{ marginBottom: 20 }}>Select another charity for now until your requested charity has been added.</Text>}
                </KeyboardAwareScrollView>

                {(false || this.state.showRequestModal) && <RequestCharityModal charityName={this.state.charityName} onHideModal={()=> this.setState({ showRequestModal: false, charityName: '', requestedCharity: true })} />}
            </View>
        );
    }
}

class RequestCharityModal extends Component {

    render() {

        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.modalWrapper}>
                    <View style={styles.feedbackModal}>
                        <Text style={{ textAlign: 'center' }}>We'll get to work on that immediately.{"\n"}{"\n"}To complete registration choose any charity and then swap when we've signed {this.props.charityName}</Text>
                        <View style={styles.submitModalButton}>
                            <Button
                                onPress={()=>this.props.onHideModal()}
                                title='Okay'
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                            />
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}