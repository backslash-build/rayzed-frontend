import React, { Component } from 'react';
import {
    View,
    Text,
    Platform,
    Dimensions,
    Keyboard,
    TextInput,
    Button,
    TouchableWithoutFeedback,
    StyleSheet
 } from 'react-native';
import autobind from 'autobind-decorator';

import ReactionButton from './ReactionButton';

const styles = StyleSheet.create({
    modalWrapper: {
        backgroundColor: 'rgba(70, 70, 70, 0.75)',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 142
            }
        })
    },
    feedbackModal: {
        width: 250,
        height: 180,
        borderRadius: 10,
        backgroundColor: '#fff',
        overflow: 'hidden',
        padding: 10
    },
    modalHeading: {
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        marginBottom: 10
    },
    commentsArea: {
        height: 100,
        fontSize: 15,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7,
        paddingTop: 2,
        textAlignVertical: 'top'
    },
    submitButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        marginTop: 10,
        overflow: 'hidden',
        width: 228,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    }
});

@autobind
export default class ReactionModal extends Component {

    static propTypes = {
        onLike: React.PropTypes.func,
        onDislike: React.PropTypes.func
    }

    state = {
        showFeedbackBox: false,
        comments: ''
    }

    renderContent() {
        if(this.state.showFeedbackBox) {
            return (
                <View>
                    <TextInput
                        style={styles.commentsArea}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        onChangeText={comments => this.setState({ comments })}
                        value={this.state.comments}
                        multiline={true}
                        placeholder={'Give brief feedback'} />
                    <View style={styles.submitButton}>
                        <Button
                            onPress={()=>this.props.onDislike(this.state.comments)}
                            title='Send Feedback'
                            color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                        />
                    </View>
                </View>
            );
        }

        return (
            <View>
                <Text style={styles.modalHeading}>Click to show how you feel...</Text>

                <ReactionButton
                    onPress={comment => this.props.onLike(comment)}
                    type={ReactionButton.types.LIKE}
                    comment={'Interesting, I\'ll look at the additional materials.'}
                />

                <ReactionButton
                    onPress={()=>this.setState({ showFeedbackBox: true })}
                    type={ReactionButton.types.NEUTRAL}
                    comment={'Other... let me type what I feel about this...'}
                />
            </View>
        );
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.modalWrapper}>
                    <View style={styles.feedbackModal}>
                        {this.renderContent()}
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }

}






