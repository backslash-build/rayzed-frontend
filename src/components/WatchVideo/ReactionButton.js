import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback
 } from 'react-native';
import autobind from 'autobind-decorator';

const images = {
    like: require('../../../images/like.png'),
    dislike: require('../../../images/dislike.png'),
    neutral: require('../../../images/neutral.png')
};

@autobind
export default class ReactionButton extends Component {

    static propTypes = {
        onPress: React.PropTypes.func,
        comment: React.PropTypes.string,
        type: React.PropTypes.string
    }

    static types = {
        LIKE: 'like',
        DISLIKE: 'dislike',
        NEUTRAL: 'neutral'
    }

    press() {
        this.props.onPress(this.props.comment);
    }

    render() {
        return (
            <View>
                <TouchableWithoutFeedback onPress={this.press}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 10,
                        marginBottom: 10
                    }}>
                        <View>
                            <Image source={images[this.props.type]} style={{
                                height: 40,
                                width: 40,
                                marginRight: 15
                            }} />
                        </View>
                        <Text style={{
                            width: 175,
                            fontSize: 13
                        }}>
                            {this.props.comment}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

}