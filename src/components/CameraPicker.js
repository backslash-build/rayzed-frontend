import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Platform,
  Button,
  TouchableHighlight,
  Image
} from 'react-native';

import autobind from 'autobind-decorator';
import CameraRollPicker from 'react-native-camera-roll-picker';

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 125,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#F6AE2D',
    },
    content: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    text: {
        fontSize: 16,
        alignItems: 'center',
        color: '#fff',
    },
    bold: {
        fontWeight: 'bold',
    },
    info: {
        fontSize: 12,
    },
    closeWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 5,
        right: 5
    },
    closeIcon: {
        width: 20,
        height: 20
    },
    refreshWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 5,
        left: 5
    },
    refreshIcon: {
        width: 20,
        height: 20
    }
});

@autobind
export default class CameraPicker extends Component {
    constructor(props) {
        super(props);
    }
    
    state = {
        showVideos: true
    }
    
    reloadVideos() {
        this.setState({
            showVideos: false
        }, function () {
            this.setState({
                showVideos: true
            });
        });
    }

    render() {
        
        let selectedVideos = [];
        if (this.props.selectedVideo) selectedVideos = [this.props.selectedVideo];
        
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <TouchableHighlight onPress={this.reloadVideos} style={styles.refreshWrapper} underlayColor={'#F6AE2D'}>
                        <Image 
                            source={require('../../images/ic_refresh_white_24dp/android/drawable-xxhdpi/ic_refresh_white_24dp.png')}
                            style={styles.refreshIcon}/>
                    </TouchableHighlight>
                    <Text style={styles.text}>
                        Select a video
                    </Text>
                    <TouchableHighlight onPress={this.props.hidePicker} style={styles.closeWrapper} underlayColor={'#F6AE2D'}>
                        <Image 
                            source={require('../../images/ic_close_white_24dp/android/drawable-xxhdpi/ic_close_white_24dp.png')}
                            style={styles.closeIcon}/>
                    </TouchableHighlight>
                </View>
                {this.state.showVideos && <CameraRollPicker
                    scrollRenderAheadDistance={500}
                    initialListSize={1}
                    pageSize={3}
                    removeClippedSubviews={false}
                    groupTypes='All'
                    batchSize={5}
                    maximum={1}
                    selected={selectedVideos}
                    assetType='Videos'
                    imagesPerRow={3}
                    imageMargin={5}
                    callback={this.props.handleSelectedVideo}
                    emptyText={"No videos"} />}
            </View>
        );
    }
}
