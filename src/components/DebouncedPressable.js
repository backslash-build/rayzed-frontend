import React, { Component } from 'react';
import _ from 'lodash';

export default class DebouncedPressable extends Component {
    render() {
        const Co = this.props.component;
        
		return(<Co
            {...this.props}
            onPress={this.props.action && _.throttle(this.props.action, 5000)}
        />);
    }
}