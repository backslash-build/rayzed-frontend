import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    ScrollView,
    Dimensions,
    Image,
    TouchableHighlight,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
	Keyboard,
    Linking,
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import Hr from 'react-native-hr';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    subheading: {
        fontSize: 14
    },
    label: {
        marginTop: 15,
        color: '#808080',
        fontSize: 15
    },
    elementContainer: {
        marginTop: 50
    },
    searchIcon: {
        height: 15,
        width: 15,
        position: 'absolute',
        top: 50,
        left: 5
    },
    searchButton: {
        marginTop: 15,
        borderRadius: 5,
        paddingTop: 3,
        paddingBottom: 3,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    inputContainer: {
        position: 'relative'
    },
    recipientResult: {
        paddingTop: 20,
        paddingBottom: 20,
        position: 'relative'
    },
    segmentHeading: {
        marginTop: 30,
    },
    searchResults: {
        marginTop: 20,
        marginBottom: 30
    },
    resultsHeading: {
        fontSize: 18,
        textAlign: 'left',
        textAlignVertical: 'center',
    },
    resultHeading: {
        fontWeight: 'bold',
        fontSize: 13,
        marginLeft: 10,
        width: Dimensions.get('window').width - 110,
        marginRight: 7
    },
    resultSubheading: {
        fontSize: 10,
        color: '#808080',
        marginLeft: 10,
        width: Dimensions.get('window').width - 110,
    },
    resultRow: {
        paddingTop: 10,
        paddingBottom: 10,
        position: 'relative',
    },
    resultWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    resultName: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 9
    },
    resultIcon: {
        height: 20,
        width: 20
    },
    arrowWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 10,
        right: 5
    },
    arrowText: {
        height: 20,
        width: 20
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 15,
        marginRight: 15,
        width: Dimensions.get('window').width - 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1
    },
    submitButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        overflow: 'hidden',
        width: Dimensions.get('window').width - 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android: {
                marginBottom: 30
            }
        })
    },
    roundGreenButton: {
        borderRadius: 20,
        padding: 0,
        width: 40,
        height: 40,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#61b746'
    },
    roundButtonText: {
        color: '#fff',
        fontSize: 25,
        ...Platform.select({
            ios: {
                lineHeight: 25
            },
            android: {
                lineHeight: 15
            }
        })
    },
});

@autobind
export default class FindRecipientForm extends Component {

    static propTypes = {
        onSearch: React.PropTypes.func,
        onSubmit: React.PropTypes.func,
        allRecipients: React.PropTypes.array,
        selectedRecipients: React.PropTypes.array
    }

    state = {
        showCameraPicker: false,
        chosenVideo: null,
        showErrors: false,
    }

    inputValidation() {
        let style = {
            height: 40,
            marginTop: 5,
            borderColor: '#d0d0d0',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 25,
            paddingTop: 5,
            paddingBottom: 5,
            position: 'relative'
        };

        if (this.state.showErrors && this.props.searchTerms.nameSearch.length == 0 && this.props.searchTerms.positionSearch.length == 0 && this.props.searchTerms.companySearch.length == 0) {
            style.borderColor = '#fa4f4f';
        }

        return style;
    }

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            paddingHorizontal: 15,
            paddingTop: 15,
            ...Platform.select({
                android: {
                    marginTop: 60,
                    marginBottom: 96
                }
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

	openUrl(url) {
        let urlLink = url;

        // has to start with http:// or https://
        if(url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL('http://www.linkedin.com').catch(err => console.error('An error occurred', err));
    }

    renderRecipient(result) {

        let organisation = '';
        let position = '';
        if (result.organisations.length > 0) {
            organisation = result.organisations[0].organisation;
            position = result.organisations[0].position;
        }

        return (
            <TouchableHighlight
                key={'sent-'+result.id}
                style={styles.resultRow}
                underlayColor={'#FFF'}
                onPress={() => {
                    this.props.addRecipient(result);
                    this.props.onSubmit();
                }}>
                <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                    <View style={styles.roundGreenButton} underlayColor={'rgba(119, 221, 68, 0.72)'}>
                        <Text style={styles.roundButtonText}>+</Text>
                    </View>
                    <View style={styles.resultWrapper}>
                        <View>
                            <Text style={styles.resultHeading}>{result.name} <Text>(£{result.donation})</Text></Text>
                            <Text style={styles.resultSubheading}>
                                <Text>
                                    {result.organisations.join('; ')}
                                </Text>
                            </Text>
                        </View>
                    </View>
                    {false && <TouchableWithoutFeedback onPress={()=> this.openUrl('www.linkedin.com')}>
						<Image source={require('../../images/linkedin.png')} style={styles.resultIcon} />
					</TouchableWithoutFeedback>}
                </View>
            </TouchableHighlight>
        );
    }

    search() {

        Keyboard.dismiss();

        if (this.props.searchTerms.nameSearch.length > 2 || this.props.searchTerms.positionSearch.length > 2 || this.props.searchTerms.companySearch.length > 2) {

            const query = {
                name: this.props.searchTerms.nameSearch,
                position: this.props.searchTerms.positionSearch,
                organisation: this.props.searchTerms.companySearch
            };

            this.props.onSearch(query);

            this.setState({
                showErrors: false
            })
        } else {
            this.setState({
                showErrors: true
            });
        }
    }

    render() {

        let searchEligible = (this.props.searchTerms.nameSearch.length > 2 || this.props.searchTerms.positionSearch.length > 2 || this.props.searchTerms.companySearch.length > 2);

        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}
                    keyboardShouldPersistTaps="handled">
                        {!this.props.showingResults && <View>
                            <View>
                                <Text style={styles.subheading}>Use boxes below to filter and find individuals:</Text>
                            </View>
                            {this.state.showErrors && !searchEligible && <Text style={{ textAlign: 'center', color: 'red', marginTop: 15 }}>Search term has to be at least 3 characters</Text>}
                            <View>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.label}>Name:</Text>
                                    <Image
                                        source={require('../../images/search-grey.png')}
                                        style={styles.searchIcon}/>
                                    <TextInput
                                        autoCorrect={false}
                                        style={this.inputValidation()}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        onChangeText={(nameSearch) => this.props.updateSearchTerms('nameSearch', nameSearch)}
                                        value={this.props.searchTerms.nameSearch}
                                        placeholder={'Search'}
                                        onSubmitEditing={()=> this.search()}
                                        returnKeyType='search' />
                                </View>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.label}>Position or Job Title:</Text>
                                    <Image
                                        source={require('../../images/search-grey.png')}
                                        style={styles.searchIcon}/>
                                    <TextInput
                                        autoCorrect={false}
                                        style={this.inputValidation()}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        onChangeText={(positionSearch) => this.props.updateSearchTerms('positionSearch', positionSearch)}
                                        value={this.props.searchTerms.positionSearch}
                                        placeholder={'Search'}
                                        onSubmitEditing={()=> this.search()}
                                        returnKeyType='search' />
                                </View>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.label}>Organisation or Company:</Text>
                                    <Image
                                        source={require('../../images/search-grey.png')}
                                        style={styles.searchIcon}/>
                                    <TextInput
                                        autoCorrect={false}
                                        style={this.inputValidation()}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        onChangeText={(companySearch) => this.props.updateSearchTerms('companySearch', companySearch)}
                                        value={this.props.searchTerms.companySearch}
                                        placeholder={'Search'}
                                        onSubmitEditing={()=> this.search()}
                                        returnKeyType='search' />
                                </View>
                            </View>

                            <View style={styles.searchButton}>
                                <Button
                                    onPress={this.search}
                                    title='Search'
                                    color={(Platform.OS === 'ios') ? (searchEligible? '#fff' : '#ddd') : '#61b746'}
                                />
                            </View>
                        </View>}

                        {this.props.showingResults && <View style={styles.searchButton}>
                            <Button
                                onPress={()=> this.props.clearSearch()}
                                title='Edit search'
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                            />
                        </View>}

                    {false && <View style={styles.segmentHeading}>
                        <Text style={styles.resultsHeading}>Click box to select recipients</Text>
                    </View>}

                    {this.props.showingResults && <View style={styles.searchResults}>
                        {this.props.allRecipients && this.props.allRecipients.recipients && this.props.allRecipients.recipients.length > 0 && this.props.allRecipients.returned != this.props.allRecipients.total && <Text>Showing {this.props.allRecipients.returned} of {this.props.allRecipients.total} results (refine your search)</Text>}
                        {this.props.allRecipients && this.props.allRecipients.recipients && this.props.allRecipients.recipients.length > 0 && this.props.allRecipients.recipients.map(this.renderRecipient)}
                        {this.props.allRecipients && this.props.allRecipients.recipients && this.props.allRecipients.recipients.length == 0 && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 20 }}>No recipients found</Text>}
                    </View>}
                </KeyboardAwareScrollView>
            </View>
        );
    }
}
