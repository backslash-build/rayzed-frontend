import React, { Component } from 'react';
import {
    View,
    Text,
    Platform,
    StyleSheet,
    Dimensions,
    ScrollView,
    Button
 } from 'react-native';
import autobind from 'autobind-decorator';
import Hr from 'react-native-hr';

import VideoPlayer from './VideoPlayer';
import ReactionModal from './WatchVideo/ReactionModal';

const styles = StyleSheet.create({
    app: {
		position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height - 24
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    scrollView: {
        backgroundColor: '#fff',
		height: Dimensions.get('window').height,
		...Platform.select({
			android: {
                marginTop: 54,
				marginBottom: 80
			},
			ios: {
				marginBottom: 80
			}
		})
    },
    videoInformation: {
        marginBottom: 30
    },
    videoHeading: {
        marginTop: 30,
        marginBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },
    videoSubheading: {
        textAlign: 'center',
        fontSize: 16
    },
    bottomViewerWrapper: {
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        width: Dimensions.get('window').width,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#539fe0'
    },
    viewerHeading: {
        color: '#fff',
        textAlign: 'center'
    },
    viewerName: {
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    viewerTerms: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 12
    },
    modalWrapper: {
        backgroundColor: 'rgba(70, 70, 70, 0.75)',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        ...Platform.select({
            ios: {
                marginTop: 35
            }
        })
    },
    feedbackModal: {
        width: 250,
        height: 350,
        borderRadius: 10,
        backgroundColor: '#fff',
        overflow: 'hidden',
        padding: 10
    },
    modalHeading: {
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center'
    },
    modalSubheading: {
        fontStyle: 'italic',
        fontSize: 12,
        textAlign: 'center'
    },
    commentsArea: {
        marginTop: 20,
        height: 200,
        fontSize: 15,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7,
        paddingTop: 2,
        textAlignVertical: 'top'
    },
    reactionContainer: {
        borderColor: '#efefef',
        borderTopWidth: 1,
        marginLeft: 15,
        marginRight: 15
    },
    reactionWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    reactionIcon: {
        height: 40,
        width: 40,
        marginRight: 15
    },
    reactionText: {
        width: 175,
        fontSize: 13
    },
    submitButton: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        marginTop: 20,
        overflow: 'hidden',
        width: 228,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    detailWrapper: {
        marginTop: 20,
        marginBottom: 20
    },
    messageDetail: {
        marginBottom: 10,
        flexDirection: 'row',
    },
    detailHeading: {
        width: 175,
        textAlign: 'right',
        marginRight: 20,
        fontSize: 15
    },
    detailContent: {
        fontWeight: 'bold',
		maxWidth: Dimensions.get('window').width - 200,
        fontSize: 15
    },
    watchVideosButton: {
        paddingTop: 5,
        paddingBottom: 5,
		marginHorizontal: 15,
        borderRadius: 5,
        overflow: 'hidden',
        alignSelf: 'stretch',
        marginBottom: 20,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
});

@autobind
export default class WatchVideoForm extends Component {

    static propTypes = {
        onDislike: React.PropTypes.func,
        onLike: React.PropTypes.func,
        videoUrl: React.PropTypes.string,
        message: React.PropTypes.object,
        me: React.PropTypes.object
    }

    state = {
        showFeedbackModal: false
    }

    showErrorMessage() {}

    render() {

        let selectedVideoUrl = null;
        if (this.props.message && this.props.videoUrl) {
            selectedVideoUrl = this.props.videoUrl;
        }

        return (
            <View style={styles.app}>
                <ScrollView keyboardShouldPersistTaps={"handled"} style={styles.scrollView}>
                    <View style={styles.videoWrapper}>
                        <VideoPlayer
                            onVideoEnd={()=> this.setState({ showFeedbackModal: true })}
                            showSlider={false}
                            videoUrl={selectedVideoUrl}
                            onError={this.showErrorMessage}
                            initialLoading={true} />

                        {this.props.message && <View style={styles.videoInformation}>
                            <Text style={styles.videoHeading}>{this.props.message.subject}</Text>

                            <View>
                                {this.props.message.sender && <Text style={styles.videoSubheading}>{this.props.message.sender.firstName} {this.props.message.sender.lastName}</Text>}
                                <Text style={styles.videoSubheading}>{this.props.message.organisation}</Text>
                            </View>
                        </View>}

                        <Hr lineColor='#b3b3b3' />

                        {this.props.message && <View style={{flex: 1, alignItems: 'center'}}>
                            <View style={styles.detailWrapper}>
                                <View style={{flex: 1, alignItems: 'center'}}>
									<View style={styles.detailWrapper}>
										{false && <View style={styles.messageDetail}>
											<Text style={styles.detailHeading}>Money Raised Today:</Text>
											<Text numberOfLines={1} style={styles.detailContent}>£300</Text>
										</View>}
										<View style={styles.messageDetail}>
											<Text style={styles.detailHeading}>Fee to charity per view:</Text>
											<Text numberOfLines={1} style={styles.detailContent}>£{this.props.me.data.donation}</Text>
										</View>
										<View style={styles.messageDetail}>
											<Text style={styles.detailHeading}>Messages Viewed:</Text>
											<Text numberOfLines={1} style={styles.detailContent}>{this.props.me.data.videosWatched}</Text>
										</View>
										{false && <View style={styles.messageDetail}>
											<Text style={styles.detailHeading}>Time Spent:</Text>
											<Text numberOfLines={1} style={styles.detailContent}>25m 10s</Text>
										</View>}
										<View style={styles.messageDetail}>
											<Text style={styles.detailHeading}>All-time Money Raised:</Text>
											<Text numberOfLines={1} style={styles.detailContent}>£{this.props.me.data.totalRaised}</Text>
										</View>
									</View>
								</View>
                            </View>
                        </View>}

                        {false && this.props.message && this.props.me.receivingMessages.length > 1 && <View style={styles.watchVideosButton}>
                            <Button
                                onPress={() => this.props.onReportVideo()}
                                title={'Report error and skip video'}
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                            />
                        </View>}
                    </View>
                </ScrollView>

                <View style={styles.bottomViewerWrapper}>
                    <View>
                        <Text style={styles.viewerHeading}>Viewer Must Be:</Text>
                        <Text style={styles.viewerName}>{this.props.me.data.firstName} {this.props.me.data.lastName}</Text>
                        <Text style={styles.viewerTerms}>(Watching on their behalf is fraudulent)</Text>
                    </View>
                </View>

                {(false || this.state.showFeedbackModal) && <ReactionModal onLike={this.props.onLike} onDislike={this.props.onDislike} />}
            </View>
        );
    }
}
