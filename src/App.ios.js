import React, { Component, Image } from 'react';
import { Provider, connect } from 'react-redux';
import { getScene } from './services/navigation';
import {
    NavigatorIOS
} from 'react-native';
import Drawer from 'react-native-drawer';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';

import * as storage from './services/storage';
import store from './store';
import * as actions from './actions';
import SplashScene from './containers/SplashScene';
import Menu from './containers/Menu';

@autobind
export default class App extends Component {

    static drawer;

    state = {
        loading: true
    }

    async componentWillMount() {
        this.refreshAuthToken();
    }

    async refreshAuthToken() {
        let token = await storage.getAuthToken();
        if (!token) {
            store.dispatch(actions.logout());
            this.setState({ loading: false });
            return;
        }

        store.dispatch(actions.testConnection(connectionStatus => {
            if (connectionStatus.connection) {
                store.dispatch(actions.refreshToken(token, ()=> {
                    this.setState({ loading: false });
                }));
            } else {
                this.setState({ connection: false });
            }
        }));
    }

    render() {
        return (
            <Provider store={store}>
                {this.state.loading? <SplashScene onRetry={this.refreshAuthToken} /> : <Navigation />}
            </Provider>
        );
    }
}

@connect(state => ( { isLoggedIn: !!state.auth, links: state.links.data } ), dispatch => ({ actions: bindActionCreators(actions, dispatch) }))
@autobind
class Navigation extends Component {
    static propTypes = {
        isLoggedIn: React.PropTypes.bool
    }

    componentDidMount() {
        this.props.actions.getLinks();
    }

    getNavigator() {
        return this.refs.navigator;
    }

    getDrawer() {
        return App.drawer;
    }

    closeDrawer() {
        App.drawer.close();
    }

    render() {

        if(this.props.isLoggedIn) {

            return (
                <Drawer
                    ref={(ref) => { App.drawer = ref; }}
                    type='overlay'
                    content={<Menu navigator={this.getNavigator} drawer={this.getDrawer} closeDrawer={this.closeDrawer}/>}
                    tapToClose={true}
                    openDrawerOffset={0.2}
                    panCloseMask={0.2}
                    closedDrawerOffset={-3}
                    styles={{
                        drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3 },
                        main: { paddingLeft: 3 }
                    }}
                    tweenHandler={(ratio) => ({
                        main: { opacity: (2 - ratio) / 2 }
                    })}>
                    <NavigatorIOS
                        titleTextColor='#fff'
                        translucent={false}
                        style={{flex: 1}}
                        barTintColor='#61b746'
                        tintColor='#fff'
                        ref='navigator'
                        initialRoute={getScene('home')}
                        interactivePopGestureEnabled={false}
                    />
                </Drawer>
            );
        }

        return (
            <NavigatorIOS
                navigationBarHidden={true}
                titleTextColor='#fff'
                tintColor='#fff'
                barTintColor='#61b746'
				translucent={false}
                style={{flex: 1}}
                ref='navigator'
                initialRoute={getScene('login')}
                interactivePopGestureEnabled={false}
            />
        );
    }
}