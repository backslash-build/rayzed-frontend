import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { getScene } from './services/navigation';
import {
    DrawerLayoutAndroid,
    Navigator
} from 'react-native';
import autobind from 'autobind-decorator';

import * as storage from './services/storage';
import store from './store';
import * as actions from './actions';
import SplashScene from './containers/SplashScene';
import Menu from './containers/Menu';

// const xhr12341234 = GLOBAL.originalXMLHttpRequest? GLOBAL.originalXMLHttpRequest: GLOBAL.XMLHttpRequest;
// XMLHttpRequest = xhr12341234;

@autobind
export default class App extends Component {

    static drawer;

    state = {
        loading: true
    }

    async componentWillMount() {
        this.refreshAuthToken();
    }

    async refreshAuthToken() {
        let token = await storage.getAuthToken();
        if (!token) {
            store.dispatch(actions.logout());
            this.setState({ loading: false });
            return;
        }

        store.dispatch(actions.testConnection(connectionStatus => {
            if (connectionStatus.connection) {
                store.dispatch(actions.refreshToken(token, ()=> {
                    this.setState({ loading: false });
                }));
            } else {
                this.setState({ connection: false });
            }
        }));
    }

    render() {
        return (
            <Provider store={store}>
                {this.state.loading? <SplashScene onRetry={this.refreshAuthToken} /> : <Navigation />}
            </Provider>
        );
    }
}

@connect(state => ({ isLoggedIn: !!state.auth}))
@autobind
class Navigation extends Component {
    static propTypes = {
        isLoggedIn: React.PropTypes.bool
    }

    renderMenu() {
        return <Menu navigator={this.getNavigator} drawer={this.getDrawer} closeDrawer={this.closeDrawer}/>;
    }

    getNavigator() {
        return this.refs.navigator;
    }

    getDrawer() {
        return App.drawer;
    }

    closeDrawer() {
        App.drawer.closeDrawer();
    }

    render() {
        if(this.props.isLoggedIn) {
            return (
                <DrawerLayoutAndroid
                    ref={(ref) => { App.drawer = ref; }}
                    drawerWidth={300}
                    drawerPosition={DrawerLayoutAndroid.positions.Left}
                    renderNavigationView={() => this.renderMenu()}
                >
                <Navigator
                    ref='navigator'
                    initialRoute={getScene('home')}
                    renderScene={ (scene, navigator) => {
                        return <scene.component navigator={navigator} />;
                    }}
                    configureScene={() => Navigator.SceneConfigs.FadeAndroid}
                />
                </DrawerLayoutAndroid>
            );
        }

        return (
            <Navigator
                ref='navigator'
                initialRoute={getScene('login')}
                renderScene={ (scene, navigator) => {
                    return <scene.component navigator={navigator} />;
                }}
                configureScene={() => Navigator.SceneConfigs.FadeAndroid}
            />
        );
    }
}