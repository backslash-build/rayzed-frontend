/* Connection */
export function connectionEstablished() {
    return { type: 'CONNECTION_ESTABLISHED' };
}
export function connectionLost() {
    return { type: 'CONNECTION_LOST' };
}
export function testConnection(callback) {
    console.log('Testing connection');
    return { type: 'TEST_CONNECTION_START', callback }
}
export function testConnectionSuccess() {
    return { type: 'TEST_CONNECTION_SUCCESS' }
}
export function testConnectionFail() {
    return { type: 'TEST_CONNECTION_FAIL' }
}

export function getLinks() {
    return { type: 'LINKS_START' };
}
export function getLinksSuccess(payload) {
    return { type: 'LINKS_SUCCESS', payload };
}

/* Login */
export function login(payload, callback) {
    return { type: 'LOGIN_START', payload, callback };
}
export function refreshToken(payload, callback) {
    console.log('Going to refresh the token');
    return { type: 'REFRESH_TOKEN', payload, callback };
}
export function loginSuccess(payload) {
    return { type: 'LOGIN_SUCCESS', payload };
}
export function loginFail(payload) {
    return { type: 'LOGIN_FAIL', payload };
}
export function logout(callback) {
    return { type: 'LOGOUT', callback };
}

/* Register */
export function register(payload, callback) {
    return { type: 'REGISTER_START', payload, callback };
}
export function registerFail(payload) {
    return { type: 'REGISTER_FAIL', payload };
}
export function registerClear() {
    return { type: 'REGISTER_CLEAR' };
}

/* User */
export function getMe(payload, callback) {
    return { type: 'GETME_START', payload, callback };
}
export function getMeSuccess(payload, callback) {
    return { type: 'GETME_SUCCESS', payload, callback };
}
export function getMeFail(payload, callback) {
    return { type: 'GETME_FAIL', payload, callback };
}
export function updateMe(payload, callback) {
    return { type: 'UPDATE_ME_START', payload, callback };
}
export function updateMeSuccess(payload, callback) {
    return { type: 'UPDATE_ME_SUCCESS', payload, callback };
}
export function updateMeFail(payload, callback) {
    return { type: 'UPDATE_ME_FAIL', payload, callback };
}
export function updatePassword(payload, callback) {
    return { type: 'UPDATE_PASSWORD_START', payload, callback };
}
export function updatePasswordSuccess(payload, callback) {
    return { type: 'UPDATE_PASSWORD_SUCCESS', payload, callback };
}
export function updatePasswordFail(payload, callback) {
    return { type: 'UPDATE_PASSWORD_FAIL', payload, callback };
}

/* Recipient */
export function signupToReceiveMessages(payload) {
    return { type: 'SIGNUP_TO_RECEIVE_MESSAGES', payload };
}




/* All Charities */
export function getAllCharities(payload) {
    return { type: 'GET_ALL_CHARITIES_START', payload };
}
export function getAllCharitiesSuccess(payload) {
    return { type: 'GET_ALL_CHARITIES_SUCCESS', payload };
}
export function getAllCharitiesFail(payload) {
    return { type: 'GET_ALL_CHARITIES_FAIL', payload };
}

/* My Charities */
export function attachCharity(payload) {
    return { type: 'ATTACH_CHARITY_START', payload };
}
export function attachCharitySuccess(payload) {
    return { type: 'ATTACH_CHARITY_SUCCESS', payload };
}
export function attachCharityFail(payload) {
    return { type: 'ATTACH_CHARITY_FAIL', payload };
}

export function detachCharity(payload) {
    return { type: 'DETACH_CHARITY_START', payload };
}
export function detachCharitySuccess(payload) {
    return { type: 'DETACH_CHARITY_SUCCESS', payload };
}
export function detachCharityFail(payload) {
    return { type: 'DETACH_CHARITY_FAIL', payload };
}

export function getMyCharities(payload) {
    return { type: 'GET_MY_CHARITIES_START', payload };
}
export function getMyCharitiesSuccess(payload) {
    return { type: 'GET_MY_CHARITIES_SUCCESS', payload };
}
export function getMyCharitiesFail(payload) {
    return { type: 'GET_MY_CHARITIES_FAIL', payload };
}

export function requestCharity(payload) {
    return { type: 'REQUEST_CHARITY_START', payload };
}
export function requestCharitySuccess(payload) {
    return { type: 'REQUEST_CHARITY_SUCCESS', payload };
}
export function requestCharityFail(payload) {
    return { type: 'REQUEST_CHARITY_FAIL', payload };
}


/* Organisations */
export function getOrganisations(payload, callback) {
    return { type: 'GET_ORGANISATIONS_START', payload, callback };
}
export function getOrganisationsSuccess(payload) {
    return { type: 'GET_ORGANISATIONS_SUCCESS', payload };
}
export function getOrganisationsFail(payload) {
    return { type: 'GET_ORGANISATIONS_FAIL', payload };
}

export function createOrganisation(payload) {
    return { type: 'CREATE_ORGANISATION_START', payload };
}
export function createOrganisationSuccess(payload) {
    return { type: 'CREATE_ORGANISATION_SUCCESS', payload };
}
export function createOrganisationFail(payload) {
    return { type: 'CREATE_ORGANISATION_FAIL', payload };
}

export function updateOrganisation(payload) {
    return { type: 'UPDATE_ORGANISATION_START', payload };
}
export function updateOrganisationSuccess(payload) {
    return { type: 'UPDATE_ORGANISATION_SUCCESS', payload };
}
export function updateOrganisationFail(payload) {
    return { type: 'UPDATE_ORGANISATION_FAIL', payload };
}

export function deleteOrganisation(payload) {
    return { type: 'DELETE_ORGANISATION_START', payload };
}
export function deleteOrganisationSuccess(payload) {
    return { type: 'DELETE_ORGANISATION_SUCCESS', payload };
}
export function deleteOrganisationFail(payload) {
    return { type: 'DELETE_ORGANISATION_FAIL', payload };
}