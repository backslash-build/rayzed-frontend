export function getReceivingMessages(payload, callback) {
    return { type: 'GET_RECEIVING_MESSAGES_START', payload, callback };
}
export function getReceivingMessagesSuccess(payload, callback) {
    return { type: 'GET_RECEIVING_MESSAGES_SUCCESS', payload, callback };
}
export function getReceivingMessagesFail(payload, callback) {
    return { type: 'GET_RECEIVING_MESSAGES_FAIL', payload, callback };
}

export function getLikedMessages(payload, callback) {
    return { type: 'GET_LIKED_MESSAGES_START', payload, callback };
}
export function getLikedMessagesSuccess(payload, callback) {
    return { type: 'GET_LIKED_MESSAGES_SUCCESS', payload, callback };
}
export function getLikedMessagesFail(payload, callback) {
    return { type: 'GET_LIKED_MESSAGES_FAIL', payload, callback };
}

export function getSentMessages(payload, callback) {
    return { type: 'GET_SENT_MESSAGES_START', payload, callback };
}
export function getSentMessagesSuccess(payload, callback) {
    return { type: 'GET_SENT_MESSAGES_SUCCESS', payload, callback };
}
export function getSentMessagesFail(payload, callback) {
    return { type: 'GET_SENT_MESSAGES_FAIL', payload, callback };
}

export function getDraftMessages(payload, callback) {
    return { type: 'GET_DRAFT_MESSAGES_START', payload, callback };
}
export function getDraftMessagesSuccess(payload, callback) {
    return { type: 'GET_DRAFT_MESSAGES_SUCCESS', payload, callback };
}
export function getDraftMessagesFail(payload, callback) {
    return { type: 'GET_DRAFT_MESSAGES_FAIL', payload, callback };
}

export function getPendingMessages(payload, callback) {
    return { type: 'GET_PENDING_MESSAGES_START', payload, callback };
}
export function getPendingMessagesSuccess(payload, callback) {
    return { type: 'GET_PENDING_MESSAGES_SUCCESS', payload, callback };
}
export function getPendingMessagesFail(payload, callback) {
    return { type: 'GET_PENDING_MESSAGES_FAIL', payload, callback };
}

export function createNewMessage(payload, callback) {
    return { type: 'CREATE_NEW_MESSAGE_START', payload, callback };
}
export function createNewMessageSuccess(payload, callback) {
    return { type: 'CREATE_NEW_MESSAGE_SUCCESS', payload, callback };
}
export function createNewMessageFail(payload, callback) {
    return { type: 'CREATE_NEW_MESSAGE_FAIL', payload, callback };
}


export function localUpdateMessage(payload, callback) {
    return { type: 'LOCAL_UPDATE_MESSAGE', payload, callback };
}

export function updateMessage(payload, callback) {
    return { type: 'UPDATE_MESSAGE_START', payload, callback };
}
export function updateMessageSuccess(payload, callback) {
    return { type: 'UPDATE_MESSAGE_SUCCESS', payload, callback };
}
export function updateMessageFail(payload, callback) {
    return { type: 'UPDATE_MESSAGE_FAIL', payload, callback };
}


export function getMessage(payload, callback) {
    return { type: 'GET_MESSAGE_START', payload, callback };
}
export function getMessageSuccess(payload, callback) {
    return { type: 'GET_MESSAGE_SUCCESS', payload, callback };
}
export function getMessageFail(payload, callback) {
    return { type: 'GET_MESSAGE_FAIL', payload, callback };
}


export function getMessagePaymentDetails(payload, callback) {
    console.log("TENAFOIEF");
    return { type: 'GET_MESSAGE_PAYMENT_DETAILS_START', payload, callback };
}
export function getMessagePaymentDetailsSuccess(payload, callback) {
    return { type: 'GET_MESSAGE_PAYMENT_DETAILS_SUCCESS', payload, callback };
}
export function getMessagePaymentDetailsFail(payload, callback) {
    return { type: 'GET_MESSAGE_PAYMENT_DETAILS_FAIL', payload, callback };
}


export function uploadVideo(payload, progress, callback) {
    return { type: 'UPLOAD_VIDEO_START', payload, progress, callback };
}
export function uploadVideoSuccess(payload, callback) {
    return { type: 'UPLOAD_VIDEO_SUCCESS', payload, callback };
}
export function uploadVideoFail(payload, callback) {
    return { type: 'UPLOAD_VIDEO_FAIL', payload, callback };
}

export function downloadVideo(payload, callback) {
    return { type: 'DOWNLOAD_VIDEO_START', payload, callback };
}
export function downloadVideoSuccess(payload, callback) {
    return { type: 'DOWNLOAD_VIDEO_SUCCESS', payload, callback };
}
export function downloadVideoFail(payload, callback) {
    return { type: 'DOWNLOAD_VIDEO_FAIL', payload, callback };
}

export function getAttachments(payload, callback) {
    return { type: 'GET_ATTACHMENTS_START', payload, callback };
}
export function getAttachmentsSuccess(payload, callback) {
    return { type: 'GET_ATTACHMENTS_SUCCESS', payload, callback };
}
export function getAttachmentsFail(payload, callback) {
    return { type: 'GET_ATTACHMENTS_FAIL', payload, callback };
}

export function uploadAttachments(payload, progress, callback) {
    return { type: 'UPLOAD_ATTACHMENTS_START', payload, progress, callback };
}
export function uploadAttachmentsSuccess(payload, callback) {
    return { type: 'UPLOAD_ATTACHMENTS_SUCCESS', payload, callback };
}
export function uploadAttachmentsFail(payload, callback) {
    return { type: 'UPLOAD_ATTACHMENTS_FAIL', payload, callback };
}

export function downloadAttachment(payload, callback) {
    return { type: 'DOWNLOAD_ATTACHMENT_START', payload, callback };
}
export function downloadAttachmentSuccess(payload, callback) {
    return { type: 'DOWNLOAD_ATTACHMENT_SUCCESS', payload, callback };
}
export function downloadAttachmentFail(payload, callback) {
    return { type: 'DOWNLOAD_ATTACHMENT_FAIL', payload, callback };
}

export function deleteAttachment(payload, callback) {
    return { type: 'DELETE_ATTACHMENT_START', payload, callback };
}
export function deleteAttachmentSuccess(payload, callback) {
    return { type: 'DELETE_ATTACHMENT_SUCCESS', payload, callback };
}
export function deleteAttachmentFail(payload, callback) {
    return { type: 'DELETE_ATTACHMENT_FAIL', payload, callback };
}

export function getAllRecipients(payload, callback) {
    return { type: 'GET_ALL_RECIPIENTS_START', payload, callback };
}
export function getAllRecipientsSuccess(payload, callback) {
    return { type: 'GET_ALL_RECIPIENTS_SUCCESS', payload, callback };
}
export function getAllRecipientsFail(payload, callback) {
    return { type: 'GET_ALL_RECIPIENTS_FAIL', payload, callback };
}

export function searchRecipients(payload, callback) {
    return { type: 'SEARCH_RECIPIENTS_START', payload, callback };
}
export function searchRecipientsSuccess(payload, callback) {
    return { type: 'SEARCH_RECIPIENTS_SUCCESS', payload, callback };
}
export function searchRecipientsFail(payload, callback) {
    return { type: 'SEARCH_RECIPIENTS_FAIL', payload, callback };
}

export function addRecipient(payload) {
    return { type: 'ADD_RECIPIENT', payload };
}
export function removeRecipient(payload) {
    return { type: 'REMOVE_RECIPIENT', payload };
}
export function cleanMessage() {
    return { type: 'CLEAN_MESSAGE' };
}

export function sendMessage(payload, callback) {
    return { type: 'SEND_MESSAGE_START', payload, callback };
}
export function sendMessageSuccess(payload, callback) {
    return { type: 'SEND_MESSAGE_SUCCESS', payload, callback };
}
export function sendMessageFail(payload, callback) {
    return { type: 'SEND_MESSAGE_FAIL', payload, callback };
}


export function likeMessage(payload, callback) {
    return { type: 'LIKE_MESSAGE_START', payload, callback};
}
export function likeMessageSuccess(payload, callback) {
    return { type: 'LIKE_MESSAGE_SUCCESS', payload, callback};
}
export function likeMessageFail(payload, callback) {
    return { type: 'LIKE_MESSAGE_FAIL', payload, callback};
}
export function dislikeMessage(payload, callback) {
    return { type: 'DISLIKE_MESSAGE_START', payload, callback};
}
export function dislikeMessageSuccess(payload, callback) {
    return { type: 'DISLIKE_MESSAGE_SUCCESS', payload, callback};
}
export function dislikeMessageFail(payload, callback) {
    return { type: 'DISLIKE_MESSAGE_FAIL', payload, callback};
}