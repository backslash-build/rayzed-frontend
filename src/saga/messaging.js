import { call, put, race } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import * as api from '../services/api';
import * as generalActions from '../actions';
import * as actions from '../actions/messaging';
import { uploadFile } from '../services/uploader';
import { downloadPdf } from '../services/downloader';

function *callWrapper(endpoint, parameters) {

    let error = null;

    try {

        const {response, timeout} = yield race({
            response: call(endpoint, parameters),
            timeout: call(delay, 10000)
        });

        if(timeout) {
            throw new api.exceptions.AuthenticationException('I\'m sorry but there appears to be a network issue');
            error = 'I\'m sorry but there appears to be a network issue';
        } else {
            return response;
        }
    } catch (e) {

        error = "Unable to connect to the server.";

        if (e.message == 'I\'m sorry but there appears to be a network issue' || e.message == "Unable to connect to the server.") {
            yield put(generalActions.testConnectionFail());
        }
    }

    if (error) {
        throw new api.exceptions.BadRequestException(error);
    }
}

export function *takeGetSentMessages(action) {

    let resultsLength = 0;
    let page = action.payload? action.payload.page : 0;

    try {
//        let page = 0;
//        let results = [];
//        let json = null;
//        do {
//            json = yield call(api.getSendingMessages, page++);
//            results = results.concat(json);
//            yield put(actions.getSendingMessagesSuccess(results));
//        } while(json.length > 0)

        const json = yield callWrapper(api.getSentMessages, page);
        resultsLength = json.length;
        console.log(json);
		yield put(actions.getSentMessagesSuccess(json));
    } catch(e) {
        console.log(e);
        yield put(actions.getSentMessagesFail(e.message));
    }

	if(action.callback) {
        action.callback(resultsLength);
    }
}

export function *takeGetDraftMessages(action) {

    let resultsLength = 0;
    let page = action.payload? action.payload.page : 0;

    try {
//        let page = 0;
//        let results = [];
//        let json = null;
//        do {
//            json = yield call(api.getSendingMessages, page++);
//            results = results.concat(json);
//            yield put(actions.getSendingMessagesSuccess(results));
//        } while(json.length > 0)

        const json = yield callWrapper(api.getDraftMessages, page);
		resultsLength = json.length;
		yield put(actions.getDraftMessagesSuccess(json));
    } catch(e) {
        console.log(e);
        yield put(actions.getDraftMessagesFail(e.message));
    }

	if(action.callback) {
        action.callback(resultsLength);
    }
}

export function *takeGetPendingMessages(action) {

    let resultsLength = 0;
    let page = action.payload? action.payload.page : 0;

    try {
//        let page = 0;
//        let results = [];
//        let json = null;
//        do {
//            json = yield call(api.getSendingMessages, page++);
//            results = results.concat(json);
//            yield put(actions.getSendingMessagesSuccess(results));
//        } while(json.length > 0)

        const json = yield callWrapper(api.getPendingMessages, page);
        console.log("PENDING MESSAGES", json);
        resultsLength = json.length;
        yield put(actions.getPendingMessagesSuccess(json));
    } catch(e) {
        console.log(e);
        yield put(actions.getPendingMessagesFail(e.message));
    }

    if(action.callback) {
        action.callback(resultsLength);
    }
}

export function *takeUpdateMessage(action) {

	let response = null;

    console.log("UPDATING MESSAGE");

    try {
        const json = yield callWrapper(api.updateMessage, action.payload);
        response = json;
        console.log("UPDATING MESSAGE RESPONSE", response);
        yield put(actions.updateMessageSuccess(json));
    } catch(e) {
        yield put(actions.updateMessageFail(e.message));
        return;
    }

    if(action.callback) {
        action.callback(response);
    }
}

export function *takeCreateMessage(action) {
    try {
        const json = yield callWrapper(api.createMessage, action.payload);
		console.log("CREATING MESSAGE", json);
        yield put(actions.createNewMessageSuccess(json));
    } catch(e) {
		console.log("CREATING MESSAGE", e);
        yield put(actions.createNewMessageFail(e.message));
        return;
    }

    if(action.callback) {
        action.callback();
    }
}

export function *takeGetReceivingMessages(action) {

	let resultsLength = 0;

    try {
        const json = yield callWrapper(api.getReceivedPendingMessages);
		resultsLength = json.length;
        yield put(actions.getReceivingMessagesSuccess(json));
    } catch(e) {
        yield put(actions.getReceivingMessagesFail(e.message));
    }

    if(action.callback) {
        action.callback(resultsLength);
    }
}

export function *takeGetLikedMessages(action) {

    let resultsLength = 0;
    let page = action.payload? action.payload.page : 0;

    try {
//        let page = 0;
//        let results = [];
//        let json = null;
//        do {
//            json = yield call(api.getSendingMessages, page++);
//            results = results.concat(json);
//            yield put(actions.getSendingMessagesSuccess(results));
//        } while(json.length > 0)

        const json = yield callWrapper(api.getReceivedLikedMessages, page);
		resultsLength = json.length;
		yield put(actions.getLikedMessagesSuccess(json));
    } catch(e) {
		console.log("ERROR", e);
        yield put(actions.getLikedMessagesFail(e.message));
    }

	if(action.callback) {
        action.callback(resultsLength);
    }
}

export function *takeGetMessage(action) {
    let messageDetails = null;
    try {
        const json = yield callWrapper(api.getMessage, action.payload);
        yield put(actions.getMessageSuccess(json));
        messageDetails = json;
    } catch(e) {
        yield put(actions.getMessageFail(e.message));
    }

    if(action.callback) {
        action.callback(messageDetails);
    }
}

export function *takeGetMessagePaymentDetails(action) {
    let paymentDetails = null;
    try {
        console.log("Getting payment details");
        const json = yield callWrapper(api.getMessagePaymentDetails, action.payload);
        console.log(json);
        yield put(actions.getMessagePaymentDetailsSuccess(json));
        paymentDetails = json;
        console.log("Payment details", json);
    } catch(e) {
        yield put(actions.getMessagePaymentDetailsFail(e.message));
    }

    if(action.callback) {
        action.callback(paymentDetails);
    }
}

export function *takeUploadVideo(action) {
    try {
        action.payload.metadata = {
            mimeType: 'video/mp4',
            extension: 'mp4',
            fileSize: 100000
        };
        const {url} = yield callWrapper(api.createMessageVideoUploadUrl, action.payload);
        yield uploadFile(action.payload.path, url, action.payload.metadata, 'Video', action.progress);
        yield put(actions.uploadVideoSuccess());
    } catch(e) {
        console.log("UPLOAD VIDEO ERROR", e);
        if (e) {
            yield put(actions.uploadVideoFail(e.message));
        }
        return;
    }

    if(action.callback) {
        action.callback();
    }
}

export function *takeDownloadVideo(action) {
    try {
        const {url} = yield callWrapper(api.createMessageVideoDownloadUrl, action.payload);
        console.log("VIDEO URL", url);
        yield put(actions.downloadVideoSuccess(url));
    } catch(e) {
        console.log(e);
        yield put(actions.downloadVideoFail(e.message));
    }
}

export function *takeGetAttachments(action) {
    try {
        const json = yield callWrapper(api.getMessageAttachments, action.payload);
        yield put(actions.getAttachmentsSuccess(json));
    } catch(e) {
        console.log(e);
        yield put(actions.getAttachmentsFail(e.message));
    }

    if(action.callback) {
        action.callback();
    }
}

export function *takeUploadAttachments(action) {
    try {
        let fileCount = 1;
        for (let file of action.payload.files) {
            // how can this be done for different video files and pdfs?
            action.payload.metadata = {
                title: file.title,
                mimeType: file.mimeType,
                extension: file.extension,
                fileSize: file.fileSize
            };
            const {url} = yield callWrapper(api.createMessageAttachmentUploadUrl, action.payload);
            console.log("PATH", file.path, "URL", url);
            yield uploadFile(file.path, url, action.payload.metadata, 'File ' + fileCount, action.progress);
            fileCount += 1;
        }
        yield put(actions.uploadAttachmentsSuccess());
    } catch(e) {

		if (e) {
			console.log(e);
        	yield put(actions.uploadAttachmentsFail(e.message));
		}
        return;
    }

    if(action.callback) {
        action.callback();
    }
}

export function *takeDownloadAttachment(action) {

    let url = "";

    try {
        let res = yield callWrapper(api.createMessageAttachmentDownloadUrl, action.payload);
        const url = yield downloadPdf(res.url, action.payload.name.replace(" ", "-"));
        yield put(actions.downloadAttachmentSuccess(url));
    } catch(e) {
		console.log("ERROR", e);
        if(action.callback) {
            action.callback(e.message, url);
        }
        yield put(actions.downloadAttachmentFail(e.message));
        return
    }

    if(action.callback) {
        action.callback(null, url);
    }
}

export function *takeDeleteAttachment(action) {
    try {
        yield callWrapper(api.deleteMessageAttachment, action.payload);
        yield put(actions.deleteAttachmentSuccess());
        yield put(actions.getAttachments(action.payload.id));
    } catch(e) {
        yield put(actions.deleteAttachmentFail(e.message));
    }
}

export function *takeGetAllRecipients() {
    try {
        const json = yield callWrapper(api.getAllRecipients);
        yield put(actions.getAllRecipientsSuccess(json));
    } catch(e) {
        console.log(e.message);
        yield put(actions.getAllRecipientsFail(e.message));
    }
}


export function *takeSearchRecipients(action) {
    try {
        const json = yield callWrapper(api.searchRecipients, action.payload);
        yield put(actions.searchRecipientsSuccess(json));
        console.log("FIND RECIPIENT", json)
    } catch(e) {
        console.log("FIND RECIPIENT ERROR", e);
        yield put(actions.searchRecipientsFail(e.message));
    }
}


export function *takeSendMessage(action) {

    let paymentDetails = null;

    console.log(action);
    console.log(action.payload);

    try {
        const json = yield callWrapper(api.sendMessage, action.payload);
        console.log("SEND MESSAGE SUCCESS", json);
        yield put(actions.sendMessageSuccess(json));
        yield put(actions.getSentMessages());
        yield put(actions.getDraftMessages());
        yield put(actions.getPendingMessages());
		paymentDetails = json;
    } catch(e) {
        action.callback({ error: e.message });
        yield put(actions.sendMessageFail(e.message));
        return;
    }

    if(action.callback) {
		console.log("Calling back", paymentDetails);
        action.callback(paymentDetails);
    }
}

export function *takeLikeMessage(action) {
    try {
        yield callWrapper(api.likeMessage, action.payload);
        yield put(actions.likeMessageSuccess());
		yield put(actions.getReceivingMessages());
        yield put(actions.getLikedMessages(0));
        yield put(actions.getMe());
    } catch(e) {
        yield put(actions.likeMessageFail(e));
        return;
    }

    if(action.callback) {
        action.callback();
    }
}

export function *takeDislikeMessage(action) {
    try {
        yield callWrapper(api.dislikeMessage, action.payload);
        yield put(actions.dislikeMessageSuccess());
		yield put(actions.getReceivingMessages());
        yield put(actions.getMe());
    } catch(e) {
        yield put(actions.dislikeMessageFail(e));
        return;
    }

    if(action.callback) {
        action.callback();
    }
}