import { takeEvery, takeLatest, call, put, race } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import {customError} from '../utils/custom-error';

import * as api from '../services/api';
import * as storage from '../services/storage';
import * as actions from '../actions';

import * as messaging from './messaging';


function *callWrapper(endpoint, parameters) {

    let error = null;

    try {

        const {response, timeout} = yield race({
            response: call(endpoint, parameters),
            timeout: call(delay, 10000)
        });

        if(timeout) {
            throw new api.exceptions.AuthenticationException('I\'m sorry but there appears to be a network issue');
            error = 'I\'m sorry but there appears to be a network issue';
        } else {
            return response;
        }
    } catch (e) {

        error = "Unable to connect to the server.";

        if (e.message == 'I\'m sorry but there appears to be a network issue' || e.message == "Unable to connect to the server.") {
            yield put(actions.testConnectionFail());
        }
    }

    if (error) {
        throw new api.exceptions.BadRequestException(error);
    }
}

function *takeTestConnection(action) {

    let isConnected = null;

    yield delay(1500);

    try {

        const {connected, timeout} = yield race({
            connected: call(api.ping),
            timeout: call(delay, 10000)
        });

        if(timeout) {
            throw new api.exceptions.AuthenticationException('I\'m sorry but there appears to be a network issue');
        } else {
            isConnected = connected;
        }
    } catch (e) {

        yield put(actions.testConnectionFail());

        if (action.callback) {
            action.callback({ connection: false, message: e.message });
        }
        return;
    }

    if (isConnected) {
        yield put(actions.testConnectionSuccess());
    } else {
        yield put(actions.testConnectionFail());
    }

    if (action.callback) {
        action.callback({ connection: isConnected });
    }
}

function *takeGetLinks(action) {
    try {
        console.log("Getting links");
        const json = yield callWrapper(api.getLinks);
        yield put(actions.getLinksSuccess(json));
        console.log(json);
    } catch(e) {
        //yield put(actions.getMeFail(e.message));
    }

    if(action.callback) {
        action.callback();
    }
}

function *takeLogin(action) {
    try {
        const {json, timeout} = yield race({
            json: call(api.login, action.payload),
            timeout: call(delay, 10000)
        });

        if(timeout) {
            throw new api.exceptions.AuthenticationException('Login request timed out.');
        }

        yield put(actions.loginSuccess(json));
    } catch(e) {
        yield put(actions.loginFail(e.message));
        return;
    }

    if(action.callback) {
        action.callback();
    }
}

function *takeRefreshToken(action) {
    console.log("Refreshing the token 2!!!");
    try {
        if(!(yield call(storage.hasAuthToken))) {
            console.log("No auth token, logging out");
            throw Error('no auth token, logging out');
        }

        const token = yield call(storage.getAuthToken);
        const json = yield call(api.refreshToken, token.refresh_token);
        console.log("Refreshed token successfully");
        yield put(actions.loginSuccess(json));
    } catch(e) {
        yield put(actions.logout());
    }

    if(action.callback) {
        action.callback();
    }
}


let refreshTimeout = null;
function *takeLoginSuccess(action) {
    console.log('Successful LOGIN');

    try {
        yield call(storage.saveAuthToken, action.payload);
        const expiresIn = action.payload["expires_in"];
        yield call(delay, ((expiresIn * 1000) / 2));
        yield put(actions.refreshToken());
    } catch(e) {
        console.log(e);
    }
}

function *takeRegister(action) {
    try {

        const {res, timeout} = yield race({
            res: call(api.createUser, action.payload),
            timeout: call(delay, 10000)
        });

        if(timeout) {
            throw new api.exceptions.AuthenticationException('Register request timed out.');
        }

        // if we're still here then the registration call was successful, now we want to log in with the same user
        const json = yield call(api.login, {
            username: action.payload.username,
            password: action.payload.password
        });

        // now we can send the login success event to clear up the register form
        yield put(actions.loginSuccess(json));
    } catch(e) {
        yield put(actions.registerFail(e.message));
        return;
    }

    if(action.callback) {
        action.callback();
    }
}

function *takeLogout(action) {
    yield call(storage.deleteAuthToken);

    if(action.callback) {
        action.callback();
    }
}

function *takeGetMe(action) {
    try {
        const json = yield callWrapper(api.getUser);
        yield put(actions.getMeSuccess(json));
    } catch(e) {
        yield put(actions.getMeFail(e.message));
    }

    if(action.callback) {
        action.callback();
    }
}

function *takeUpdateMe(action) {
    try {
        const json = yield callWrapper(api.updateUser, action.payload);
        yield put(actions.updateMeSuccess(json));
        yield put(actions.getMe());
    } catch(e) {
        yield put(actions.updateMeFail(e.message));
    }

    if(action.callback) {
        action.callback();
    }
}

function *takeUpdatePassword(action) {
    try {
        const json = yield callWrapper(api.updatePassword, action.payload);
        yield put(actions.updatePasswordSuccess(json));
        yield put(actions.getMe());
    } catch(e) {
        yield put(actions.updatePasswordFail(e.message));
    }

    if(action.callback) {
        action.callback();
    }
}

function *takeSignUpToReceiveMessages() {
    try {
        console.log("Signing up to receive message");
        yield callWrapper(api.becomeRecipient);
        const json = yield callWrapper(api.getUser);
        yield put(actions.getMeSuccess(json));
        console.log("")
    } catch(e) {
        console.log("Error signing up", e);
        yield put(actions.getMeFail(e.message));
    }
}

function *takeGetAllCharities() {
    try {
        const json = yield callWrapper(api.getAllCharities);
        yield put(actions.getAllCharitiesSuccess(json));
    } catch (e) {
        yield put(actions.getAllCharitiesFail(e.message));
    }
}

function *takeGetMyCharities() {
    try {
        const json = yield callWrapper(api.getUserCharities);
        yield put(actions.getMyCharitiesSuccess(json));
    } catch (e) {
        yield put(actions.getMyCharitiesFail(e.message));
    }
}

function *takeRequestCharity(action) {
    try {
        const json = yield callWrapper(api.requestCharity, action.payload);
        console.log("JUST REQUESTED A CHARITYY", json);
        yield put(actions.requestCharitySuccess());
    } catch(e) {
        yield put(actions.requestCharityFail(e.message));
    }
}

function *takeAttachCharity(action) {
    try {
        yield callWrapper(api.attachCharityToUser, action.payload);
        yield put(actions.attachCharitySuccess());
        yield put(actions.getMyCharities());
    } catch(e) {
        console.log("ADDING CHARITY ERROR", e);
        yield put(actions.attachCharityFail(e.message));
    }
}

function *takeDetachCharity(action) {
    try {
        yield callWrapper(api.detachCharityFromUser, action.payload);
        yield put(actions.detachCharitySuccess());
        yield put(actions.getMyCharities());
    } catch(e) {
        yield put(actions.detachCharityFail(e.message));
    }
}

function *takeGetOrganisations(action) {
    try {
        const json = yield callWrapper(api.getOrganisations);
        console.log(json);
        yield put(actions.getOrganisationsSuccess(json));
    } catch (e) {
        if (action.callback) {
            action.callback(e.message);
        }
        yield put(actions.getOrganisationsFail(e.message));
    }

    if(action.callback) {
        action.callback();
    }
}

function *takeCreateOrganisation(action) {
    try {
        yield callWrapper(api.createOrganisation, action.payload);
        yield put(actions.createOrganisationSuccess());
        yield put(actions.getOrganisations());
    } catch(e) {
        console.log("CREATE ORGANISATION ERROR", e);
        yield put(actions.createOrganisationFail(e.message));
    }
}

function *takeUpdateOrganisation(action) {
    try {
        yield callWrapper(api.updateOrganisation, action.payload);
        yield put(actions.updateOrganisationSuccess());
        yield put(actions.getOrganisations());
    } catch(e) {
        yield put(actions.updateOrganisationFail(e.message));
    }
}

function *takeDeleteOrganisation(action) {
    try {
        yield callWrapper(api.deleteOrganisation, action.payload);
        yield put(actions.deleteOrganisationSuccess());
        yield put(actions.getOrganisations());
    } catch(e) {
        yield put(actions.deleteOrganisationFail(e.message));
    }
}



export default function *() {
    yield takeEvery('TEST_CONNECTION_START', takeTestConnection);
    yield takeLatest('LINKS_START', takeGetLinks);
    yield takeLatest('LOGIN_START', takeLogin);
    yield takeLatest('LOGIN_SUCCESS', takeLoginSuccess);
    yield takeLatest('REGISTER_START', takeRegister);
    yield takeEvery('REFRESH_TOKEN', takeRefreshToken);
    yield takeLatest('LOGOUT', takeLogout);

    yield takeLatest('GETME_START', takeGetMe);
    yield takeLatest('UPDATE_ME_START', takeUpdateMe);
    yield takeLatest('UPDATE_PASSWORD_START', takeUpdatePassword);
    yield takeLatest('SIGNUP_TO_RECEIVE_MESSAGES', takeSignUpToReceiveMessages);

    yield takeLatest('GET_ALL_CHARITIES_START', takeGetAllCharities);
    yield takeLatest('GET_MY_CHARITIES_START', takeGetMyCharities);
    yield takeLatest('REQUEST_CHARITY_START', takeRequestCharity);

    yield takeLatest('ATTACH_CHARITY_START', takeAttachCharity);
    yield takeLatest('DETACH_CHARITY_START', takeDetachCharity);

    yield takeLatest('GET_ORGANISATIONS_START', takeGetOrganisations);

    yield takeLatest('CREATE_ORGANISATION_START', takeCreateOrganisation);
    yield takeLatest('UPDATE_ORGANISATION_START', takeUpdateOrganisation);
    yield takeLatest('DELETE_ORGANISATION_START', takeDeleteOrganisation);

    yield takeLatest('GET_RECEIVING_MESSAGES_START', messaging.takeGetReceivingMessages);
    yield takeLatest('GET_LIKED_MESSAGES_START', messaging.takeGetLikedMessages);

    yield takeLatest('GET_SENT_MESSAGES_START', messaging.takeGetSentMessages);
    yield takeLatest('GET_DRAFT_MESSAGES_START', messaging.takeGetDraftMessages);
    yield takeLatest('GET_PENDING_MESSAGES_START', messaging.takeGetPendingMessages);
    yield takeLatest('CREATE_NEW_MESSAGE_SUCCESS', messaging.takeGetDraftMessages);
    yield takeLatest('UPDATE_MESSAGE_SUCCESS', messaging.takeGetDraftMessages);
    yield takeLatest('UPDATE_MESSAGE_SUCCESS', messaging.takeGetSentMessages);
    yield takeLatest('UPDATE_MESSAGE_SUCCESS', messaging.takeGetPendingMessages);

    yield takeLatest('CREATE_NEW_MESSAGE_START', messaging.takeCreateMessage);
    yield takeLatest('UPDATE_MESSAGE_START', messaging.takeUpdateMessage);
    yield takeLatest('GET_MESSAGE_START', messaging.takeGetMessage);
    yield takeLatest('GET_MESSAGE_PAYMENT_DETAILS_START', messaging.takeGetMessagePaymentDetails);

    yield takeLatest('UPLOAD_VIDEO_START', messaging.takeUploadVideo);
    yield takeLatest('DOWNLOAD_VIDEO_START', messaging.takeDownloadVideo);

    yield takeLatest('GET_ATTACHMENTS_START', messaging.takeGetAttachments);
    yield takeLatest('UPLOAD_ATTACHMENTS_START', messaging.takeUploadAttachments);
    yield takeLatest('DOWNLOAD_ATTACHMENT_START', messaging.takeDownloadAttachment);
    yield takeLatest('DELETE_ATTACHMENT_START', messaging.takeDeleteAttachment);

    yield takeLatest('GET_ALL_RECIPIENTS_START', messaging.takeGetAllRecipients);
    yield takeLatest('SEARCH_RECIPIENTS_START', messaging.takeSearchRecipients);

    yield takeLatest('SEND_MESSAGE_START', messaging.takeSendMessage);
    yield takeLatest('SEND_MESSAGE_SUCCESS', messaging.takeGetDraftMessages);
    yield takeLatest('SEND_MESSAGE_SUCCESS', messaging.takeGetSentMessages);
    yield takeLatest('SEND_MESSAGE_SUCCESS', messaging.takeGetPendingMessages);
    yield takeLatest('LIKE_MESSAGE_START', messaging.takeLikeMessage);
    yield takeLatest('LIKE_MESSAGE_SUCCESS', takeGetMe);
    yield takeLatest('DISLIKE_MESSAGE_START', messaging.takeDislikeMessage);
    yield takeLatest('DISLIKE_MESSAGE_SUCCESS', takeGetMe);
}