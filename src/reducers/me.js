const initialState = {
    data: null,
    loading: false,
    error: null,
    sentMessages: [],
    draftMessages: [],
    pendingMessages: [],
    receivingMessages: [],
    likedMessages: []
};

export default function (state = initialState, action) {
    switch(action.type) {
        case 'GETME_START':
            return {...state, error: null};
        case 'GETME_SUCCESS':
            return {...state, data: action.payload, error: null};
        case 'GETME_FAIL':
            return {...state, error: action.payload};

        case 'GET_SENT_MESSAGES_START':
            return {...state, loading: true};
        case 'GET_SENT_MESSAGES_SUCCESS':
            return {...state, loading: false, sentMessages: action.payload};
        case 'GET_SENT_MESSAGES_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'GET_DRAFT_MESSAGES_START':
            return {...state, loading: true};
		case 'GET_DRAFT_MESSAGES_SUCCESS':
            return {...state, loading: false, draftMessages: action.payload};
        case 'GET_DRAFT_MESSAGES_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'GET_PENDING_MESSAGES_START':
            return {...state, loading: true};
        case 'GET_PENDING_MESSAGES_SUCCESS':
            console.log("PENENENNENDNNDNNDNDNDNDNDNDNDNNDND", action.payload);
            return {...state, loading: false, pendingMessages: action.payload};
        case 'GET_PENDING_MESSAGES_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'GET_RECEIVING_MESSAGES_START':
            return {...state, loading: true};
        case 'GET_RECEIVING_MESSAGES_SUCCESS':
            return {...state, loading: false, receivingMessages: action.payload};
        case 'GET_RECEIVING_MESSAGES_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'GET_LIKED_MESSAGES_START':
            return {...state, loading: true};
        case 'GET_LIKED_MESSAGES_SUCCESS':
            return {...state, loading: false, likedMessages: action.payload};
        case 'GET_LIKED_MESSAGES_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'UPDATE_PASSWORD_START':
            return {...state, loading: true, error: null};
        case 'UPDATE_PASSWORD_SUCCESS':
            return {...state, loading: false, data: action.payload, error: null};
        case 'UPDATE_PASSWORD_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'LOGOUT':
            return initialState;

        default:
            return state;
    }
}