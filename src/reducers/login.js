const initialState = {
    loading: false,
    error: null,
    token: null,
    connection: null
};

export default function (state = initialState, action) {
    console.log(action.type);
    switch(action.type) {
        case 'TEST_CONNECTION_START':
            console.log("HERE");
            return {...state, error: null, connection: null};
        case 'TEST_CONNECTION_SUCCESS':
            return {...state, connection: true};
        case 'TEST_CONNECTION_FAIL':
            return {...state, connection: false};
        case 'LOGIN_START':
            return {...state, loading: true, error: null};
        case 'LOGIN_SUCCESS':
            return {...state, loading: false, error: null, token: action.payload.access_token};
        case 'LOGIN_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'LOGOUT':
            return initialState;

        default:
            return state;
    }
}