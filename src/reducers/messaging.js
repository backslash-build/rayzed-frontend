const initialState = {
    message: null,
    video: null,
    attachments: null,
    attachment: null,
    loading: false,
    attachmentsLoading: false,
    videoLoading: false,
    error: null,
    recipients: [],
    searchedRecipients: [],
    messageSent: false,
    payment: null
};

const transformMessage = message => {
    console.log(message);
    return ({
        ...message,
        recipientIds: message.recipients ? message.recipients.map(recipient => recipient.userId) : []
    });
};

export default function (state = initialState, action) {

    switch(action.type) {

        case 'CREATE_NEW_MESSAGE_START':
        case 'UPDATE_MESSAGE_START':
            return {...state, loading: true, error: null};

        case 'CREATE_NEW_MESSAGE_FAIL':
        case 'UPDATE_MESSAGE_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'CREATE_NEW_MESSAGE_SUCCESS':
        case 'UPDATE_MESSAGE_SUCCESS':
            return {...state, loading: false, error: null, message: transformMessage(action.payload)};

        case 'GET_MESSAGE_START':
            return {...state, loading: true, message: { id: action.payload } };
        case 'GET_MESSAGE_SUCCESS':
            return {...state, loading: false, message: transformMessage(action.payload) };
        case 'GET_MESSAGE_FAIL':
            return {...state, loading: false, error: action.payload };

        case 'SEND_MESSAGE_START':
            return {...state, payment: null};
        case 'SEND_MESSAGE_SUCCESS':
            console.log("TEST", action.payload);
            return {...state, payment: action.payload, messageSent: true};
        case 'SEND_MESSAGE_FAIL':
            return {...state, error: action.payload, messageSent: true};

        case 'GET_MESSAGE_PAYMENT_DETAILS_START':
            return {...state, invoice: null };

        case 'GET_MESSAGE_PAYMENT_DETAILS_SUCCESS':
            return {...state, invoice: action.payload };

        case 'LOCAL_UPDATE_MESSAGE':
            return {...state, message: {...state.message, ...action.payload} };

        case 'UPLOAD_VIDEO_SUCCESS':
            return {...state, video: true};

        case 'DOWNLOAD_VIDEO_START':
            return {...state, videoLoading: true };
        case 'DOWNLOAD_VIDEO_SUCCESS':
            return {...state, videoLoading: false, video: action.payload};
        case 'DOWNLOAD_VIDEO_FAIL':
            return {...state, videoLoading: false, error: action.payload};

        case 'UPLOAD_ATTACHMENTS_SUCCESS':
            return {...state, attachments: true};

        case 'GET_ATTACHMENTS_START':
            return {...state, attachmentsLoading: true}
        case 'GET_ATTACHMENTS_SUCCESS':
            return {...state, attachmentsLoading: false, attachments: action.payload}
        case 'GET_ATTACHMENTS_FAIL':
            return {...state, attachmentsLoading: false}

        case 'DOWNLOAD_ATTACHMENT_START':
            return {...state, attachment: null};
        case 'DOWNLOAD_ATTACHMENT_SUCCESS':
            return {...state, attachment: action.payload};

        case 'GET_ALL_RECIPIENTS_SUCCESS':
            return {...state, recipients: action.payload};

        case 'SEARCH_RECIPIENTS_START':
            return {...state, loading: true, searchedRecipients: null};
        case 'SEARCH_RECIPIENTS_SUCCESS':
            return {...state, loading: false, searchedRecipients: action.payload};
        case 'SEARCH_RECIPIENTS_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'CLEAN_MESSAGE':
            return {...state, message: null, attachments: null, attachment: null, video: null, messageSent: false};

        case 'ADD_RECIPIENT':
        case 'REMOVE_RECIPIENT':
            let message = state.message || {};
            let userId = action.payload.userId? action.payload.userId : action.payload.id;
            let recipientIds = (message.recipientIds || []).filter(a => a !== userId);
            let currentRecipients = message.recipients || [];
            let recipients = [];

            if(action.type === 'ADD_RECIPIENT') {
                recipientIds.push(userId);

                recipients = (currentRecipients||[]).filter(a => {
                    let id = a.userId? a.userId : a.id;
                    if (id !== userId) {
                        return(a);
                    }
                });
                recipients.push(action.payload)
            } else {
                recipients = (currentRecipients||[]).filter(a => {
                    return(a);
                });
            }

            return {...state, message: {...message, recipientIds, recipients}, searchedRecipients: null};

        case 'LOGOUT':
            return initialState;

        default:
            return state;
    }
}
