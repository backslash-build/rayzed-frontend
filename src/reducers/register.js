const initialState = {
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch(action.type) {

        case 'REGISTER_START':
            return {...state, loading: true, error: null};

        case 'LOGIN_SUCCESS':
            return {...state, loading: false, error: null};

        case 'REGISTER_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'REGISTER_CLEAR':
            return {...state, loading: false, error: null};

        case 'LOGOUT':
            return initialState;

        default:
            return state;
    }
}
