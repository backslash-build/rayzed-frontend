const initialState = {
    data: null,
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch(action.type) {
        case 'GET_ORGANISATIONS_START':
            return {...state, loading: true, error: null};
        case 'GET_ORGANISATIONS_SUCCESS':
            return {...state, loading: false, data: action.payload, error: null};
        case 'GET_ORGANISATIONS_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'CREATE_ORGANISATION_START':
            return {...state};
        case 'CREATE_ORGANISATION_SUCCESS':
            return {...state};
        case 'CREATE_ORGANISATION_FAIL':
            return {...state};

        case 'UPDATE_ORGANISATION_START':
            return {...state};
        case 'UPDATE_ORGANISATION_SUCCESS':
            return {...state};
        case 'UPDATE_ORGANISATION_FAIL':
            return {...state};

        case 'DELETE_ORGANISATION_START':
            return {...state};
        case 'DELETE_ORGANISATION_SUCCESS':
            return {...state};
        case 'DELETE_ORGANISATION_FAIL':
            return {...state};

        case 'LOGOUT':
            return initialState;

        default:
            return state;
    }
}