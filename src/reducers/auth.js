const initialState = null;

export default function (state = initialState, action) {
    switch(action.type) {
        case 'LOGIN_SUCCESS':
            return {...state, ...action.payload};

        case 'LOGOUT':
            return initialState;

        default:
            return state;
    }
}