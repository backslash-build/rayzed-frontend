const initialState = {
    data: {},
};

export default function (state = initialState, action) {
    switch(action.type) {
        case 'LINKS_START':
            return {...state, data: {}};
        case 'LINKS_SUCCESS':
            console.log("SUCCESS", action.payload);
            return {...state, data: action.payload};

        default:
            return state;
    }
}