const initialState = {
    data: null,
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch(action.type) {
        case 'GET_ALL_CHARITIES_START':
            return {...state, loading: true, error: null};
        case 'GET_ALL_CHARITIES_SUCCESS':
            return {...state, loading: false, data: action.payload, error: null};
        case 'GET_ALL_CHARITIES_FAIL':
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}