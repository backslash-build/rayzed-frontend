import { combineReducers } from 'redux';

import login from './login';
import register from './register';
import auth from './auth';
import links from './links';
import me from './me';
import allCharities from './allCharities';
import myCharities from './myCharities';
import messaging from './messaging';
import organisations from './organisations';

export default combineReducers({
    links,
    login,
    register,
    auth,
    me,
    allCharities,
    myCharities,
    messaging,
    organisations
});

