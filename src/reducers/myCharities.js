const initialState = {
    data: null,
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch(action.type) {
        case 'GET_MY_CHARITIES_START':
            return {...state, loading: true, error: null};
        case 'GET_MY_CHARITIES_SUCCESS':
            return {...state, loading: false, data: action.payload, error: null};
        case 'GET_MY_CHARITIES_FAIL':
            return {...state, loading: false, error: action.payload};

        case 'ATTACH_CHARITY_START':
            return {...state};
        case 'ATTACH_CHARITY_SUCCESS':
            return {...state};
        case 'ATTACH_CHARITY_FAIL':
            return {...state};

        case 'DETACH_CHARITY_START':
            return {...state};
        case 'DETACH_CHARITY_SUCCESS':
            return {...state};
        case 'DETACH_CHARITY_FAIL':
            return {...state};

        case 'LOGOUT':
            return initialState;

        default:
            return state;
    }
}