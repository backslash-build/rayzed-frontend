import { createStore as reduxCreateStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import saga from '../saga';
import rootReducer from '../reducers';

const sagaMiddleware = createSagaMiddleware()
const createStoreWithMiddleware = applyMiddleware(sagaMiddleware)(reduxCreateStore);
const store = createStoreWithMiddleware(rootReducer);
sagaMiddleware.run(saga);

export default store;