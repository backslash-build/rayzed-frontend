import RNFetchBlob from 'react-native-fetch-blob';

export function uploadFile(filepath, url, metadata, type, progressCallback) {

    filepath = decodeURI(filepath);

    console.log("UPLOADER", {filepath: filepath.replace(/^file:\/\//, ""), url});

    const headers = {
        'x-ms-blob-type': 'BlockBlob',
        'x-ms-version': '2009-09-19',
        'Content-Type': metadata.mimeType
    };

    return new Promise((resolve, reject) => {
        RNFetchBlob.fetch('PUT', url, headers, RNFetchBlob.wrap(filepath.replace(/^file:\/\//, "")))
            .uploadProgress((written, total) => {
                console.log("Upload progress", type, (written / total));
                progressCallback(type, written / total);
            })
            .then((res)=>{
                console.log("No ERROR", res);
                resolve()
            })
            .catch((err)=>{
                console.log("ERROR", err);
                reject()
            });
    });
}
