import { AsyncStorage } from 'react-native';

export async function saveAuthToken(token) {
    await AsyncStorage.setItem('token', JSON.stringify(token));
}
export async function getAuthToken() {
    return JSON.parse(await AsyncStorage.getItem('token'));
}
export async function hasAuthToken() {
    return !!await AsyncStorage.getItem('token');
}
export async function deleteAuthToken() {
    return await AsyncStorage.removeItem('token');
}