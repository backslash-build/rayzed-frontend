import SplashScene from '../containers/SplashScene';
import ViewLinkScene from '../containers/ViewLinkScene';
import LoginScene from '../containers/LoginScene';
import RegisterScene from '../containers/RegisterScene';
import AboutScene from '../containers/AboutScene';
import HomeScene from '../containers/HomeScene';
import EditProfileScene from '../containers/EditProfileScene';
import EditOrganisationScene from '../containers/EditOrganisationScene';
import ImportantMessageScene from '../containers/ImportantMessageScene';
import CharitiesScene from '../containers/CharitiesScene';
import EditCharitiesScene from '../containers/EditCharitiesScene';
import WatchVideosScene from '../containers/WatchVideosScene';
import FurtherInformationScene from '../containers/FurtherInformationScene';
import ViewAttachmentScene from '../containers/ViewAttachmentScene';
import SentMessagesScene from '../containers/SentMessagesScene';
import SentMessageSummaryScene from '../containers/SentMessageSummaryScene';
import NewMessageScene from '../containers/NewMessage/InitialScene';
import UpdateMessageScene from '../containers/NewMessage/UpdateMessageScene';
import FindRecipientScene from '../containers/FindRecipientScene';
import NewMessageContentScene from '../containers/NewMessage/ContentScene';
import NewMessageContactScene from '../containers/NewMessage/ContactScene';
import NewMessageSummaryScene from '../containers/NewMessage/SummaryScene';
import NewMessagePaymentScene from '../containers/NewMessage/PaymentScene';
import NewMessageRaisedScene from '../containers/NewMessage/RaisedScene';

import App from '../App.ios';

export function getScene(key, props) {
    switch(key) {

        case 'splash':
            return {
                title: 'Splash',
                component: SplashScene
            };

        case 'login':
            return {
                title: '',
                component: LoginScene
            };

        case 'register':
            return {
                title: 'Register',
                component: RegisterScene,
                navigationBarHidden: false
            };

		case 'about':
			return {
				title: 'About Raised',
				component: AboutScene,
                backButtonTitle: 'Back',
				navigationBarHidden: false
			}

        case 'home':
            return {
                title: '',
                component: HomeScene,
                leftButtonIcon: require('../../images/Raised-small-white-icon.png'),
                rightButtonIcon: require('../../images/ic_menu_black_24dp/android/drawable-hdpi/ic_menu_black_24dp.png'),
                onRightButtonPress: ()=> App.drawer.open(),
                backButtonTitle: 'Back'
            };

        case 'editProfile':
            return {
                title: 'Your Profile',
                component: EditProfileScene,
                backButtonTitle: 'Back'
            };

		case 'editOrganisation':
            return {
                title: 'Your Organisations',
                component: EditOrganisationScene,
                backButtonTitle: 'Back',
                leftButtonTitle: ' '
            };

        case 'importantMessage':
            return {
                title: 'Important Message',
                component: ImportantMessageScene
            };

        case 'charities':
            return {
                title: 'Charities you support',
                component: CharitiesScene,
                backButtonTitle: 'Back'
            };

        case 'editCharities':
            return {
                title: 'Charities you support',
                component: EditCharitiesScene,
                backButtonTitle: 'Back'
            };

        case 'watchVideos':
            return {
                title: 'Watch Videos',
                component: WatchVideosScene,
                backButtonTitle: 'Back',
                titleImage: require('../../images/Raised-small-white-icon.png'),
            };

        case 'furtherInformation':
            return {
                title: 'Further Information',
                component: FurtherInformationScene,
                backButtonTitle: 'Back',
                titleImage: require('../../images/Raised-small-white-icon.png'),
            };

        case 'viewAttachment':
            return {
                title: 'Attachment',
                component: ViewAttachmentScene,
                backButtonTitle: 'Back'
            };

        case 'sentMessages':
            return {
                title: 'Sent Messages',
                component: SentMessagesScene,
                backButtonTitle: 'Back'
            };

        case 'sentMessageSummary':
            return {
                title: 'Message Summary',
                component: SentMessageSummaryScene,
                backButtonTitle: 'Back'
            };

        case 'newMessage':
            return {
                title: 'New Message',
                component: NewMessageScene,
                backButtonTitle: 'Back'
            };

        case 'newMessageUpdate':
            return {
                title: 'Edit Message',
                component: UpdateMessageScene,
                backButtonTitle: 'Back'
            };

        case 'findRecipient':
            return {
                title: 'Find Target Recipient',
                component: FindRecipientScene,
                backButtonTitle: 'Back'
            };

        case 'newMessageContent':
            return {
                title: 'Message Content',
                component: NewMessageContentScene,
                backButtonTitle: 'Back'
            };

        case 'newMessageContact':
            return {
                title: 'Contact Details',
                component: NewMessageContactScene,
                backButtonTitle: 'Back'
            };

        case 'newMessageSummary':
            return {
                title: 'Summary',
                component: NewMessageSummaryScene,
                backButtonTitle: 'Back'
            };

        case 'newMessagePayment':
            return {
                title: 'Payment',
                component: NewMessagePaymentScene,
                backButtonTitle: 'Back',
            };

        case 'newMessageRaised':
            return {
                title: 'Message Sent',
                component: NewMessageRaisedScene,
                backButtonTitle: 'Home',
                leftButtonTitle: ' '
            };

		case 'test':
			return {
                title: 'Raised',
                component: NewMessageRaisedScene,
                backButtonTitle: 'Home',
            };

        case 'viewLink':
			return {
                title: 'View Link',
                component: ViewLinkScene,
                backButtonTitle: 'Back',
                passProps: { url: props.url }
			};

        default:
            throw new Error(`Invalid Navigation Key "${key}" supplied to getScene`);
    }
}