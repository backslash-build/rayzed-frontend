let timeouts = {};
let intervals = {};

let idcount = 0;

export default {

    setTimeout: (fn, t) => {

        let id = idcount++;

        timeouts[id] = window.setTimeout(()=>{
            fn();
            delete timeouts[id];
        }, t);

        return id;
    },

    clearTimeout: (id) => {
        if(timeouts[id]) {
            window.clearTimeout(timeouts[id]);
            delete timeouts[id];
        }
    },

    setInterval: (fn, t) => {

        let id = idcount++;

        intervals[id] = window.setInterval(()=>{
            fn();
        }, t);

        return id;
    },

    clearInterval: (id) => {
        if(intervals[id]) {
            window.clearInterval(intervals[id]);
            delete intervals[id];
        }
    },

    clearAll: () => {
        Object.keys(timeouts).map(i => {
            window.clearTimeout(timeouts[i]);
        });

        timeouts = {};

        Object.keys(intervals).map(i => {
            window.clearInterval(intervals[i]);
        });

        intervals = {};
    }

}