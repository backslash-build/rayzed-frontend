import RNFetchBlob from 'react-native-fetch-blob';

const DocumentDir = RNFetchBlob.fs.dirs.DocumentDir;

export function downloadPdf(uri_attachment, filename_attachment) {

    return new Promise((resolve, reject) => {
        RNFetchBlob.fetch('GET', uri_attachment)
        .then((res) => {

            let base64Str = res.data;
            let pdfLocation = DocumentDir + '/' + filename_attachment;
            RNFetchBlob.fs.writeFile(pdfLocation, base64Str, 'base64');
            console.log(pdfLocation);
            resolve(pdfLocation);
        }).catch((error) => {
            // error handling
            console.log("Error", error)
        });
    });
}
