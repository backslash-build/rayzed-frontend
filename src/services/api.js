import {customError} from '../utils/custom-error';
import store from '../store';

const api = 'https://api-raised.glass.lu/api/';
const auth = 'https://api-raised.glass.lu/';
const clientId = 'rayzedWebApi';
const clientSecret = 'AF9225D9-E1E6-4719-B5FE-2E30F2ABFDB5';

export const exceptions = {
    UnableToConnectException: customError('UnableToConnectException', 'Unable to connect to the server.'),
    AuthenticationException: customError('AuthenticationException', 'Authentication problem.'),
    AuthorizationException: customError('AuthorizationException', 'User is not authorized for this action.'),
    BadRequestException: customError('BadRequestException')
};

/* Auth
----------------------------------------------------------------------------------------------------------------------*/
export async function login(credentials) {
    const body = [
        'grant_type=password',
        `username=${encodeURIComponent(credentials.username)}`,
        `password=${credentials.password}`,
        `client_id=${clientId}`,
        `client_secret=${clientSecret}`,
        'scope=openid+roles+offline_access'
    ].join('&');

    const res = await fetch(`${auth}/connect/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: body
    });

    if (res.status !== 200) {
        throw new exceptions.AuthenticationException('Invalid username or password');
    }

    return await res.json();
}

export async function refreshToken(token) {
    const body = [
        'grant_type=refresh_token',
        `refresh_token=${token}`,
        `client_id=${clientId}`,
        `client_secret=${clientSecret}`,
        'scope=openid+roles+offline_access'
    ].join('&');

    const res = await fetch(`${auth}/connect/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: body
    });

    console.log("Refresh token response", res);

    if (res.status === 500) {
        throw new exceptions.AuthenticationException('Unable to connect to the server.');
    } else if (res.status !== 200) {
        throw new exceptions.AuthenticationException('Refresh token is expired');
    }

    return await res.json();
}


/* Api helper functions
----------------------------------------------------------------------------------------------------------------------*/
function getToken() {
    const a = store.getState().auth;
    return a && a.access_token;
}

async function getJson(path) {
    try {
        const res = await fetch(`${api}/${path}`, { headers: { Authorization: `Bearer ${getToken()}`} });
        console.log(res);
        if(res.status === 500) {
            throw new exceptions.UnableToConnectException('Unable to connect to the server.');
        }
        return res;
    } catch(e) {
        throw new exceptions.UnableToConnectException('Unable to connect to the server.');
    }
}

async function postJson(path, body) {
    try {
        const res = await fetch(`${api}/${path}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${getToken()}`,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        });
        if(res.status === 500) {
            throw new exceptions.UnableToConnectException('Unable to connect to the server.');
        }
        return res;
    } catch(e) {
        throw new exceptions.UnableToConnectException('Unable to connect to the server.');
    }
}

async function del(path) {
    const res = await fetch(`${api}/${path}`, {
        method: 'DELETE',
        headers: { Authorization: `Bearer ${getToken()}`}
    });
    if(res.status === 500) {
        throw new exceptions.UnableToConnectException('Unable to connect to the server.');
    }
    return res;
}

/* Ping
----------------------------------------------------------------------------------------------------------------------*/
export async function ping() {
    try {

        const res = await getJson('/test/ping');

        if (res.status === 200) {
            return true;
        } else {
            return false;
        }
    } catch(e) {
        return false;
    }
}

/* User
----------------------------------------------------------------------------------------------------------------------*/
// TODO
// getUserTAndC
// acceptTAndC
// enableRecipient
// disableRecipient

export async function getLinks(callback) {
    const res = await getJson('/content/links');
    const json = await res.json();
    return json;
}

export async function getUser() {
    const res = await getJson('/user/');
    if(res.status === 401) {
        throw new exceptions.AuthenticationException('Not logged in.');
    }
    return await res.json();
}

export async function createUser(user) {
    const res = await postJson('/user/create', user);
    if (res.status !== 200) {
        const json = await res.json();
        throw new exceptions.BadRequestException(
            json.map(error => error.error + ':' + error.errorDescription).join(',')
        );
    }
}

export async function updateUser(user) {
	console.log("Updating user");
    const res = await postJson('/user/update', user);
    console.log(res);
    if (res.status !== 200) {
        const json = await res.json();
		console.log("Updating user error", json);
        throw new exceptions.BadRequestException(
            json.map(error => error.error + ':' + error.errorDescription).join(',')
        );
    }
    return await res.json();
}

export async function updatePassword(password) {
	console.log("Updating password");
    const res = await postJson('/user/password/change', password);
	console.log(res);
    if (res.status !== 200) {
        const json = await res.json();
		console.log("Updating password error", json);
        throw new exceptions.BadRequestException(
            json.map(error => error.error + ':' + error.errorDescription).join(',')
        );
    }
    return await res.json();
}

export async function becomeRecipient() {
    const res = await getJson('/user/becomeRecipient');
    console.log("Becoming recipient", res);
    let json = await res.json();
    console.log(json);
    return json;
}

/* Charities
----------------------------------------------------------------------------------------------------------------------*/
export async function getAllCharities() {
    const res = await getJson('/charities');
    return (await res.json()).charities;
}

export async function getUserCharities() {
    const res = await getJson('/user/charities');
    return (await res.json()).charities;
}

export async function requestCharity(charity) {
    const res = await postJson('/charities/request', charity);
    return (await res.json());
}

export async function attachCharityToUser(id) {
    const res = await postJson(`/user/charities/${id}/attach`);
    const json = await res.json();
    console.log("ERRORS?", json);
    return (json).charities;
}

export async function detachCharityFromUser(id) {
    const res = await postJson(`/user/charities/${id}/detach`);
    return (await res.json()).charities;
}


/* Received Messages
----------------------------------------------------------------------------------------------------------------------*/
export async function getReceivedPendingMessages() {
    const res = await getJson('/user/messages/received/pending');
	console.log("Received Pending Res", res);
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a receiver');
    }
	let messages = (await res.json()).messages;
	console.log(messages);
    return messages;
}

export async function getReceivedViewedMessages() {
    const res = await getJson('/user/messages/received/viewed');
	console.log("Received Pending Viewed", res);
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a receiver');
    }
    return (await res.json()).messages;
}

export async function getReceivedLikedMessages(page) {
	console.log("PAGE NUMBER", page);
    const res = await getJson(`/user/messages/received/liked?page=${page}`);
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a receiver');
    }
    let messages = (await res.json()).messages;
    console.log("MESSAGES", messages);
    return messages;
}


/* Sending Messages
----------------------------------------------------------------------------------------------------------------------*/
export async function getAllRecipients() {
    const res = await getJson(`/users/recipients`);
    return (await res.json()).recipients;
}

export async function searchRecipients({ name, position, organisation } = {}) {
    const query = [ `name=${name}`, `position=${position}`, `organisation=${organisation}` ].join('&');
    const res = await getJson(`/users/recipients?${query}`);
    const result = (await res.json());
    console.log(result);
    return result;
}


export async function getSentMessages(page) {

    const res = await getJson(`/user/messages/sending/sent?page=${page}`);

    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getDraftMessages(page) {

    const res = await getJson(`/user/messages/sending/drafts?page=${page}`);

    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getPendingMessages(page) {

    const res = await getJson(`/user/messages/sending/pending?page=${page}`);

    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getSendingDraftMessages() {
    const res = await getJson('/user/messages/sending/drafts');
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getSendingExpiredMessages() {
    const res = await getJson('/user/messages/sending/expired');
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getSendingSentMessages(page=1) {
    const res = await getJson(`/user/messages/sending/sent?page=${page}`);
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getSendingViewedMessages() {
    const res = await getJson('/user/messages/sending/viewed');
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getSendingLikedMessages() {
    const res = await getJson('/user/messages/sending/liked');
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

export async function getSendingDislikedMessages() {
    const res = await getJson('/user/messages/sending/disliked');
    if(res.status === 401) {
        throw new exceptions.BadRequestException('User is not a sender');
    }
    return (await res.json()).messages;
}

/* Notifications
----------------------------------------------------------------------------------------------------------------------*/
export async function getNotifications() {
    const res = await getJson('/user/notifications');
    return (await res.json()).notifications;
}

/* Organisations
----------------------------------------------------------------------------------------------------------------------*/
export async function getOrganisations() {
    const res = await getJson('/user/organisations');
    return (await res.json()).organisations;
}

export async function createOrganisation(organisation) {
    console.log("CREATING ORGANISATION", organisation);
    const res = await postJson('/user/organisations/create', organisation);
    console.log("CREATING ORGANISATION", res);
    return await res.json();
}

export async function getOrganisation(id) {
    const res = await getJson(`/user/organisations/${id}`);
    return await res.json();
}

export async function updateOrganisation(payload) {
    console.log("ORGANISATION", payload.organisation);
    const res = await postJson(`/user/organisations/${payload.id}/update`, payload.organisation);
    return await res.json();
}

export async function deleteOrganisation(id) {
    console.log("DELETING ORGANISATIONS");
    const res = await del(`/user/organisations/${id}/delete`);
}


/* Single Message
----------------------------------------------------------------------------------------------------------------------*/
export async function getMessage(id) {
    const res = await getJson(`/messages/${id}`);
    return await res.json();
}

export async function getMessagePaymentDetails(id) {
    console.log("wejfnwejfnwe", id);
    const res = await getJson(`/messages/${id}/payment/details`);
    console.log(res);
    const json = await res.json();
    console.log(json);
    return json;
}

export async function createMessage(message) {
    const res = await postJson('/messages/create', message);
    return await res.json();
}

export async function updateMessage(message) {
    console.log("UPDATING MESSAGE 2", message.id, message);
    const res = await postJson(`/messages/${message.id}/update`, message);
    console.log("REIJFEA", res);
    const json = await res.json();
    console.log("fbmcvbmcvb", json);
    return json;
}

/* Single Message Video
----------------------------------------------------------------------------------------------------------------------*/
export async function createMessageVideoUploadUrl(message) {
    console.log("ATTEMPTING TO UPLOAD VIDEO")
    const res = await postJson(`/messages/${message.id}/upload`, message.metadata);
    console.log("ATTEMPTING TO UPLOAD VIDEO", res);
    return await res.json();
}

export async function createMessageVideoDownloadUrl(id) {
    const res = await getJson(`/messages/${id}/download`);
    console.log(res);
    const json = await res.json();
    return json;
}

/* Multiple Message Attachments
----------------------------------------------------------------------------------------------------------------------*/
export async function getMessageAttachments(id) {
    console.log("GETTING ATTACHMENTS", id);
    const res = await getJson(`/messages/${id}/attachments`);
    const json = await res.json();
    console.log(res);
    console.log(json);
    return json.attachments;
}

/* Single Message Attachments
----------------------------------------------------------------------------------------------------------------------*/
export async function createMessageAttachmentUploadUrl(message) {
    console.log("ATTEMPTING TO UPLOAD FILE", JSON.stringify(message.metadata));
    const res = await postJson(`/messages/${message.id}/attachments/upload`, message.metadata);
    const json = await res.json();
    console.log("ATTEMPTING TO UPLOAD FILE", json);
    return json;
}

export async function createMessageAttachmentDownloadUrl(message) {
    const res = await getJson(`/messages/${message.id}/attachments/${message.attachmentId}/download`);
    const json = await res.json();
    return json;
}

export async function deleteMessageAttachment(message) {
    const res = await del(`/messages/${message.id}/attachments/${message.attachmentId}/delete`);
}

/* Single Message Actions
----------------------------------------------------------------------------------------------------------------------*/
export async function likeMessage(message) {
    const res = await postJson(`/messages/${message.id}/liked`, message.data); // data is just {  "comments": "string" }
}

export async function dislikeMessage(message) {
    const res = await postJson(`/messages/${message.id}/disliked`, message.data); // data is just {  "comments": "string" }
    return await res.json();
}

export async function reportMessage(id) {
    const res = await getJson(`/messages/${id}/report`);
    return await res.json();
}

export async function sendMessage(payload) {
    console.log(payload);
    const res = await getJson(`/messages/${payload.id}/send?returnUrl=`+payload.returnUrl);
    console.log(`/messages/${payload.id}/send?returnUrl=`+payload.returnUrl);
    //const res = await getJson(`/messages/${payload.id}/send`);
    return await res.json();
}

export async function deleteMessage(id) {
    const res = await getJson(`/messages/${id}/delete`);
    return await res.json();
}
