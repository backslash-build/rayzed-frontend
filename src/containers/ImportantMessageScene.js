import React, { Component } from 'react';
import {
    View,
    Text,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Button,
    TextInput,
    Dimensions,
    ScrollView
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { getScene } from '../services/navigation';
import * as actions from '../actions';

import VideoPlayer from '../components/VideoPlayer';

import SplashScene from './SplashScene';

import App from '../App';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
        ...Platform.select({
            ios: {
                marginLeft: 15,
                marginRight: 15,
                width: Dimensions.get('window').width - 30,
            },
            android: {
                paddingLeft: 15,
                paddingRight: 15,
                width: Dimensions.get('window').width
            }
        })
    },
    cancelMessageButton: {
        paddingTop: 5,
		paddingBottom: 5,
		borderRadius: 5,
		overflow: 'hidden',
		alignSelf: 'stretch',
		marginBottom: 0,
		...Platform.select({
			ios: {
				backgroundColor: '#61b746'
			}
		})
    },
});


@connect(state => ({ ...state, links: state.links.data, connection: state.login.connection}), dispatch => bindActionCreators(actions, dispatch))
@autobind
export default class SentMessageSummaryScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        logout: React.PropTypes.func,
		me: React.PropTypes.object,
		oranisations: React.PropTypes.object,
		getOrganisations: React.PropTypes.func,
		myCharities: React.PropTypes.object,
        getMyCharities: React.PropTypes.func,
        updateMe: React.PropTypes.func,
        signupToReceiveMessages: React.PropTypes.func
    }

    state = {
		showDelegateButton: false
	}

    componentDidMount() {

		this.props.getOrganisations();

        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

	scrollViewStyle() {
        let style = {
			backgroundColor: '#fff',
			height: Dimensions.get('window').height,
			...Platform.select({
				android: {
                    marginBottom: 96
				},
				ios: {
					marginBottom: 96,
				}
			})
		};

        return style;
    }

	setWatchedImportantVideo() {

        if (this.state.showDelegateButton) {
            let payload = {
                introVideoViewed: true
            }

            this.props.updateMe(payload);
            this.props.signupToReceiveMessages();

            this.props.navigator.popToTop();
        }
	}

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    render() {

        let selectedVideoUrl = null;
        if (this.state.selectedVideo) selectedVideoUrl = this.state.selectedVideo.uri;


		let preCharityText = "charity '";
		if (this.props.myCharities.data.length > 1) {
			preCharityText = "charities '";
		}

        let charityText = "";
		this.props.myCharities.data.map(function (charity) {
			charityText += charity.name + ", ";
		});

		charityText = charityText.substring(0, charityText.length - 2);
        charityText += "'";

        let video = null;
        if (Platform.OS === 'android') {
            video = { uri: 'video' };
        } else if (Platform.OS == 'ios') {
            video = require('../../video.mp4');
        }

        return (
            <View style={styles.app}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Important Message'
                    titleColor='#fff'
                />}

				<KeyboardAwareScrollView
					automaticallyAdjustContentInsets={false}
					style={this.scrollViewStyle()}
					keyboardShouldPersistTaps="handled">

					<View style={{ flex: 1 }}>
						<View style={{ marginBottom: 30 }}>
                        	{true && <VideoPlayer
								showSlider={false}
								videoUrl={video}
								onVideoEnd={()=> this.setState({ showDelegateButton: true })}/>}
                        	{false && <VideoPlayer
								showSlider={false}
								videoUrl={this.props.links.videoRecipient}
								onVideoEnd={()=> this.setState({ showDelegateButton: true })}/>}
                    	</View>

						{this.props.me.data && this.props.organisations && this.props.organisations.data && <View style={{ marginBottom: 30, paddingHorizontal: 15 }}>
							<Text style={{ textAlign: 'center' }}>Companies or individuals wanting to send you a targetted message will be paying <Text style={{ fontWeight: 'bold' }}>£{this.props.me.data.donation}</Text> to your chosen {preCharityText}<Text style={{ fontWeight: 'bold' }}>{charityText}</Text>.{"\n"}{"\n"}These companies or individuals have a right to expect that <Text style={{ fontWeight: 'bold' }}>'{this.props.me.data.firstName + " " + this.props.me.data.lastName + ", " + this.props.organisations.data[0].position + ", " + this.props.organisations.data[0].name}'</Text> will personally watch these messages.{"\n"}{"\n"}Asking a third party such as a colleague, relative or friend to watch the messages on your behalf is strictly forbidden and constitutes fraud against the party who has posted the message.{"\n"}{"\n"}Please be patient and keep this app on your phone while we build momentum for this service. It will be worth the wait!​</Text>
						</View>}
					</View>
                </KeyboardAwareScrollView>

				<View style={styles.bottomButtonWrapper}>
                    <View style={styles.cancelMessageButton}>
                        <Button
                            onPress={this.setWatchedImportantVideo}
                            title={this.state.showDelegateButton? 'I will not delegate video watching' : 'Watch the message then click'}
                            color={(Platform.OS === 'ios') ? ((!this.state.showDelegateButton) ? '#ddd' : '#fff') : '#61b746'}
                        />
                    </View>
                </View>

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
