import React, { Component } from 'react';
import {
    View,
    Button,
    Text,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../services/navigation';
import * as actions from '../actions';

class MenuLink extends Component {

    static propTypes = {
        title: React.PropTypes.string,
        style: React.PropTypes.object
    }

    render() {
        return (
            <TouchableHighlight {...this.props}
                underlayColor={'#FFF'}
                style={{
                padding: 20,
                borderBottomColor: '#F0F0F0',
                borderBottomWidth: 1,
                ...this.props.style
            }}>
                <Text>{this.props.title}</Text>
            </TouchableHighlight>
        );
    }

}

@connect(state => state, dispatch => bindActionCreators(actions, dispatch))
@autobind
export default class Menu extends Component {

    static propTypes = {
        navigator: React.PropTypes.func,
        drawer: React.PropTypes.func,
        me: React.PropTypes.object,
        logout: React.PropTypes.func
    }

	renderOrganisationInfo() {
		let organisations = this.props.organisations.data.map(function(organisation) {
			return organisation.name + ", " + organisation.position;
		})
		return organisations.join('; ');
	}

    render() {


        return (
            <View>
                <ScrollView>
                    <View style={{ backgroundColor: '#61b746', height: 120, flexDirection: 'column', alignItems: 'center', paddingTop: 20, overflow: 'visible' }}>
                        <Image resizeMode='contain' style={{ width: 180, maxHeight: 100, marginTop: 20 }} source={require('../../images/Raised-white.png')} />
                    </View>

                    <View style={{
                        borderBottomColor: '#F0F0F0',
                        borderBottomWidth: 1,
                        paddingVertical: 20,
                        paddingHorizontal: 20,
                        backgroundColor: '#61b746'}}>
                        {this.props.me.data && (
                            <View>
                                <Text style={{ color: '#fff', fontSize: 17, fontWeight: 'bold' }}>{this.props.me.data.firstName} {this.props.me.data.lastName}</Text>
                                <Text style={{ fontSize: 13, color: '#fff' }}>
                                    {this.props.me.data.email}
                                </Text>
                                {this.props.organisations && this.props.organisations.data && <Text style={{ fontSize: 13, color: '#fff' }}>
                                    {this.renderOrganisationInfo()}
                                </Text>}
                            </View>
                        )}
                    </View>

                    <MenuLink title='About Us' onPress={() => {
                        this.props.navigator().push(getScene('about'));
                        this.props.closeDrawer();
                    }}/>
                    <MenuLink title='Edit Profile' onPress={() => {
                        this.props.navigator().push(getScene('editProfile'));
                        this.props.closeDrawer();
                    }}/>
                    <MenuLink title='Edit Organisation' onPress={() => {
                        this.props.navigator().push(getScene('editOrganisation'));
                        this.props.closeDrawer();
                    }}/>
                    {this.props.me && this.props.me.data && this.props.me.data.recipient && this.props.me.data.recipient.status == 'Approved' && <MenuLink title='Edit Charities and fee' onPress={() => {
                        this.props.navigator().push(getScene('charities'));
                        this.props.closeDrawer();
                    }}/>}
                    <MenuLink title='Send a Raised Message' onPress={() => {
                        this.props.navigator().push(getScene('newMessage'));
                        this.props.closeDrawer();
                    }}/>
                    <MenuLink title='Log out' onPress={()=> this.props.logout()}/>
                </ScrollView>
            </View>
        );
    }
}