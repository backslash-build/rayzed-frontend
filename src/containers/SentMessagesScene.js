import React, { Component } from 'react';
import {
    View,
    ToolbarAndroid,
    StyleSheet,
    Platform,
    BackAndroid,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../services/navigation';
import * as actions from '../actions';

import SentMessagesContent from '../components/SentMessagesContent';

import SplashScene from './SplashScene';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({ ...state, connection: state.login.connection }), dispatch => bindActionCreators(actions, dispatch))
@autobind
export default class SentMessagesScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        logout: React.PropTypes.func
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    openMessage() {
        this.props.navigator.push(getScene('sentMessageSummary'));
    }

    newMessage() {
        this.props.navigator.push(getScene('newMessage'));
    }

    render() {

        return (
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Raised'
                    titleColor='#fff'
                />}
                <SentMessagesContent onNewMessage={this.newMessage} onMessagePress={this.openMessage} />

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
