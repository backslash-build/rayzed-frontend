import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SplashScene from './SplashScene';

import { getScene } from '../services/navigation';
import * as actions from '../actions';
import * as messagingActions from '../actions/messaging';

import App from '../App';
import EditOrganisationForm from '../components/EditOrganisationForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});


//@connect(
//    ({ organisations }) => ({ organisations }),
//    dispatch => bindActionCreators(actions, dispatch)
//)

@connect(state => ({...state.messaging, me: state.me, organisations: state.organisations, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class EditOrganisationScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        logout: React.PropTypes.func,
        organisations: React.PropTypes.object,
        me: React.PropTypes.object,
        getOrganisations: React.PropTypes.func,
        createOrganisation: React.PropTypes.func,
        updateOrganisation: React.PropTypes.func,
        deleteOrganisation: React.PropTypes.func
    }

    state = {}

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
        this.props.getOrganisations();
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popToTop();
        return true;
    }

	signupToReceiveMessages(payload) {

		let that = this;

        payload.map(function(organisation, index) {

            that.props.createOrganisation(organisation);
        });

        this.props.signupToReceiveMessages();

		this.props.navigator.push(getScene('charities'));
	}

    submit(payload) {

        let that = this;

        payload.map(function(organisation, index) {

            that.props.createOrganisation(organisation);
        });

        this.back();
    }

    render() {

        return (
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Edit Organisation'
                    titleColor='#fff'
                />}
                <EditOrganisationForm
                    onUpdateOrganisation={this.props.updateOrganisation}
                    onDeleteOrganisation={this.props.deleteOrganisation}
                    organisations={this.props.organisations}
					signupToReceiveMessages={this.signupToReceiveMessages}
					me={this.props.me}
                    onSubmit={this.submit} />

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
