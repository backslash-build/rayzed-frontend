import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    Text,
    Button,
    View
} from 'react-native';

import * as actions from '../actions';

@connect(state => ({...state.login, me: state.me}), dispatch => bindActionCreators(actions, dispatch))
export default class Login extends Component {
    static propTypes = {
        register: React.PropTypes.func,
        login: React.PropTypes.func,
        isLoggingIn: React.PropTypes.bool,
        error: React.PropTypes.string,
        token: React.PropTypes.string,
        getMe: React.PropTypes.func,
        me: React.PropTypes.object
    }

    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>
                    Test page
                </Text>

                <Button title='Valid login' onPress={()=>this.props.login({ username: 'admin', password: 'Gr*atP%40%24%24w0rd'})} />

                <Text>---</Text>

                <Button title='Invalid login' onPress={()=>this.props.login({ username: 'test', password: 'test'})} />

                <Text>---</Text>

                <Button disabled={this.props.me.loading} title='Get Me' onPress={()=>this.props.getMe()} />

                <Text>---</Text>

                <Text>
                    IsLoggingIn: {this.props.isLoggingIn ? 'yes' : 'no'}; {this.props.error}
                </Text>

                <Text>
                    Token: {this.props.token && this.props.token.slice(0, 9)}
                </Text>

                <Text>---</Text>

                <Text>
                    MeJson: {JSON.stringify(this.props.me)}
                </Text>

            </View>
        );
    }
}