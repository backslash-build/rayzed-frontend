import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    View,
    BackAndroid,
	Image,
	Text,
	StyleSheet,
    Linking,
    TouchableWithoutFeedback,
    Platform,
    Dimensions,
    ToolbarAndroid
} from 'react-native';

import * as actions from '../actions';
import { getScene} from '../services/navigation';
import RegisterForm from '../components/RegisterForm';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import SplashScene from './SplashScene';

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
	supportersText: {
		textAlign: 'center',
		marginVertical: 20,
		fontWeight: 'bold'
	},
	supporterImage: {
        maxWidth: 150,
        maxHeight: 150,
		margin: 5
	},
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    scrollView: {
        backgroundColor: '#fff',
        height: Dimensions.get('window').height,
        paddingHorizontal: 15,
        paddingTop: 15
    }
});

@connect(state => ({ ...state, links: state.links.data, connection: state.login.connection }), dispatch => bindActionCreators(actions, dispatch))
@autobind
export default class AboutScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    openUrl(url) {

        let urlLink = url;

        if (url.indexOf("https://") != -1) {
            this.props.navigator.push(getScene('viewLink', { url: url }));
        } else {
            if (url.indexOf("http://") === -1) {
                urlLink = "http://" + urlLink;
            }
            Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
        }
    }

    render() {

        return (
            <View style={styles.app}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    titleColor='#fff'
                    title='About Raised'
                />}
                <KeyboardAwareScrollView
                    automaticallyAdjustContentInsets={false}
                    style={styles.scrollView}>
                    <Text>
                        Raised is a not-for-profit Community Interest Company* whose mission is to make it possible for anyone to send a message, and hopefully open a dialogue, with the world’s leading decision-makers – people who are very hard-to-reach because they are very busy.
                        {"\n\n"}In return for agreeing to watch these messages, the decision-makers ask for a donation to a charity of their choosing. 100% of these fees goes to charity. We fund the service by making a small handling charge on top of the viewing fee. Any surplus from this fee also goes to charity. We will never share your contact details nor abuse your trust. <TouchableWithoutFeedback onPress={()=> this.openUrl(this.props.links.aboutUs)}><Text style={{ textDecorationLine: 'underline' }}>Click to read who we are.</Text></TouchableWithoutFeedback>
                        {"\n\n"}*Uk company 10743421
                    </Text>
                    <Text style={styles.supportersText}>With thanks to our supporters!</Text>

                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <TouchableWithoutFeedback onPress={()=> this.openUrl('https://backslash.build')}>
                            <Image resizeMode="contain" style={styles.supporterImage} source={require('../../images/supporters/backslashbuild.png')} />
                        </TouchableWithoutFeedback>
                        <Text style={{ width: 150, textAlign: 'center' }}>Backslash Build – Web and app development</Text>
                        <TouchableWithoutFeedback onPress={()=> this.openUrl('https://www.thespecialistworks.com/')}>
                            <Image resizeMode="contain" style={styles.supporterImage} source={require('../../images/supporters/thespecialistworks.jpg')} />
                        </TouchableWithoutFeedback>
                        <Text style={{ width: 150, textAlign: 'center' }}>The Specialist Works – Performance Media Agency</Text>
                        <TouchableWithoutFeedback onPress={()=> this.openUrl('http://www.hanselhenson.co.uk/')}>
                            <Image resizeMode="contain" style={styles.supporterImage} source={require('../../images/supporters/hanselhenson.png')} />
                        </TouchableWithoutFeedback>
                        <Text style={{ width: 150, textAlign: 'center' }}>Hansel Henson – Intellectual Property Law Firm</Text>
                    </View>
                </KeyboardAwareScrollView>

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}