import React, {Component} from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import {
    Platform,
    Button,
    View,
    Text,
    StyleSheet,
    ToolbarAndroid,
    ActivityIndicator,
    TouchableHighlight,
    ScrollView,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    Linking,
    TouchableWithoutFeedback,
    AsyncStorage,
    TextInput
} from 'react-native';
import Hr from 'react-native-hr';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import LoadingScene from './LoadingScene';
import SplashScene from './SplashScene';

import { getScene } from '../services/navigation';
import { refreshToken } from '../services/api';
import * as actions from '../actions';
import timeouts from '../services/timeouts';
import * as messagingActions from '../actions/messaging';
import App from '../App';

import DebouncedPressable from '../components/DebouncedPressable';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    watchVideosButton: {
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 5,
        overflow: 'hidden',
        alignSelf: 'stretch',
        marginBottom: 20,
        ...Platform.select({
            ios: {
                backgroundColor: '#dd0000'
            }
        })
    },
    firstMessageButton: {
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 5,
        overflow: 'hidden',
        alignSelf: 'stretch',
        marginTop: 20,
        marginBottom: 20,
        marginHorizontal: 15,
        ...Platform.select({
            ios: {
                backgroundColor: '#FF0000'
            }
        })
    },
    noVideosButton: {
        borderRadius: 5,
        overflow: 'hidden',
        borderColor: '#61b746',
        borderWidth: 1,
        alignSelf: 'stretch',
        marginBottom: 20,
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            }
        })
    },
    segmentHeading: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#61b746',
        minHeight: 30,
        borderColor: '#b3b3b3',
        borderBottomWidth: 1
    },
    newButton: {
        overflow: 'hidden',
        borderRadius: 5,
        flex: 1,
        width: 40,
        margin: 10,
        ...Platform.select({
            ios: {
                backgroundColor: '#4477dd'
            }
        })
    },
    receivingMessages: {
        fontWeight: 'bold',
        color: '#fff',
        flex: 3,
        fontSize: 16,
        textAlign: 'left',
        margin: 4,
        padding: 10,
        textAlignVertical: 'center',
    },
    sendingMessages: {
        fontWeight: 'bold',
        color: '#fff',
        flex: 3,
        fontSize: 16,
        textAlign: 'left',
        margin: 4,
        padding: 10,
        textAlignVertical: 'center',
    },
    sentMessage: {
        paddingVertical: 10,
        position: 'relative',
        backgroundColor: '#F0F0F0',
        borderColor: '#b3b3b3',
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
    },
    downloadMessages: {
        paddingVertical: 20,
        position: 'relative',
        backgroundColor: '#F0F0F0',
        borderColor: '#b3b3b3',
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
    },
    messageWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    messageName: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 5
    },
    messageIcon: {
        marginRight: 5,
        height: 20,
        width: 20
    },
    messageHeading: {
        fontWeight: 'bold',
        fontSize: 13,
		width: Dimensions.get('window').width - 85
    },
    messageSubheading: {
        fontSize: 10
    },
    arrowWrapper: {
        height: 30,
        width: 30,
        borderRadius: 15,
        overflow: 'hidden',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 10,
        right: 10
    },
    arrowText: {
        height: 20,
        width: 20
    },
    detailWrapper: {
        marginTop: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    messageDetail: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    detailHeading: {
        width: 80,
        fontSize: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        marginBottom: 5
    },
    detailContent: {
        fontWeight: 'bold',
		maxWidth: Dimensions.get('window').width - 200,
    },
    detailLink: {
        maxWidth: Dimensions.get('window').width - 250,
        color: '#539fe0',
        textDecorationLine: 'underline',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 11
    },
    detailValue: {
        maxWidth: Dimensions.get('window').width - 250,
        color: '#539fe0',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15
    },
    detailValueLink: {
        maxWidth: Dimensions.get('window').width - 250,
        color: '#539fe0',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15,
        textDecorationLine: 'underline'
    },
    bottomButtonWrapper: {
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        paddingTop: 15,
        paddingBottom: 15,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#efefef',
        borderTopWidth: 1,
        ...Platform.select({
            ios: {
                marginLeft: 15,
                marginRight: 15,
                width: Dimensions.get('window').width - 30,
            },
            android: {
                paddingLeft: 15,
                paddingRight: 15,
                width: Dimensions.get('window').width
            }
        })
    },
    reachMeRaised: {
        flexDirection: 'column',
        marginTop: 10,
        marginBottom: 20,
        alignItems: 'center',
		marginHorizontal: 15
    },
    reachMeRaisedIcon: {
        height: 30,
        marginBottom: 10
    },
    reachMeRaisedLink: {
        width: Dimensions.get('window').width - 85,
        textAlign: 'center',
        color: '#808080',
        textDecorationLine: 'underline',
    },
    increaseValue: {
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: 'center',
		marginHorizontal: 15
    },
    increaseValueIcon: {
        width: 30,
        height: 30,
        marginRight: 15
    },
    increaseValueMessage: {
        width: Dimensions.get('window').width - 85,
    },
    increaseValueLink: {
        width: Dimensions.get('window').width - 85,
        color: '#808080',
        textDecorationLine: 'underline',
    },
    verifyEmailError: {
        height: 45,
        marginTop: 7,
        marginBottom: 15,
        borderColor: '#d0d0d0',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 7,
        width: Dimensions.get('window').width - 145,
        marginRight: 5
    },
    refreshEmail: {
        flexDirection: 'row',
        ...Platform.select({
            android: {
                marginBottom: 30
            }
        })
    },
    refreshEmailButton: {
        width: 100,
        height: 45,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 5,
        marginTop: 7,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        ...Platform.select({
            ios: {
                backgroundColor: '#61b746'
            },
            android : {
                marginBottom: 30
            }
        })
    },
    refreshEmailText: {
        color: '#539fe0',
        textDecorationLine: 'underline',
        textAlign: 'center',
        fontSize: 16,
        marginTop: 12
    }
});

@connect(state => ({ ...state, links: state.links.data, connection: state.login.connection }), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class HomeScene extends Component {

	sentPage = 0;
	draftPage = 0;
	likedPage = 0;

    static propTypes = {
        navigator: React.PropTypes.any,
        testConnection: React.PropTypes.func,
        logout: React.PropTypes.func,
        getMe: React.PropTypes.func,
        getSentMessages: React.PropTypes.func,
        getPendingMessages: React.PropTypes.func,
        getDraftMessages: React.PropTypes.func,
        getReceivingMessages: React.PropTypes.func,
        getLikedMessages: React.PropTypes.func,
        getMessage: React.PropTypes.func,
        signupToReceiveMessages: React.PropTypes.func,
        me: React.PropTypes.object,
        myCharities: React.PropTypes.object,
        getMyCharities: React.PropTypes.func,
        getOrganisation: React.PropTypes.func
    }

    componentDidMount() {

		let that = this;

        this.props.getMe({}, async () => {
			this.getSentMessages();
            this.getDraftMessages();
            this.getPendingMessages();

            this.setState({
                refreshEmail: this.props.me.data? this.props.me.data.email : ""
            });

            if (this.props.me.data && this.props.me.data.recipient && this.props.me.data.recipient.status === 'Approved') {
				this.props.getMyCharities();
                this.getLikedMessages();

				this.props.getReceivingMessages(function (resultsCount) {
					that.setState({
						lastPendingMessagesCount: resultsCount
					});
                });

                if (this.state && !this.state.notFirstVisit) {
                    try {
                        const value = await AsyncStorage.getItem('@RaisedFirstVisit');
                        if (value !== null){
                            this.setState({
                                notFirstVisit: true
                            });
                        } else {
                            try {
                                await AsyncStorage.setItem('@RaisedFirstVisit', 'false');
                            } catch (error) {
                                // Error saving data
                            }
                        }
                    } catch (error) {
                        console.log(error);
                    }
                }
            }
        });

        this.props.getOrganisations({}, (error) => {
            if (error) {
                this.setState({ networkError: error });
            } else if (this.props.organisations && this.props.organisations.data && this.props.organisations.data.length == 0) {
                this.props.navigator.push(getScene('editOrganisation'));
            }
        });

        this.startGetMeInterval();
    }

	componentWillUnmount() {
		this.clearAllIntervals();
    }

    openUrl(url) {

        let urlLink = url;

        Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));

        return true;

        if (url.indexOf("https://") != -1) {
            this.props.navigator.push(getScene('viewLink', { url: url }));
        } else {
            if (url.indexOf("http://") === -1) {
                urlLink = "http://" + urlLink;
            }
            Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
        }
    }

	startGetMeInterval() {
        let that = this;

		this.getMeInterval = timeouts.setInterval(()=> {
			if (this.props.me && this.props.me.data && this.props.me.data.emailStatus != "Verified") {
				this.props.getMe({}, ()=> {
					if (this.props.me.data.emailStatus == "Verified") {
						refreshToken();
					}
				});
			}
		}, 5000);
	}

	clearAllIntervals() {
		timeouts.clearInterval(this.getMeInterval);
	}

	getSentMessages() {

		let that = this;

		this.props.getSentMessages({ page: this.sentPage }, function (resultsCount) {
			that.setState({
				lastSentMessagesCount: resultsCount
			});
		});
		this.sentPage += 1;
	}

	getDraftMessages() {

		let that = this;

		this.props.getDraftMessages({ page: this.draftPage }, function (resultsCount) {
			that.setState({
				lastDraftMessagesCount: resultsCount
			});
		});
		this.draftPage += 1;
    }

    getPendingMessages() {

        let that = this;

        this.props.getPendingMessages();
    }

	getLikedMessages() {

		let that = this;

		this.props.getLikedMessages({ page: this.likedPage }, function (resultsCount) {
			that.setState({
				lastLikedMessagesCount: resultsCount
			});
		});
		this.likedPage += 1;
	}

    buttonStyle(colour, disabled, disabledColour, bottomButton) {

        let selectedColour = colour;
        if(disabled) {
            selectedColour = disabledColour;
        }

        let style = {
            paddingTop: 5,
            paddingBottom: 5,
            borderRadius: 5,
            overflow: 'hidden',
            alignSelf: 'stretch',
            marginBottom: 10,
            ...Platform.select({
                ios: {
                    backgroundColor: selectedColour
                }
            })
        };

        if (Platform.OS === 'ios' && bottomButton) {
            style.marginBottom = 0;
        } else if (Platform.OS === 'android' && bottomButton) {
            style.marginBottom = 30;
        }

        return style;
    }

    scrollViewStyle(bottomButton) {

		let style = {
			backgroundColor: '#fff',
			height: Dimensions.get('window').height,
			...Platform.select({
				android: {
                    marginTop: 60,
                    marginBottom: 96
				},
				ios: {
					marginBottom: 80,
				}
			})
		};

        if (!bottomButton) {
            style.marginBottom = 0;
        }

		return style;
    }

    updateEmailAndRefresh() {
        if (!/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(this.state.refreshEmail.toLowerCase())) {
            this.setState({
                emailRefreshError: "Please enter a vaild email address",
            });
            return false;
        }

        this.setState({
            emailRefreshError: "",
            emailRefreshed: true
        });
        this.props.updateMe({ email: this.state.refreshEmail });
    }

    logout() {
        this.props.logout(()=>{
            try {
                this.props.navigator.resetTo(getScene('login'));
            } catch (e) {
                console.log(e);
            }
        });
    }

    watchNewVideos() {
        if(this.props.me && this.props.me.receivingMessages && this.props.me.receivingMessages.length) {
            this.props.getMessage(this.props.me.receivingMessages[0].id);
            this.props.downloadVideo(this.props.me.receivingMessages[0].id);
            this.props.navigator.push(getScene('watchVideos'));
        }
    }

    renderLoading() {
        return (<LoadingScene />)
    }

    renderError() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: 8,
                    flex: 1
                }}>
                <Text>Couldn't connect to the server.</Text>
                <Button title='retry' onPress={() => this.props.getMe()}/>
            </View>
        );
    }

    renderVerifyEmailMessage() {
        if (this.props.me && this.props.me.data && this.props.me.data.emailStatus != "Verified") {
            return (
                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: 8,
                        flex: 1,
                        paddingTop: 40
                    }}>

                    <Text style={{ textAlign: 'center', marginBottom: 20, fontSize: 18, fontWeight: 'bold' }}>An email with a verification link has been sent. Please verify now.</Text>
                    <View style={styles.refreshEmail}>
                        {this.state && this.state.refreshEmail && !this.state.emailRefreshed && <TextInput
                            autoCorrect={false}
                            style={styles.verifyEmailError}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChange={(value)=> this.setState({ refreshEmail: value })}
                            value={this.state.refreshEmail}
                            placeholder={'Email address'}
                            maxLength = {100}
                            returnKeyType='done'
                        />}
                        {!this.state.emailRefreshed && <View style={styles.refreshEmailButton}>
                            <Button
                                title='Resend'
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                                onPress={() => this.updateEmailAndRefresh()}/>
                        </View>}
                        {this.state.emailRefreshed && <TouchableWithoutFeedback onPress={()=> this.setState({ emailRefreshed: false })}><Text style={styles.refreshEmailText}>Please check your emails</Text></TouchableWithoutFeedback>}
                    </View>
                    {this.state && !!this.state.emailRefreshError && <Text style={{ textAlign: 'center', color: 'red', marginBottom: 20 }}>{this.state.emailRefreshError}</Text>}

                    {false && <View style={this.buttonStyle('#4477dd', false)}>
                        <Button
                            color={(Platform.OS === 'ios') ? '#fff' : '#4477dd'}
                            onPress={() => this.props.navigator.push(getScene('editOrganisation'))}
                            style={{ margin: 5 }}
                            title='Edit my organisations' />
                    </View>}
                </View>
            );
        }
    }

    renderPage() {

        if (this.props.me.data && this.props.me.data.recipient && (this.props.me.data.recipient.status == "Approved" || (this.props.me.sentMessages.length == 0 && this.props.me.draftMessages.length == 0))) {
            return (
                <View style={{ flex: 1 }}>
                    {this.renderVerifyEmailMessage()}
                    {this.renderRecipientStats()}
                    {this.renderRecipientSection()}
					{this.renderIncreaseValue()}
                    {this.renderSenderSection()}
                </View>
            );
        } else if (this.props.me.data) {
            return(
                <View style={{ flex: 1 }}>
                    {this.renderVerifyEmailMessage()}
                    {this.renderSenderSection()}
                    {this.renderRecipientSection()}
                </View>
            );
        }
    }

    renderSenderSection() {

		let that = this;

        return (
			<View>
                {this.props.me && this.props.me.sentMessages && this.props.me.sentMessages.length == 0 && this.props.me.pendingMessages && this.props.me.pendingMessages.length == 0 && this.props.me.draftMessages && this.props.me.draftMessages.length == 0 && this.props.me.data.recipient && this.props.me.data.recipient.status != 'Approved' && <View style={styles.firstMessageButton}>
                    <Button
                        onPress={() => this.props.navigator.push(getScene('newMessage'))}
                        title={`Send your first message`}
                        color={(Platform.OS === 'ios') ? '#fff' : '#dd0000'}
                    />
                </View>}

				{!!this.props.me && (this.props.me.sentMessages.length > 0 || this.props.me.pendingMessages.length > 0) && <View style={{
					marginHorizontal: 10,
					marginTop: 20,
				}}>
					<View style={styles.segmentHeading}>
						<Text style={styles.sendingMessages}>Previously Raised</Text>
						<View style={styles.newButton}>
							<Button
								onPress={() => this.props.navigator.push(getScene('newMessage'))}
								title='New'
								color={(Platform.OS === 'ios') ? '#fff' : '#4477dd'} />
						</View>
					</View>

					<View>
                        {this.props.me.pendingMessages.map(function (message) {
                            return (<View key={'sent-'+message.id}>
                                <View style={styles.sentMessage}>
                                    <TouchableHighlight onPress={() => {
                                        that.props.cleanMessage();
                                        that.props.getMessage(message.id);
                                        that.props.navigator.push(getScene('furtherInformation'));
                                    }} style={styles.messageName}
                                        underlayColor={'#F0F0F0'}>
                                        <View style={styles.messageWrapper}>
                                            <View>
                                                <Text numberOfLines={1} style={styles.messageHeading}>{message.subject}</Text>
                                                <Text style={styles.messageSubheading}>Processing message</Text>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                    <View style={styles.arrowWrapper}>
                                        <Image source={require('../../images/ic_chevron_right_black_24dp/android/drawable-hdpi/ic_chevron_right_black_24dp.png')} style={styles.arrowText} />
                                    </View>
                                </View>
                            </View>)
                        })}
						{this.props.me.sentMessages.map(function (message) {
						 	if (message.status === "Sent") {
								return (<View key={'sent-'+message.id}>
									<View style={styles.sentMessage}>
										<TouchableHighlight onPress={() => {
                                            that.props.cleanMessage();
											that.props.getMessage(message.id);
											that.props.navigator.push(getScene('furtherInformation'));
										}} style={styles.messageName}
											underlayColor={'#F0F0F0'}>
											<View style={styles.messageWrapper}>
												{false && message.status !== "Sent" && <Image source={require('../../images/ic_mode_edit_black_24dp/android/drawable-hdpi/ic_mode_edit_black_24dp.png')} style={styles.messageIcon} />}
												{false && message.status === "Sent" && <Image source={require('../../images/ic_email_black_24dp/android/drawable-hdpi/ic_email_black_24dp.png')} style={styles.messageIcon} />}
												<View>
													<Text numberOfLines={1} style={styles.messageHeading}>{message.subject}</Text>
													{message.expires !== "0001-01-01T00:00:00" && !message.viewed && <Text style={styles.messageSubheading}>Expires {moment(new Date(message.expires)).format("dddd, Do MMM YYYY")}</Text>}
													{message.viewed && <Text style={styles.messageSubheading}>Message viewed</Text>}
												</View>
											</View>
										</TouchableHighlight>
										<View style={styles.arrowWrapper}>
											<Image source={require('../../images/ic_chevron_right_black_24dp/android/drawable-hdpi/ic_chevron_right_black_24dp.png')} style={styles.arrowText} />
										</View>
									</View>
								</View>)
							}
						})}
						{false && this.state && this.state.lastSentMessagesCount == 10 && <View style={styles.downloadMessages}>
							<TouchableHighlight onPress={this.getSendingMessages} underlayColor={'#F0F0F0'}>
								<View>
									<Text numberOfLines={1} style={{fontWeight: 'bold', fontSize: 15, textAlign: 'center'}}>Download older messages...</Text>
								</View>
							</TouchableHighlight>
						</View>}
					</View>
				</View>}

				{!!this.props.me && this.props.me.draftMessages.length > 0 && <View style={{
					marginHorizontal: 10,
					marginTop: 20,
                    marginBottom: 50
				}}>

					<View style={styles.segmentHeading}>
						<Text style={styles.sendingMessages}>Drafts</Text>
						<View style={styles.newButton}>
							<Button
								onPress={() => this.props.navigator.push(getScene('newMessage'))}
								title='New'
								color={(Platform.OS === 'ios') ? '#fff' : '#4477dd'} />
						</View>
					</View>

					<View>
						{this.props.me.draftMessages.map(function (message) {
						 	if (message.status !== "Sent") {
								return (<View key={'draft-'+message.id}>
									<View style={styles.sentMessage}>
										<TouchableHighlight onPress={() => {
                                            that.props.cleanMessage();
											that.props.getMessage(message.id);
											that.props.navigator.push(getScene('newMessageUpdate'));
										}} style={styles.messageName}
											underlayColor={'#F0F0F0'}>
											<View style={styles.messageWrapper}>
												{false && message.status !== "Sent" && <Image source={require('../../images/ic_mode_edit_black_24dp/android/drawable-hdpi/ic_mode_edit_black_24dp.png')} style={styles.messageIcon} />}
												{false && message.status === "Sent" && <Image source={require('../../images/ic_email_black_24dp/android/drawable-hdpi/ic_email_black_24dp.png')} style={styles.messageIcon} />}
												<View>
													<Text numberOfLines={1} style={styles.messageHeading}>{message.subject}</Text>
													{message.expires === "0001-01-01T00:00:00" && <Text style={styles.messageSubheading}>Draft message</Text>}
												</View>
											</View>
										</TouchableHighlight>
										<View style={styles.arrowWrapper}>
											<Image source={require('../../images/ic_chevron_right_black_24dp/android/drawable-hdpi/ic_chevron_right_black_24dp.png')} style={styles.arrowText} />
										</View>
									</View>
								</View>)
							}
						})}
						{false && this.state && this.state.lastDraftMessagesCount == 10 && <View style={styles.downloadMessages}>
							<TouchableHighlight onPress={this.getSendingMessages} underlayColor={'#F0F0F0'}>
								<View>
									<Text numberOfLines={1} style={{fontWeight: 'bold', fontSize: 15, textAlign: 'center'}}>Download older messages...</Text>
								</View>
							</TouchableHighlight>
						</View>}
					</View>
				</View>}
			</View>
        );
    }

    renderRecipientSection() {
        let content = null;

        switch(this.props.me.data && this.props.me.data.recipient && this.props.me.data.recipient.status) {

            case 'Approved':

                const likedVideos = this.props.me.likedMessages;

                content = (
                    <View>
                        {likedVideos.length > 0 && likedVideos.map(message => (
                            <View key={"liked-"+message.id}>
                                {message.subject !== "Raised Charity Initiative" && <View style={styles.sentMessage}>
                                    <TouchableHighlight key={message.id} onPress={() => {
                                        this.props.cleanMessage();
                                        this.props.getMessage(message.id);
                                        this.props.navigator.push(getScene('furtherInformation'));
                                    }} style={styles.messageName}
                                        underlayColor={'#F0F0F0'}>
                                        <View>
                                            <Text style={styles.messageHeading}>{message.subject}</Text>
                                            {message.date !== "0001-01-01T00:00:00" && <Text style={styles.messageSubheading}>Sent on {moment(new Date(message.date)).format("dddd, Do MMM YYYY")}</Text>}
                                        </View>
                                    </TouchableHighlight>
                                    <View style={styles.arrowWrapper}>
                                        <Image source={require('../../images/ic_chevron_right_black_24dp/android/drawable-hdpi/ic_chevron_right_black_24dp.png')} style={styles.arrowText} />
                                    </View>
                                </View>}
                            </View>
                        ))}
						{false && this.state && this.state.lastLikedMessagesCount == 10 && <View style={styles.downloadMessages}>
							<TouchableHighlight onPress={this.getLikedMessages} underlayColor={'#F0F0F0'}>
								<View>
									<Text numberOfLines={1} style={{fontWeight: 'bold', fontSize: 15, textAlign: 'center'}}>Download older messages...</Text>
								</View>
							</TouchableHighlight>
						</View>}

                        {false && !!this.props.me && this.props.me.likedMessages.length == 0 && <View style={{ padding: 10, backgroundColor: '#F0F0F0'  }}>
                            <Text style={{ marginTop: 5, marginBottom: 5 }}>
                                No messages to display.
                            </Text>
                        </View>}
                    </View>
                );
                break;

            case 'Pending':
                return false;
                // content = (
                //     <View style={{ padding: 10, backgroundColor: '#F0F0F0' }}>
                //         <Text style={{ marginBottom: 10 }}>
                //             Your ability to receive messages is pending approval. In the mean time you should set up which charities you would like to support!
                //         </Text>

                //         <View style={this.buttonStyle('#4477dd', false)}>
                //             <Button color={(Platform.OS === 'ios') ? '#fff' : '#4477dd'} onPress={() => this.props.navigator.push(getScene('charities'))} title='Configure Charities'/>
                //         </View>

                //         <View style={this.buttonStyle('#4477dd', false)}>
                //             <Button color={(Platform.OS === 'ios') ? '#fff' : '#4477dd'} onPress={() => this.props.getMe()} title='Refresh'/>
                //         </View>
                //     </View>
                // );
                // break;

            case 'False':
            default:
                return false;
                // content = (
                //     <View style={{ padding: 10, backgroundColor: '#F0F0F0' }}>
                //         <Text style={{ marginBottom: 10 }}></Text>
                //         <View style={this.buttonStyle('#4477dd', false)}>
                //             <Button color={(Platform.OS === 'ios') ? '#fff' : '#4477dd'} onPress={() => this.props.signupToReceiveMessages()} title='Sign up to receive messages'/>
                //         </View>
                //     </View>
                // );
                //break;
        }

        let videosToWatch = this.props.me.receivingMessages.length;
        if (videosToWatch == 10) {
            videosToWatch = "10+"
        }

        return (
             <View style={{
                 marginHorizontal: 10,
                 marginTop: 20,
                 marginBottom: 0
             }}>

                {!!videosToWatch && <View style={styles.watchVideosButton}>
                    <Button
                        onPress={() => {
                            this.props.cleanMessage();
                            this.watchNewVideos()}
                        }
                        title={`Watch and discover (${videosToWatch} available)`}
                        color={(Platform.OS === 'ios') ? '#fff' : '#dd0000'}
                    />
                </View>}

                {!videosToWatch && <View style={styles.noVideosButton}>
                    <Button
                        title={`Nothing to watch - come back soon`}
                        color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                    />
                </View>}

				{this.props.me.likedMessages.length > 0 && <View style={styles.segmentHeading}>
                    <Text style={styles.receivingMessages}>Previously raised your interest</Text>
                </View>}

                {content}

            </View>
        );
    }

	renderRecipientCharities() {
		let that = this;

		return(<View>
			{this.props.myCharities.data.map(function(charity) {
				return(<Text key={'charity-'+charity.id} onPress={()=> that.props.navigator.push(getScene('charities'))} numberOfLines={1} style={styles.detailLink}>{charity.name}</Text>);
			})}
		</View>);
	}

    renderRecipientStats() {
		if (this.props.me.data && this.props.me.data.recipient && ((this.props.me.data.recipient.status != 'Approved' && this.state && this.state.notFirstVisit) || !this.props.me.data.introVideoViewed)) {
			return <View></View>;
		}

        return (
            <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
                <View style={styles.detailWrapper}>
                    {false && <View style={styles.messageDetail}>
                        <Text style={styles.detailHeading}>Money Raised Today:</Text>
                        <Text numberOfLines={1} style={styles.detailContent}>£300</Text>
                    </View>}
					{this.props.myCharities.data && this.props.myCharities.data.length > 0 && <View style={styles.messageDetail}>
                        <Image style={{ width: 40, height: 40 }} source={require('../../images/home-screen/charities.png')} />
                        <Text style={styles.detailHeading}>CHARITIES SUPPORTED</Text>
                        {this.renderRecipientCharities()}
                    </View>}
					<View style={styles.messageDetail}>
                        <Image style={{ width: 40, height: 40 }} source={require('../../images/home-screen/message-fee.png')} />
                        <Text style={styles.detailHeading}>RAISED PER MESSAGE</Text>
                        <Text onPress={()=> this.props.navigator.push(getScene('charities'))} numberOfLines={1} style={styles.detailValueLink}>£{this.props.me.data.donation}</Text>
                    </View>
                    <View style={styles.messageDetail}>
                        <Image style={{ width: 40, height: 40 }} source={require('../../images/home-screen/viewed.png')} />
                        <Text style={styles.detailHeading}>VIEWED TO DATE</Text>
                        <Text numberOfLines={1} style={styles.detailValue}>{this.props.me.data.videosWatched}</Text>
                    </View>
                    {false && <View style={styles.messageDetail}>
                        <Text style={styles.detailHeading}>Time Spent:</Text>
                        <Text numberOfLines={1} style={styles.detailValue}>25m 10s</Text>
                    </View>}
                    <View style={styles.messageDetail}>
                        <Image style={{ width: 40, height: 40 }} source={require('../../images/home-screen/money-raised.png')} />
                        <Text style={styles.detailHeading}>ALL-TIME RAISED</Text>
                        <Text numberOfLines={1} style={styles.detailValue}>£{this.props.me.data.totalRaised}</Text>
                    </View>
                </View>
            </View>
        );
    }

	renderIncreaseValue() {
		if (this.props.me.data && this.props.me.likedMessages.length == 0 && this.props.me.sentMessages.length == 0 && this.props.me.draftMessages.length == 0 && this.props.me.pendingMessages.length == 0 && (this.props.me.data.recipient.status == 'Approved' || this.props.me.data.recipient.status == 'Disabled')) {
			return (<View>
				<Text style={{ textAlign: 'center', fontWeight: 'bold', marginTop: 20, marginBottom: 20 }}>Increase the value you get and give from Raised...</Text>
				<View>
					<View style={styles.reachMeRaised}>
						<Image resizeMode="contain" style={styles.reachMeRaisedIcon} source={require('../../images/Reach Us On Raised.png')} />
						<TouchableWithoutFeedback onPress={()=> this.openUrl(this.props.links? this.props.links.reachMeOnRaised : null)}>
                            <View>
                                <Text style={styles.reachMeRaisedLink}>Add our icon to your websites contact page. Find out how.</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
					<View style={styles.increaseValue}>
						<Image style={styles.increaseValueIcon} source={require('../../images/linkedin.png')} />
                        <TouchableWithoutFeedback onPress={()=> this.openUrl(this.props.links? this.props.links.sharing : null)}>
                            <View>
						        <Text style={styles.increaseValueLink}>Tell the world you can be reached via Raised. Learn how.</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
					<View style={styles.increaseValue}>
						<Image style={styles.increaseValueIcon} source={require('../../images/ic_phone_black_24dp/web/ic_phone_black_24dp_2x.png')} />
						<Text style={styles.increaseValueLink}>Get cold callers directed to Raised as your preferred contact method. Read more.</Text>
					</View>
				</View>
            </View>);
		}
	}

    render() {

        let page = null;

        if (this.props.me.loading) {
            return this.renderLoading();
        }

        if (!this.props.organisations || !this.props.organisations.data || this.props.organisations.data.length == 0) {
            if (this.state && this.state.networkError) {
                return (<View>
                    <Text style={{ textAlign: 'center', color: 'red', marginVertical: 20 }}>{this.state.networkError}</Text>
                </View>);
            } else if (this.props.connection) {
                return null;
            }
        }

        if (!this.props.loading) {
            if ((this.props.me.data && !this.props.me.data.hasCharities) && this.props.me.data.emailStatus !== 'Verified') {
                page = this.renderVerifyEmailMessage();
            } else {
                if (this.props.me.loading && this.props.me.data && this.props.me.data.emailStatus === 'Verified') {
                    page = this.renderLoading();
                } else {
                    page = this.renderPage();
                }
            }

//			if(this.props.me.error) {
//                page = this.renderError();
//            }
        } else {
            page = this.renderLoading();
        }

        console.log("CONNECTION", this.props.connection);

        return (
            <View style={{flex: 1}}>

                {(Platform.OS === 'android') && (<ToolbarAndroid
                    navIcon={require('../../images/ic_menu_white_24dp/android/drawable-hdpi/ic_menu_white_24dp.png')}
                    onIconClicked={() => App.drawer.openDrawer()}
                    style={styles.toolbar}
                    title='Home'
                    titleColor='#fff'
                />)}

                <View style={styles.app}>
                    <KeyboardAwareScrollView
                        automaticallyAdjustContentInsets={false}
                        style={this.scrollViewStyle(this.props.me.data && this.props.me.data.recipient && this.props.me.data.recipient.status != 'Approved')}
                        keyboardShouldPersistTaps="handled">
                        <View style={{ flex: 1, marginBottom: 20 }}>
                            {page}
                        </View>
                    </KeyboardAwareScrollView>

					{this.props.me.data && this.props.me.data.recipient && this.props.me.data.recipient.status != 'Approved' && <View style={styles.bottomButtonWrapper}>
						<View style={this.buttonStyle('#61b746', false, '', true)}>
                            <Button
                                title='Sign up to receive messages'
                                color={(Platform.OS === 'ios') ? '#fff' : '#61b746'}
                                onPress={() => this.props.navigator.push(getScene('charities'))} />
                        </View>
                    </View>}
                </View>

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}