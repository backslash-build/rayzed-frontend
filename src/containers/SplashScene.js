import React, {Component} from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    Platform,
    TextInput,
    Button,
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    ActivityIndicator
} from 'react-native';

import { getScene } from '../services/navigation';
import * as actions from '../actions';

const styles = StyleSheet.create({
    app: {
        backgroundColor: '#61b746',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimensions.get('window').width,
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    logo: {
        width: 200,
        height: 65
    },
    loadingText: {
        marginTop: 20,
		fontSize: 20,
		fontWeight: 'bold',
		color: '#fff'
    },
    refreshButton: {
        overflow: 'hidden',
        borderRadius: 5,
        flex: 1,
        width: 100,
        maxHeight: 80,
        margin: 20,
        ...Platform.select({
            ios: {
                backgroundColor: '#fff'
            }
        })
    },
});

@connect(state => ({ ...state, links: state.links.data }))
@autobind
export default class SplashScene extends Component {

    render() {

        let connection = "";
        if (this.props.login.connection === true) {
            connection = "Connected";
        } else if (this.props.login.connection === false) {
            connection = "Not connected";
        } else if (this.props.login.connection === null) {
            connection = "Nobody knows";
        }

        console.log("CONNECTION 2", connection);

        return (
            <View style={styles.app}>
                <Image style={styles.logo} source={require('../../images/Raised-white.png')} />
                {(this.props.login.connection === null || this.props.login.connection) && <ActivityIndicator
                    animating={true}
                    style={[styles.centering, {height: 80}]}
                    size="large"
                    color="white"
                />}
                {this.props.login.connection === false && <Text style={{ textAlign: 'center', color: 'white', marginTop: 15, fontSize: 17, marginLeft: 15, marginRight: 15 }}>Sorry, poor Internet connectivity detected. Please reconnect and try again, or come back when later when you've got better coverage.</Text>}
                {this.props.login.connection === false && <View style={styles.refreshButton}>
                    <Button
                        onPress={this.props.onRetry}
                        title='Retry'
                        color={(Platform.OS === 'ios') ? '#61b746' : '#bbb'} />
                </View>}
            </View>

        );
    }
}