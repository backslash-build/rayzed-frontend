import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../services/navigation';
import * as actions from '../actions';

import SplashScene from './SplashScene';
import LoadingScene from './LoadingScene';
import App from '../App';
import CharitiesForm from '../components/CharitiesForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state, me: state.me, allCharities: state.allCharities, myCharities: state.myCharities, organisation: state.organisations, connection: state.login.connection}),
    dispatch => bindActionCreators(actions, dispatch)
)
@autobind
export default class CharitiesScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        logout: React.PropTypes.func,
        myCharities: React.PropTypes.object,
        getAllCharities: React.PropTypes.func,
        getMyCharities: React.PropTypes.func,
        detachCharity: React.PropTypes.func,
        me: React.PropTypes.object,
        updateMe: React.PropTypes.func,
        organisations: React.PropTypes.object,
    }

    state = {
        error: ''
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
        this.props.getMyCharities();
        this.props.getAllCharities();
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    findCharity() {
        this.props.navigator.push(getScene('editCharities'));
    }

    detachCharity(payload) {
        this.props.detachCharity(payload);
        this.setState({
            error: ''
        });
    }

    submit(payload) {
        this.props.updateMe(payload);

		if (this.props.me.data.introVideoViewed) {
        	this.back();
		} else {
			this.props.navigator.push(getScene('importantMessage'));
		}
    }

    render() {

        return (
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Your Charities'
                    titleColor='#fff'
                />}
                {(this.props.myCharities.data == null) && <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height}}><LoadingScene /></View>}
                {this.props.myCharities.data != null && <CharitiesForm
                    me={this.props.me}
                    onDetachCharity={this.detachCharity}
                    myCharities={this.props.myCharities}
                    onSubmit={this.submit}
                    error={this.state.error}
                    onFindCharity={this.findCharity}
                    />}

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}