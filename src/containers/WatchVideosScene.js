import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SplashScene from './SplashScene';

import { getScene } from '../services/navigation';
import * as actions from '../actions';
import * as messagingActions from '../actions/messaging';

import App from '../App';
import WatchVideoForm from '../components/WatchVideoForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state.messaging, me: state.me, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class WatchVideosScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        message: React.PropTypes.object,
        me: React.PropTypes.object,
        video: React.PropTypes.string,
        likeMessage: React.PropTypes.func,
        dislikeMessage: React.PropTypes.func,
        myCharities: React.PropTypes.object,
        downloadVideo: React.PropTypes.func
    }

    state = {
        messageRetrieved: false
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

	componentDidUpdate() {
		if(this.props.message && !this.state.messageRetrieved) {
            this.props.downloadVideo(this.props.message.id);
			this.setState({
				messageRetrieved: true
			});
        }
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    retryConnection() {
        this.props.testConnection(()=> {
            if (connectionStatus) {
                this.props.getMessage(this.props.message.id);
                this.props.downloadVideo(this.props.message.id);
            }
        });
    }

    reportVideo() {

        let that = this;

        let currentVideoId = 0;

        if(this.props.me && this.props.me.receivingMessages && this.props.me.receivingMessages.length) {

            for (let i = 0; i < this.props.me.receivingMessages.length; i++) {
                if (this.props.me.receivingMessages[i].id == this.props.message.id) {
                    currentVideoId = i + 1;
                    break;
                }
            }

            this.props.getMessage(this.props.me.receivingMessages[currentVideoId].id, function () {
                that.props.downloadVideo(that.props.message.id);
            });
        }
    }

    like(comments) {
        const payload = {
            id: this.props.message.id,
            data: {
                comments
            }
        };

        this.props.likeMessage(payload);
        this.props.getReceivingMessages();
        this.props.getMe();
        this.props.navigator.replace(getScene('furtherInformation'));
    }

    dislike(comments) {
        const payload = {
            id: this.props.message.id,
            data: {
                comments
            }
        };

        this.props.dislikeMessage(payload);
        this.props.getReceivingMessages();
        this.props.getMe();
        this.props.navigator.pop();
    }

    render() {

        if (this.props.connection === false) {
            return (<SplashScene />)
        }

        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Watch Videos'
                    titleColor='#fff'
                />}

                <WatchVideoForm
                    videoUrl={this.props.video}
                    message={this.props.message}
                    me={this.props.me}
                    onLike={this.like}
                    onDislike={this.dislike}
                    onReportVideo={this.reportVideo} />

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.retryConnection()} /></View>}
            </View>
        );
    }
}
