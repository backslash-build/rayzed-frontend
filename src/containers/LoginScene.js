import React, {Component} from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    Platform,
    TextInput,
    Button,
    View,
    StyleSheet,
    Image,
    KeyboardAvoidingView,
    ScrollView,
    Dimensions
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { getScene } from '../services/navigation';
import * as actions from '../actions';
import LoginForm from '../components/LoginForm';

const styles = StyleSheet.create({
    app: {
        flex: 1,
        height: Dimensions.get('window').height,
        ...Platform.select({
            android: {
                paddingBottom: 50,
                width: Dimensions.get('window').width,
            }
        })
    },
    emailInput: {
        height: 40,
        marginTop: 30,
        marginBottom: 15,
        marginHorizontal: 15
    },
    passwordInput: {
        height: 40,
        marginTop: 15,
        marginBottom: 30,
        marginHorizontal: 15
    },
    registerButton: {
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 30,
        ...Platform.select({
            ios: {
                backgroundColor: '#dd0000'
            }
        }),
        marginHorizontal: 15
    },
    aboutButton: {
        marginTop: 30,
        marginBottom: 30,
        borderRadius: 5,
        overflow: 'hidden',
        ...Platform.select({
            ios: {
                backgroundColor: '#4477dd'
            }
        }),
        marginHorizontal: 15
    },
    logoWrapper: {
        backgroundColor: '#61b746',
        alignItems: 'center',
        paddingVertical: 50,
        marginBottom: 30
    },
    logo: {
        width: 280,
        height: 90
    },
});

@connect(state => ({ ...state.login, links: state.links.data}), dispatch => bindActionCreators(actions, dispatch))
@autobind
export default class LoginScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        login: React.PropTypes.func,
        error: React.PropTypes.string,
        loading: React.PropTypes.bool
    }

    state = {
        keyboardShowing: false
    }

    scrollViewStyle() {
        let style = {
            backgroundColor: '#fff',
            height: Dimensions.get('window').height,
            ...Platform.select({
                android: {
                    position: 'absolute',
                    marginBottom: 80,
                    width: Dimensions.get('window').width,
                }
            })
        };

        if (this.state.keyboardShowing) {
            style.marginBottom = 15;
        }

        return style;
    }

    login(payload) {
        this.props.login(payload, ()=>{
            try {
                this.props.navigator.resetTo(getScene('home'));
            } catch (e) {
                console.log(e);
            }
        });
    }

    openUrl(url) {

        let urlLink = url;

        if (url.indexOf("https://") != -1) {
            this.props.navigator.push(getScene('viewLink', { url: url }));
        } else {
            if (url.indexOf("http://") === -1) {
                urlLink = "http://" + urlLink;
            }
            Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
        }
    }

    render() {
        return (
            <View style={styles.app}>
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={"handled"}
                    automaticallyAdjustContentInsets={false}
                    keyboardOpeningTime={0}
                    onKeyboardWillShow={()=> this.setState({ keyboardShowing: true })}
                    onKeyboardWillHide={()=> this.setState({ keyboardShowing: false })}
                    style={this.scrollViewStyle()}>

                    <View style={styles.logoWrapper}>
                        <Image style={styles.logo} source={require('../../images/Raised-white.png')} />
                    </View>
                    <View style={styles.registerButton}>
                        <Button
                            onPress={()=>this.props.navigator.push(getScene('register'))}
                            title='New User - Register'
                            color={(Platform.OS === 'ios') ? '#fff' : '#dd0000'}
                        />
                    </View>

                    <LoginForm loading={this.props.loading} error={this.props.error} onSubmit={this.login} links={this.props.links} keyboardShowing={this.state.keyboardShowing} testConnection={this.props.testConnection} openUrl={this.openUrl} />

                    <View style={styles.aboutButton}>
                        <Button
                            onPress={()=>this.props.navigator.push(getScene('about'))}
                            title='About'
                            color={(Platform.OS === 'ios') ? '#fff' : '#4477dd'}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>

        );
    }
}