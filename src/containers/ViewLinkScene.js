import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions,
    WebView
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../services/navigation';
import * as actions from '../actions';

import SplashScene from './SplashScene';
import LoadingScene from './LoadingScene';
import App from '../App';
import CharitiesForm from '../components/CharitiesForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@autobind
export default class ViewLinkScene extends Component {

    state = {
        pageLoading: true
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    render() {

        return (
            <View style={styles.app}>
                {this.state.pageLoading && <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height}}><LoadingScene /></View>}
                <WebView
                    automaticallyAdjustContentInsets={false}
                    contentInset={{ top: 0 }}
                    scalesPageToFit={true}
                    onNavigationStateChange={this.onNavigationStateChange}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    source={{
                        uri: this.props.url,
                    }}
                    onError={(error)=> console.log("Error", error)}
                    onLoadEnd={()=> { this.setState({ pageLoading: false }); console.log("Loading end"); } }
                    onLoad={()=> { this.setState({ pageLoading: false }); console.log("Loading end"); } }
                  />
            </View>
        );
    }
}