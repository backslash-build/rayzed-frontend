import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    View,
    BackAndroid,
    StyleSheet,
    Platform,
    ToolbarAndroid
} from 'react-native';

import * as actions from '../actions';
import { getScene} from '../services/navigation';
import RegisterForm from '../components/RegisterForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    }
});

@connect(state => ({ ...state.register, links: state.links.data}), dispatch => bindActionCreators(actions, dispatch))
@autobind
export default class RegisterScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,

        //appstate:
        error: React.PropTypes.string,
        loading: React.PropTypes.bool,

        //actions:
        register: React.PropTypes.func,
        registerFail: React.PropTypes.func,
        registerClear: React.PropTypes.func
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    register(payload) {
        this.props.register(payload, ()=>{
            // this.props.navigator.replace(getScene('editOrganisation'));
            // this.props.navigator.replaceAtIndex(getScene('home'), 0);
        });
    }

    back() {
        this.props.registerClear();
        this.props.navigator.popN(1);
        return true;
    }

    translateError(error) {
        if (!error) {
            return '';
        }

        if (error.indexOf("EmailAlreadyTaken:An error has occurred") !== -1){
            return "Email already in use";
        } else if (error.indexOf("UsernameAlreadyTaken:An error has occurred") !== -1) {
            return "Username already in use";
        } else if (error.indexOf("EmailInvalid:An error has occurred") !== -1) {
            return "Invalid email address";
        } else if (error == "Register request timed out.") {
            return error;
        }
        return error;
    }

    openUrl(url) {

        let urlLink = url;

        if (url.indexOf("https://") != -1) {
            this.props.navigator.push(getScene('viewLink', { url: url }));
        } else {
            if (url.indexOf("http://") === -1) {
                urlLink = "http://" + urlLink;
            }
            Linking.openURL('http://www.linkedin.com').catch(err => console.error('An error occurred', err));
        }
    }

    render() {

        let error = this.translateError(this.props.error);

        return (
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    titleColor='#fff'
                    title='Register'
                />}
                <RegisterForm loading={this.props.loading} onSubmit={this.register} error={error} links={this.props.links} testConnection={this.props.testConnection} openUrl={this.openUrl} />
            </View>
        );
    }
}