import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions,
    Linking
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../services/navigation';
import * as actions from '../actions';
import * as messagingActions from '../actions/messaging';

import LoadingScene from './LoadingScene';
import EditCharitiesForm from '../components/EditCharitiesForm';

import SplashScene from './SplashScene';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state, me: state.me, allCharities: state.allCharities, myCharities: state.myCharities, organisation: state.organisations, connection: state.login.connection}),
    dispatch => bindActionCreators(actions, dispatch)
)
@autobind
export default class EditCharitiesScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        testConnection: React.PropTypes.func,
        message: React.PropTypes.any,
        allCharities: React.PropTypes.object,
        attachCharity: React.PropTypes.func,
    }

    componentDidMount() {
        this.props.getAllCharities();
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    state = {
        showingResults: false,
        searchTerms: {
            nameSearch: ''
        }
    }

    search(query) {
        this.setState({
            showingResults: true
        })
        this.props.searchRecipients(query);
    }

    updateSearchTerms(type, term) {
        let searchTerms = this.state.searchTerms;
        searchTerms[type] = term;
        this.setState({ searchTerms: searchTerms });
    }

    clearSearch() {
        this.setState({
            showingResults: false
        })
    }

    attachCharity(payload) {
        this.props.attachCharity(payload);
        this.props.getMyCharities();
        this.back();
    }

    openUrl(url) {

        let urlLink = url;

        if (url.indexOf("https://") != -1) {
            this.props.navigator.push(getScene('viewLink', { url: url }));
        } else {
            if (url.indexOf("http://") === -1) {
                urlLink = "http://" + urlLink;
            }
            Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
        }
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Charities you support'
                    titleColor='#fff'
                />}

                {!this.props.loading &&  <EditCharitiesForm
                    searchTerms={this.state.searchTerms}
                    updateSearchTerms={this.updateSearchTerms}
                    onSearch={this.search}
                    onSubmit={this.back}
                    openUrl={this.openUrl}
                    allCharities={this.props.allCharities.data}
                    myCharities={this.props.myCharities.data}
                    onAttachCharity={this.attachCharity}
                    showingResults={this.state.showingResults}
                    clearSearch={this.clearSearch}
                    requestCharity={this.props.requestCharity} />}
                {this.props.loading && <LoadingScene />}

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
