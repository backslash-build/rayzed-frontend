import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Linking,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../services/navigation';
import * as actions from '../actions';
import * as messagingActions from '../actions/messaging';

import App from '../App';
import SplashScene from './SplashScene';
import LoadingScene from './LoadingScene';
import FurtherInformationForm from '../components/FurtherInformationForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state.messaging, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class FurtherInformationScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        message: React.PropTypes.object,
        downloadVideo: React.PropTypes.func,
        video: React.PropTypes.string,
        getAttachments: React.PropTypes.func,
        getMessagePaymentDetails: React.PropTypes.func,
        attachments: React.PropTypes.array,
        downloadAttachment: React.PropTypes.func
    }

    state = {
		messageRetrieved: false
	}

    componentDidMount() {
        if(this.props.message) {
            this.props.downloadVideo(this.props.message.id);
            this.props.getAttachments(this.props.message.id);
        }
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

	componentDidUpdate() {
		if(this.props.message && !this.state.messageRetrieved) {
            this.props.downloadVideo(this.props.message.id);
            this.props.getAttachments(this.props.message.id);
            this.props.getMessagePaymentDetails(this.props.message.id, (invoice) => {
                console.log("invoice", invoice);
                this.setState({
                    invoice: invoice
                });
            });
			this.setState({
				messageRetrieved: true
			});
        }
	}

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    retryConnection() {
        this.props.testConnection((connectionStatus)=> {
            if (connectionStatus) {
                this.props.getMessage(this.props.message.id);
                this.props.downloadVideo(this.props.message.id);
                this.props.getAttachments(this.props.message.id);
                this.props.getMessagePaymentDetails(this.props.message.id, (invoice) => {
                    console.log("invoice", invoice);
                    this.setState({
                        invoice: invoice
                    });
                });
            }
        });
    }

    back() {
        this.props.navigator.popN(1);
        this.props.cleanMessage();
        return true;
    }

    onViewFile(file) {
        this.setState({ loading: true, error: null });
        this.props.downloadAttachment({ id: this.props.message.id, attachmentId: file.id, name: file.title }, (error, downloadedFile) => {
            if (error) {
                this.setState({
                    error: error,
                    loading: false
                });
            } else {
                this.props.navigator.push(getScene('viewAttachment'));

                let that = this;
                setTimeout(function(){
                    that.setState({
                        loading: false
                    });
                }, 2000);
            }
        });
    }

    render() {

		if (this.props.loading) {
            return (<View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height}}><LoadingScene /></View>);
        }

        console.log(this.props);

        return (
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Further Information'
                    titleColor='#fff'
                />}

                {!this.state.loading && <FurtherInformationForm message={this.props.message} videoUrl={this.props.video} attachments={this.props.attachments} viewFile={this.onViewFile} invoice={this.state.invoice} error={this.state.error} />}
                {this.state.loading && <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height}}><LoadingScene /></View>}

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.retryConnection()} /></View>}
            </View>
        );
    }
}
