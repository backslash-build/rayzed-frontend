import React, {Component} from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    Platform,
    TextInput,
    Button,
    View,
    Text,
    StyleSheet,
    Image,
	Dimensions
} from 'react-native';

import { getScene } from '../services/navigation';
import * as actions from '../actions';

const styles = StyleSheet.create({
    app: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
		...Platform.select({
            android: {
        		height: Dimensions.get('window').height
            },
            ios: {
        		height: Dimensions.get('window').height - 64
            }
        })
    },
    logo: {
        width: 75,
        height: 75
    }
});

export default class SplashScene extends Component {

    render() {
        return (
            <View style={styles.app}>
                <Image style={styles.logo} source={require('../../images/loading.gif')} />
            </View>

        );
    }
}