import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Linking,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SplashScene from '../SplashScene';

import { getScene } from '../../services/navigation';
import * as actions from '../../actions';
import * as messagingActions from '../../actions/messaging';
import timeouts from '../../services/timeouts';

import NewMessageContactForm from '../../components/NewMessage/ContactForm';
import NewMessagePaymentForm from '../../components/NewMessage/PaymentForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({ ...state, connection: state.login.connection }), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class NewMessageContactScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        updateMessage: React.PropTypes.func,
        sendMessage: React.PropTypes.func,
        localUpdateMessage: React.PropTypes.func,
        me: React.PropTypes.object,
        messaging: React.PropTypes.object,
        message: React.PropTypes.object
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);

        if (!this.props.messaging.message.organisation) {
            this.change({
                organisation: this.props.organisations.data[0].name,
                website: this.props.organisation.data[0].website
            })
        }
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

	state = {
		message: this.props.messaging.message,
        error: '',
        disableSubmit: false
	}

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    change(value) {
        this.props.localUpdateMessage(value);
    }

    startSentInterval() {
        let that = this;

		this.getSentInterval = timeouts.setInterval(()=> {
			if (this.props.messaging.message) {
				this.props.getMessage(this.state.message.id, ()=> {
					console.log("Message status", this.props.messaging.message.status);
					if (this.props.messaging.message.status == "Sent") {
						this.props.navigator.push(getScene('newMessageRaised'));
						this.clearAllIntervals();
					}
				});
			}
		}, 5000);
	}

    clearAllIntervals() {
		timeouts.clearInterval(this.getSentInterval);
	}

	translateError(error) {
        if (!error) {
            return '';
        }

        if (error.indexOf("VideoNotEncoded") !== -1){
            return "Sorry, there was an error with your video";
        } else if (error.indexOf("RecipientHasExceedMaxMessages") !== -1) {
            return "Sorry, this recipient has exceeded their message limit this week";
        }
        return error;
    }

    submit() {}

	openUrl(url) {
        let urlLink = url;

        // has to start with http:// or https://
        if(url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
    }

	submit(value, callback) {

        // if (!/^[-a-zA-Z0-9@:%.+~#=]{2,256}.[a-z]{2,6}\b([-a-zA-Z0-9@:%+.~#?&//=])/.test(value.website)) {
        //     this.setState({
        //         error: "Please enter a vaild website"
        //     });
        //     return false;
        // }

        // if (value.website.indexOf("http://") == -1 && value.website.indexOf("https://") == -1) {
        //     value.website = "http://" + value.website;
        // }

		this.setState({
            updatePending: true,
            disableSubmit: true
		});
        this.props.updateMessage(value, (response) => {
            console.log("Response", response);
			if (this.state.updatePending) {
				this.setState({
					error: null,
					updatePending: false
                });

                let that = this;

                that.props.sendMessage({ id: this.state.message.id, returnUrl: "https://inbox.raised.global/messages/paymentResponse" }, (paymentDetails) => {
                    if (paymentDetails.paymentUrl) {
                        // this.startSentInterval();
                        // this.openUrl(paymentDetails.paymentUrl);

                        this.props.navigator.push(getScene('newMessagePayment'));

                        that.setState({
                            error: null,
                            disableSubmit: false
                        });
                    } else if (paymentDetails[0] && paymentDetails[0].error) {
                        that.setState({
                            error: that.translateError(paymentDetails[0].error),
                            disableSubmit: false
                        });

                        callback();
                    } else if (paymentDetails.error) {
                        that.setState({
                            error: that.translateError(paymentDetails.error),
                            disableSubmit: false
                        });
                    }
                });
			}
        });
    }

    render() {
        return(
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Contact Details'
                    titleColor='#fff'
                />}

                <NewMessageContactForm
                    me={this.props.me.data}
                    message={this.props.messaging.message}
                    defaultOrganisation={this.props.organisations.data[0]}
                    onChange={this.change}
                    onSubmit={this.submit}
					error={this.state.error}
                    disableSubmit={this.state.disableSubmit} />

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
