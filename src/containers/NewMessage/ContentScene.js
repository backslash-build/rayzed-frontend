import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SplashScene from '../SplashScene';

import { getScene } from '../../services/navigation';
import timeouts from '../../services/timeouts';
import * as actions from '../../actions';
import * as messagingActions from '../../actions/messaging';

import LoadingScene from '../LoadingScene';

import NewMessageContentForm from '../../components/NewMessage/ContentForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state.messaging, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class NewMessageContentScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        uploadVideo: React.PropTypes.func,
        downloadVideo: React.PropTypes.func,
        getAttachments: React.PropTypes.func,
        uploadAttachments: React.PropTypes.func,
        downloadAttachment: React.PropTypes.func,
        deleteAttachment: React.PropTypes.func,
        message: React.PropTypes.object,
        getMessage: React.PropTypes.object,
        video: React.PropTypes.string,
        attachments: React.PropTypes.object
    }

    componentDidMount() {
        setTimeout(()=>this.props.navigator.replaceAtIndex(getScene('newMessageUpdate'), -2), 100);
        if (this.props.message.id) {
            this.props.downloadVideo(this.props.message.id);
            this.props.getAttachments(this.props.message.id);
        }

        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    state = {
        uploadingText: '',
        uploadedVideo: {},
        uploadedFiles: [],
        message: this.props.message
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    secondsToMins(seconds) {

    }

    startEncodingCountdown() {
        let that = this;

        this.encodingInterval = timeouts.setInterval(()=> {
            if (this.encodingCountdown > 0) {

                this.encodingCountdown = this.encodingCountdown - 1;
                let mins = Math.floor(this.encodingCountdown / 60);
                let secs = this.encodingCountdown % 60;

                let timeText = 'Encoding Video (' + mins + 'min' + (mins > 1? 's ' : ' ') + secs + 'sec' + (secs > 1? 's' : '') + ')'

                this.setState({
                    uploadingText: timeText
                });
            }
        }, 1000);
    }

    clearAllIntervals() {
        timeouts.clearInterval(this.encodingInterval);
    }

    updateProgress(type, percent) {
        this.setState({
            uploadingText: "Uploading " + type + " (" + Math.ceil(percent * 100) + "%)"
        });
    }

    uploadAdditionalFiles(additionalFiles) {

        let files = [];
        additionalFiles.map((file, index) => {

            const isVideo = extension => extension == "3gp" || extension == "mp4" || extension == "ts" || extension == "webm" || extension == "mkv";

            // if (extension == "3gp" || file.extension == "mp4" || file.extension == "ts" || file.extension == "webm" || file.extension == "mkv" || file.extension == "mov") {
            //     mimeType = 'video/' + file.extension;
            // } else if (file.extension == "pdf") {
            //     mimeType = 'application/pdf';
            // }

            let mimeType = "";
            if (file.extension.toLowerCase() == "mov") {
                mimeType = 'video/mp4';
                file.extension = "mp4";
            } else if (isVideo(file.extension.toLowerCase())) {
                mimeType = 'video/' + file.extension;
            } else {
                mimeType = 'application/pdf';
            }

            files.push({
                title: file.filename,
                mimeType: mimeType,
                extension: file.extension,
                path: file.uri,
                fileSize: file.fileSize
            });
        });

        this.props.uploadAttachments({ files, id: this.props.message.id }, this.updateProgress, ()=> {
            this.submit();
        });
    }

    deleteAttachment(fileId) {
        this.props.deleteAttachment({ id: this.props.message.id, attachmentId: fileId });
    }

    uploadAllFiles(video, additionalFiles) {

        this.setState({
            uploadingText: 'Uploading',
            uploadedVideo: video,
            uploadedFiles: additionalFiles
        });

        if (video.uri) {
            this.props.uploadVideo({ path: video.uri, id: this.props.message.id }, this.updateProgress, ()=> {
                if (additionalFiles) {
                    this.uploadAdditionalFiles(additionalFiles);
                } else {
                    this.submit();
                }
            });
        } else if (additionalFiles) {
            this.uploadAdditionalFiles(additionalFiles);
        }
    }

    submit() {

        setTimeout(()=>this.props.downloadVideo(this.props.message.id), 10);
        setTimeout(()=>this.props.getAttachments(this.props.message.id), 10);

        let that = this;

        if (that.props && that.props.message){
            that.props.getMessage(that.props.message.id, (message) => {

                that.props.navigator.push(getScene('newMessageContact'));
                that.setState({
                    uploadingText: '',
                    uploadedVideo: {},
                    uploadedFiles: [],
                });

                // if (message.encodedStatus == "NotEncoded" || message.encodedStatus == "Processing") {
                //     if (!that.videoEncoding) {
                //         that.videoEncoding = true;
                //         that.encodingCountdown = 180;
                //         that.startEncodingCountdown();

                //         that.setState({
                //             uploadingText: 'Encoding Video (3m 0s)'
                //         });
                //     }
                // } else if (that.videoEncoding) {
                //     that.props.navigator.push(getScene('newMessageContact'));
                //     that.setState({
                //         uploadingText: '',
                //         uploadedVideo: {},
                //         uploadedFiles: [],
                //     });
                //     that.clearAllIntervals();
                //     that.videoEncoding = false;
                //     clearInterval(uploadStatus);
                // }
            });
        }
    }

    render() {

        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Message Content'
                    titleColor='#fff'
                />}

                {(this.props.attachmentsLoading || this.props.videoLoading) && <LoadingScene />}

                {(!this.props.attachmentsLoading && !this.props.videoLoading) && <NewMessageContentForm
                    defaultVideoUrl={this.props.video}
                    attachments={this.props.attachments}
                    uploadedVideo={this.state.uploadedVideo}
                    uploadedFiles={this.state.uploadedFiles}
                    onUploadAllFiles={this.uploadAllFiles}
                    onDeleteAttachment={this.deleteAttachment}
                    onSubmit={this.submit}
                    uploadingText={this.state.uploadingText} />}

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
