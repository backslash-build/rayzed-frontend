import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Linking,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../../services/navigation';
import timeouts from '../../services/timeouts';
import * as actions from '../../actions';
import * as messagingActions from '../../actions/messaging';

import SplashScene from '../SplashScene';

import App from '../../App';
import NewMessageSummaryForm from '../../components/NewMessage/SummaryForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state.messaging, me: state.me, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class NewMessageSummaryScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        message: React.PropTypes.any,
		getMessage: React.PropTypes.func,
        recipients: React.PropTypes.any,
        attachments: React.PropTypes.object,
        getAttachments: React.PropTypes.func,
        me: React.PropTypes.any,
        sendMessage: React.PropTypes.func
    }

    state = {
		message: this.props.message
	}

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
		this.startSentInterval();
		//Linking.addEventListener('url', this._handleOpenURL);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
		//Linking.removeEventListener('url', this._handleOpenURL);
		this.clearAllIntervals();
    }

//	_handleOpenURL(event) {
//  		console.log(event.url);
//	}

    back() {
        this.props.navigator.popN(1);
        return true;
    }

	startSentInterval() {
        let that = this;

		this.getSentInterval = timeouts.setInterval(()=> {
			if (this.state.message) {
				this.props.getMessage(this.state.message.id, ()=> {
					console.log("Message status", this.props.message.status);
					if (this.props.message.status == "Sent") {
						this.props.navigator.push(getScene('newMessageRaised'));
						this.clearAllIntervals();
					}
				});
			}
		}, 5000);
	}

    clearAllIntervals() {
		timeouts.clearInterval(this.getSentInterval);
	}

	translateError(error) {

        return error;
        if (!error) {
            return '';
        }

        if (error.indexOf("VideoNotEncoded") !== -1){
            return "Sorry, there was an error with your video";
        }
        return error;
    }

    submit() {
        this.props.sendMessage(this.state.message.id, (paymentDetails) => {
			this.setState({
				paymentUrl: paymentDetails.paymentUrl
			});
			if (paymentDetails.paymentUrl) {
				this.openUrl(paymentDetails.paymentUrl);
				this.setState({
					error: null
				})
			} else if (paymentDetails[0].error) {
				this.setState({
					error: this.translateError(paymentDetails[0].error)
				})
			}
        });
    }

	openUrl(url) {
        let urlLink = url;

        // has to start with http:// or https://
        if(url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
    }

	paymentSubmitted() {
		this.props.navigator.replace(getScene('newMessagePayment'));
	}

    render() {
        const items = [];

        const cost = this.state.message.recipients.map(r => r.donation).reduce((a, b) => a+b, 0);
        const totalCost = cost + 10;

        const summary = {
            title: this.state.message.subject,
            recipients: this.state.message.recipients,
			attachments: this.props.attachments,
            content: this.state.message.body,
            contact: [this.props.me.data.firstName + " " + this.props.me.data.lastName, this.props.me.data.email, this.state.message.website, this.state.message.telephone],
            messageCost: '£' + cost,
            adminFee: '£10',
            totalCost: '£' + totalCost
        };

        return (
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Payment'
                    titleColor='#fff'
                />}
                <NewMessageSummaryForm
                    summary={summary}
                    onSubmit={this.submit}
                    items={items}
					paymentUrl={this.state.paymentUrl}
					paymentSubmitted={this.paymentSubmitted}
					error={this.state.error} />

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
