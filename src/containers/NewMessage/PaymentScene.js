import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Linking,
    TextInput,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SplashScene from '../SplashScene';

import { getScene } from '../../services/navigation';
import * as actions from '../../actions';
import * as messagingActions from '../../actions/messaging';
import timeouts from '../../services/timeouts';

import NewMessagePaymentForm from '../../components/NewMessage/PaymentForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class NewMessagePaymentScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        updateMessage: React.PropTypes.func,
        sendMessage: React.PropTypes.func,
        localUpdateMessage: React.PropTypes.func,
        me: React.PropTypes.object,
        messaging: React.PropTypes.object,
        message: React.PropTypes.object,
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    submit() {}

	openUrl(url) {
        let urlLink = url;

        // has to start with http:// or https://
        if(url.indexOf("http://") === -1 && url.indexOf("https://") === -1) urlLink = "http://" + urlLink;

        Linking.openURL(urlLink).catch(err => console.error('An error occurred', err));
    }

	paymentSubmitted() {
		this.props.navigator.replace(getScene('newMessageRaised'));
	}

    render() {
        return(
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Payment'
                    titleColor='#fff'
                />}

                {this.props.messaging.payment && this.props.messaging.payment.paymentUrl && <NewMessagePaymentForm
                    paymentUrl={this.props.messaging.payment.paymentUrl}
                    onPageChange={this.paymentSubmitted} />}

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
