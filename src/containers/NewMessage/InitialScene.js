import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Text,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../../services/navigation';
import * as actions from '../../actions';
import * as messagingActions from '../../actions/messaging';

import SplashScene from '../SplashScene';
import LoadingScene from '../LoadingScene';
import NewMessageForm from '../../components/NewMessage/InitialForm';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state.messaging, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class NewMessageScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        loading: React.PropTypes.bool,
        cleanMessage: React.PropTypes.func,
        createNewMessage: React.PropTypes.func,
        message: React.PropTypes.object,
        recipients: React.PropTypes.array,
        error: React.PropTypes.string
    }

    state = {
        message: {
            subject: '',
            expiryInDays: 28,
            recipients: []
        }
    }

    componentDidMount() {
        this.props.cleanMessage();
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    findRecipient() {
        this.props.navigator.push(getScene('findRecipient'));
    }

    submit(payload) {
        this.props.createNewMessage(payload);
		this.props.navigator.push(getScene('newMessageContent'));
    }

    render() {
        const selectedRecipients = this.props.message && this.props.message.recipientIds || [];
        const recipients = (this.props.message && this.props.message.recipients||[]).filter(a => selectedRecipients.indexOf(a.id) !== -1);

        const value = {
            ...this.state.message,
            recipients
        };

        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='New Message'
                    titleColor='#fff'
                />}

                <NewMessageForm
                    onSubmit={this.submit}
                    value={value}
                    loading={this.props.loading}
                    onChange={value => this.setState({ message: value })}
                    recipients={recipients}
                    onFindRecipient={this.findRecipient}
                    removeRecipient={this.props.removeRecipient} />

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
