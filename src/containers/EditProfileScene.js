import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getScene } from '../services/navigation';
import * as actions from '../actions';
import * as messagingActions from '../actions/messaging';

import App from '../App';
import EditProfileForm from '../components/EditProfileForm';

import SplashScene from './SplashScene';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});


//@connect(
//    ({ organisations }) => ({ organisations }),
//    dispatch => bindActionCreators(actions, dispatch)
//)

@connect(state => ({...state.messaging, me: state.me, organisations: state.organisations, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class EditProfileScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        me: React.PropTypes.object,
        updateMe: React.PropTypes.func,
		updatePassword:React.PropTypes.func,
    }

    state = {}

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    back() {
        this.props.navigator.popToTop();
        return true;
    }

	translateError(error) {
        if (!error) {
            return '';
        }

        if (error.indexOf("CurrentPasswordIncorrect:An error has occurred") !== -1){
            return "Current password is incorrect";
        }
        return error;
    }

    submit(userPayload, passwordPayload) {

		let that = this;

		this.setState({
            userError: null,
            passwordError: null
        });

        if (!/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(userPayload.email)) {
            this.setState({
                userError: "Please enter a vaild email address",
            });
            return false;
        }

        let userChanges = {
            firstName: userPayload.firstName,
            lastName: userPayload.lastName
        }

        if (this.props.me.data.email != userPayload.email) {
            userChanges.email = userPayload.email;
        }

        this.props.updateMe(userChanges);

        if (passwordPayload.NewPassword && passwordPayload.NewPassword.length > 0) {
            this.props.updatePassword(passwordPayload, function () {
                if (!that.props.me.error) {
                    that.back();
                } else {
                    that.setState({
                        passwordError: that.translateError(that.props.me.error)
                    })
                }
            });
        } else {
            this.back();
        }
    }

    render() {

        return (
            <View style={{flex:1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Your Profile'
                    titleColor='#fff'
                />}

                <EditProfileForm
					me={this.props.me}
                    onSubmit={this.submit}
					userError={this.state.userError}
					passwordError={this.state.passwordError} />

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
