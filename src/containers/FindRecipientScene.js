import React, { Component } from 'react';
import {
    View,
    BackAndroid,
    Platform,
    ToolbarAndroid,
    StyleSheet,
    Dimensions
 } from 'react-native';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from '../actions';
import * as messagingActions from '../actions/messaging';

import LoadingScene from './LoadingScene';
import FindRecipientForm from '../components/FindRecipientForm';

import SplashScene from './SplashScene';

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#61b746',
        height: 56
    },
    app: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: Dimensions.get('window').width,
        ...Platform.select({
            android: {
                height: Dimensions.get('window').height,
            },
            ios: {
                height: Dimensions.get('window').height - 64
            }
        })
    }
});

@connect(state => ({...state.messaging, connection: state.login.connection}), dispatch => bindActionCreators({...actions, ...messagingActions}, dispatch))
@autobind
export default class FindRecipientScene extends Component {

    static propTypes = {
        navigator: React.PropTypes.any,
        testConnection: React.PropTypes.func,
        message: React.PropTypes.any,
        recipients: React.PropTypes.array,
        searchedRecipients: React.PropTypes.array,
        getAllRecipients: React.PropTypes.func,
        addRecipient: React.PropTypes.func,
        removeRecipient: React.PropTypes.func
    }

    state = {
        selectedRecipients: [],
        showingResults: false,
        searchTerms: {
            nameSearch: '',
            positionSearch: '',
            companySearch: ''
        }
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.back);
    }

    componentWillReceiveProps(newProps) {
        // bit of a hack because the API isn't consistent with how it represents recipients on a message!!

        if(newProps.message && newProps.message.recipientIds && newProps.message.recipientIds.length >= 0) {
            const selectedRecipients = newProps.message.recipientIds.map(r => r.userId || r);
            this.setState({ selectedRecipients });
        }
    }

    back() {
        this.props.navigator.popN(1);
        return true;
    }

    search(query) {
        this.setState({
            showingResults: true
        })
        this.props.searchRecipients(query);
    }

    updateSearchTerms(type, term) {
        let searchTerms = this.state.searchTerms;
        searchTerms[type] = term;
        this.setState({ searchTerms: searchTerms });
    }

    clearSearch() {
        this.setState({
            showingResults: false
        })
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {(Platform.OS === 'android') && <ToolbarAndroid
                    navIcon={require('../../images/ic_arrow_back_white_24dp/android/drawable-hdpi/ic_arrow_back_white_24dp.png')}
                    onIconClicked={this.back}
                    style={styles.toolbar}
                    title='Select recipients'
                    titleColor='#fff'
                />}

                {!this.props.loading &&  <FindRecipientForm
                    searchTerms={this.state.searchTerms}
                    updateSearchTerms={this.updateSearchTerms}
                    onSearch={this.search}
                    onSubmit={this.back}
                    addRecipient={this.props.addRecipient}
                    removeRecipient={this.props.removeRecipient}
                    selectedRecipients={this.state.selectedRecipients}
                    allRecipients={this.props.searchedRecipients}
                    showingResults={this.state.showingResults}
                    clearSearch={this.clearSearch} />}
                {this.props.loading && <LoadingScene />}

                {this.props.connection !== true && <View style={styles.app}><SplashScene onRetry={()=> this.props.testConnection(null)} /></View>}
            </View>
        );
    }
}
