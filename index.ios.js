import { AppRegistry } from 'react-native';

import App from './src/App.ios';

AppRegistry.registerComponent('hughdemo', () => App);