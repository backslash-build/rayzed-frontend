# React Native Demo
React native supports iOS and Android, (no windows phone)

## Before running the app
before running this demo, you need to download some tools, if you want to run iOS, you must have a Mac
if you want to run android, you need either Windows or Mac

In both cases you need to install the react native cli,
`npm install react-native-cli --global`
(requires Nodejs/npm)

### iOS
xcode
???
To run the app on a phone, setup the signing keys and provisioning profile, and then open the project in XCode, and click build.

### Android
android studio
something version 6.0
either an android virtual device or a real android phone connected with debugging enabled

## Running the React Native demo
Clone this repo. Navigate to the folder with this README file in, and run `npm install`.
Once installed, you can start the react native webserver and packager with `react-native start`
You can then run the iOS and Android debuggers with
`react-native run-ios`
and
`react-native run-android`


#todo
TODO

    pre-setup
        figure out what stage of setup the user is at


    home page
        load stats
        load liked videos


    watch next video page

        try to load list of available videos,
        if there are any, pick one and load the content for it

        if there are none, show message saying they've watched all their videos, with 'ok' button that takes them back to  the home page


    liked video page

        show the content of the video,

        show the extra things


    settings page

        show current info

        allow update calls to info




possible states on main page

    enabled receiver

    enabled sender

    unverified email address

    verified email address, not a receiver

    verified email address, has opted to become receiver but has not been 'enabled'

